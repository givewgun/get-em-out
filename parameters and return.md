Register
 user/register/1 for homeowner
 user/register/0 for contractor
 require parameter 
    please see backend its a lot
 return 
    {status: "username taken"} if username duplicate
    {status:"duplicate ID"} if NAtId or fda_cer duplicate
    {status: "success"} if success

Login
    require
        username
        password
        user_type : "1" for gomeowner / "1" for homeowner
    return
        {status: "no account"} if there is no account (including login to wrong user_type choice)
        {status: 'wrong username or password'} for correct use_type but wrong username or password

        ####if successfully login as homeowner 
        [{"status":"success"},{"NatID":,"firstname":,"lastname":,"username":"","addr":"","latitude":,"longitude":,"phone":"","email":"","photo_url":"","email_verify":0/1}]
        
        #####if successfully login as contractor
        [{"status":"success"},{"fda_cer":"","tax_id":"","comp_name":"","username":"","addr":"","latitude":,"longitude":,"photo_url":"","paymentByCredit":0/1,"paymentByBank":0/1,"phone":"","email":"","rating":,"verify_status":0/1,"email_verify":0/1}]

BOOKING

    getAllRequest
        /booking/getAllRequest
        require
            -homeowner
            {
                "user_type": "1",
                "NatID": "",
            }
            -contractor
            {
                "user_type": "0",
                "fda_cer": ""
            }
        return
            [
                {
                    "caseid": ,
                    "comp_name": ",
                    "contractor_id": "",
                    "descr": "",
                    "type_name": "",
                    "price": ,
                    "status_name": "",
                    "request_date": "",
                    "service_date": 
                }, .....
            ]
     getRequestDetail
        /booking/getAllRequest
        require
            {
                "caseid" : ""
            }
        return
           เยอะเหี้ยๆ ดูในbacktest เอาเถอะเพื่อน
    Check Availibility
    /booking/checkAvailable
            require:
                {
                    "user_type": "1",
                    "sid" : "",
                    "request_date" : ""   // date type in html
                }
            result
           {status : "full"}
           {status : "available"}

    Request service
        /booking/request
            require:
                {
                    "user_type": "1",
                    "NatID" : "",
                    "sid" : "",
                    "request_date" : ""   // date type in html
                }
            result
                {status:"success"}
                {status:"not homeowner"}
    Cancel service
        /booking/cancel
         require:
                {
                    "user_type": "1",
                    "caseid": ""
                }
         result
                {status:"success"}
                {status:"not homeowner"}
    Accept service
        /booking/accept
         require:
                {
                    "user_type": "0",    // cuz contractor
                    "caseid": ""
                }
        result
                {status:"success"}
                {status:"not contractor"}
    Reject service
        /booking/reject
        require:
            {
                "user_type": "0 / 1",
                "caseid": "",
                "remarks" : ""
            }
        result
                {status:"success"}
                {status:"not contractor"}

     service complete
        /booking/serviced
        require:
            {
                "user_type": "0 / 1",
                "caseid": "",
            }
        result
                {status:"success"}
                {status:"not contractor"}

Feedback
    giveFeedback
        /feeback/giveFeedback
        require:
        {
            "caseid": "",
            "rating": "",
            "comment": ""
        }
    




