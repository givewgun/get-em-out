//this code is for file dbconnect.js in server/config
var mysql = require('mysql');
    port = process.env.PORT || 4205;

if (port === 4205) {
    var connection = mysql.createConnection({
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'xxxx', // change before runnnnnnnnnn
        database: 'se_project',
        insecureAuth: true,
        multipleStatements: true,
        dateStrings: 'date'
    });
} else {
    console.log("can't connect")
   //same as above, with live server details
}

connection.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

module.exports = connection;