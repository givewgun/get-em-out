use se_project;
SET SQL_SAFE_UPDATES = 0;
SET FOREIGN_KEY_CHECKS = 0; 

delete from authentication;
delete from contractor;
delete from service;
TRUNCATE TABLE service;
TRUNCATE TABLE req_case;
TRUNCATE TABLE payment;
delete from homeowner;
delete from req_case;
delete from payment;
delete from requests;
SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('aongoong', '$2b$10$G4MFAv19XQD73B5pXUxLpeK/2C8JMP/wIxT3im2z1qTR9Rfc8D4ry', '1');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('Boomsixx','$2b$10$LKt6LV/15WNecisdePWs.O2vJ2MMJO3hzJGOeLQwQyfOMy.w8E7Pi','1');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('bugaway','$2b$10$xaDZhZ0seaX5N12lcmC1H.PltOxSzXWe6EC.XlyyQLa.P85ueCM9m','0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('dmx','$2b$10$chaIY7C2abQ1EbObZW/aXOccv8.C76gMxkVe4wzGEpSKLg27anjk6','0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('dplopx','$2b$10$G4MFAv19XQD73B5pXUxLpeK/2C8JMP/wIxT3im2z1qTR9Rfc8D4ry','0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('dtpp','$2b$10$G4MFAv19XQD73B5pXUxLpeK/2C8JMP/wIxT3im2z1qTR9Rfc8D4ry','0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('hell0258','$2b$10$gCaiD93Zy2bxH2p2.yjDl.0QCa2dklbPMwFo17Q1iymoHX3527FUG','1');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('KantLove0000','$2b$10$G4MFAv19XQD73B5pXUxLpeK/2C8JMP/wIxT3im2z1qTR9Rfc8D4ry','1');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('pepest','$2b$10$6PZdwLfM2.jqdFRokt7E8ObOxQ2QnIuFOoUGj9z2zRW8zfRUSHV5a','0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('youtub','$2b$10$JV9Sp8hfU.JbqM02yUtPoe7b2ynIxuiB9q762pqcdv.NU8S72NNv6','0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('abc','$2b$10$2/d.6yak3ARDITXbDoG70OAZeYTFYK/xzHPLqpSR4Qvt/JgEF1uGq', '0');
INSERT INTO `authentication` (`username`,`pass`,`user_type`) VALUES ('admin', '$2b$10$XwItjzGHNSJPTKm1UKcgY.KcmEV0ZgoAKSg8U17IStzc1nfUlo5wq', '99');


insert into contractor(fda_cer, tax_id, comp_name, username, addr, latitude, longitude, photo_url,paymentByCredit,paymentByBank,verify_status,email_verify,bank_account) values("96526","6","Youtube", "youtub","9911 Ratchadaphisek 14/1 Alley, Khwaeng Bukkhalo, Khet Thon Buri, Krung Thep Maha Nakhon 10600, Thailand",13.713596,100.479991,"https://i.kym-cdn.com/entries/icons/original/000/021/311/free.jpg",1,1,1,1,"45454");
insert into contractor(fda_cer, tax_id, comp_name, username, addr, latitude, longitude, photo_url,paymentByCredit,paymentByBank,verify_status,email_verify,bank_account) values("78551","5","Pestbuster", "pepest","99 Somdet Phra Chao Tak Sin Rd, Khwaeng Khlong Ton Sai, Khet Khlong San, Krung Thep Maha Nakhon 10600, Thailand",13.724386,100.493113,"https://pbs.twimg.com/profile_images/2370446440/6e2jwf7ztbr5t1yjq4c5_400x400.jpeg",1,0,1,1,"1234567890");
insert into contractor(tax_id,fda_cer, comp_name, username,phone,email,rating,photo_url,addr, latitude, longitude, paymentByCredit,paymentByBank,verify_status,email_verify,bank_account,photo_fdacer,bankname) values("1","55555", "D-Plop X", "dplopx","0891729318","aongoong.jp@gmail.com",3.45,"https://dellingerexterminating.com/wp-content/uploads/2018/10/pest-control-768x644.jpg","92 Soi Saphan Lueang, Khwaeng Hiranruchi, Khet Thon Buri, Krung Thep Maha Nakhon 10600, Thailand",13.727925,100.491584,1,1,1,1,"88888888","/image/fdacer/55555.jpg", "Kasikorn");
insert into contractor(tax_id,fda_cer, comp_name, username,phone,email,rating,photo_url,addr, latitude, longitude, paymentByCredit,paymentByBank,verify_status,email_verify,bank_account) values("2","24680", "DTPP", "dtpp","0891729318","DTPP@GGWP.com",5,"https://pbs.twimg.com/profile_images/1045004026197159936/R2Y4njvm_400x400.jpg","82 Soi Sunthon Phimon, Khwaeng Rong Muang, Khet Pathum Wan, Krung Thep Maha Nakhon 10330, Thailand",13.738860,100.520461,0,1,1,1,"1234567890");
insert into contractor(tax_id,fda_cer, comp_name, username,phone,email,rating,photo_url,addr, latitude, longitude, paymentByCredit,paymentByBank,verify_status,email_verify,bank_account) values("3","12345", "DMX", "dmx","0868923172","asd@GG.com",2.111,"https://brightcove04pmdo-a.akamaihd.net/3653334524001/3653334524001_5726853352001_5726807320001-vs.jpg?pubId=3653334524001&videoId=5726807320001","156 Thanon Phahon Yothin, Khwaeng Samsen Nai, Khet Phaya Thai, Krung Thep Maha Nakhon 10400, Thailand",13.767364,100.539998,1,0,1,1,"1234567890");
insert into contractor(tax_id,fda_cer, comp_name, username,phone,email,rating,photo_url,addr, latitude, longitude, paymentByCredit,paymentByBank,verify_status,email_verify,bank_account) values("4","67890", "Bug-Away", "bugaway","0836427893","bff@GG.com",4.3,"https://i.kym-cdn.com/entries/icons/facebook/000/000/043/disaster-girl.jpg","41/55, Soi Sunthronpimol, Jarumeung Rd.,Rong Muang, Pathum Wan,Bangkok, 10330, 10330, Thailand",13.736669,100.534875,1,1,1,1,"1234567890");
insert into contractor(tax_id,fda_cer, comp_name, username,phone,email,rating,photo_url,addr, latitude, longitude, paymentByCredit,paymentByBank,verify_status,email_verify,bank_account,photo_fdacer) values("1234567890123","78634535", "Ku-Y INC", "abc","081111111","abu@GG.com",4.3," /image/profile/Contractor/78634535.jpg","41/55, Soi Sunthronpimol, Jarumeung Rd.,Rong Muang, Pathum Wan,Bangkok, 10330, 10330, Thailand",13.736669,100.534875,1,1,0,1,"1234567890","/image/fdacer/78634535.jpg");



insert into service(contractor_id, serv_type, price, descr, daily_limit) values ("55555", "3", 500,"กำจัดฝูงมดรวมทั้งรัง", 5);
insert into service(contractor_id, serv_type, price, descr, daily_limit) values ("55555", "2", 800,"ไม่รู้งง", 2); 
insert into service(contractor_id, serv_type, price, warranty,descr) values ("24680", "2", 750, "18","รับกำจัดงูไม่รวมงูพิษ");
insert into service(contractor_id, serv_type, price,descr) values ("12345", "1", 1220,"กำจัดหนูทั้งรัง"); 
insert into service(contractor_id, serv_type, price, warranty,descr) values ("67890", "2", 780, "12","กำจัดงูขนาดเล็ก");
insert into service(contractor_id, serv_type, price,descr) values ("55555", "4", 699,"รับกำจัดฝูงปลวก"); 
insert into service(contractor_id, serv_type, price, warranty,descr) values ("12345", "2", 5000, "10","กำจัดฝูงมดรวมรัง");
insert into service(contractor_id, serv_type, price, warranty,descr) values ("12345", "3", 3000, "2","กำจัดงูทุกแบบรวมงูพิษ");
insert into service(contractor_id, serv_type, price, warranty,descr) values ("12345", "4", 2000, "6","รับกำจัดรังจอมปลวกขนาดใหญ่");
insert into service(contractor_id, serv_type, price, warranty,descr) values ("12345", "5", 2500, "10","กำจัดแมลงสาบทั่วบ้านรวมรัง");
insert into service(contractor_id, serv_type, price,descr) values ("96526", "1", 2000,"กำจัดหนูทั่วบ้าน");



insert into homeowner(natid,firstname, lastname, username,addr,phone,email,email_verify) values ("1234567890123","GGWP", "-san","hell0258", "ads dsa bkk","0801234567","aongoong.jp@gmail.com",1); 
insert into homeowner(natid,firstname, lastname, username,addr,phone,email,email_verify) values ("4567890123456","Kant", "Love","KantLove0000", "white rock vancouver BC","0801234567","Kanlove@dplop.com",1);
insert into homeowner(natid,firstname, lastname, username,addr,phone,email,email_verify) values ("1513545531597","Boom", "Sang","Boomsixx", "entrty muang khkn","0876544567","Boomsan@khonkaen.com",1);
INSERT INTO `se_project`.`homeowner` (`NatID`, `firstname`, `lastname`, `username`, `addr`, `latitude`, `longitude`, `phone`, `email`, `photo_url`, `photo_idcard`, `email_verify`) VALUES ('1100702588251', 'Gun', 'Kaewngarm', 'aongoong', '28หมู่2', '13.7387352', '100.53', '0970385655', 'aongoong.jp@gmail.com', '1100702588251.jpg', '1100702588251.jpg', '0');


insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("2",1,"Contractor cancelled so it's trash!",'2019-01-1',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("4",5,"Contractor accept my req so it's 5 star!",'2019-01-15',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("6",4,"Good!",'2019-02-01 10:41:26','2019-02-05');
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("5",null,null,'2019-02-05',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("1",null,null,'2019-01-15',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("2",null,null,'2019-03-05',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("3",null,null,'2019-04-05',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("4",null,null,'2019-02-05',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("5",null,null,'2019-02-05',null);
insert into req_case(cstatus,feedback_rating,feedback_comment,request_date,service_date) values ("6",5,"Good!",'2019-04-25',null);

insert into payment(cid,ptype,paydate,method) values (3,"deposit",'2019-02-05 10:41:13',"credit");
insert into payment(cid,ptype,paydate,method) values (2,"deposit",'2019-02-05 10:41:13',"credit");
insert into payment(cid,ptype,paydate,method) values (1,"remaining",'2019-02-18 15:32:34',"bank");
insert into payment(cid,ptype,paydate,method) values (1,"deposit",'2019-02-14 12:22:22',"credit");


insert into requests(natid,caseid,sid) values ("1234567890123","0000000001",1);
insert into requests(natid,caseid,sid) values ("1234567890123","0000000003",2);
insert into requests(natid,caseid,sid) values ("4567890123456","0000000002",1);
insert into requests(natid,caseid,sid) values ("4567890123456","0000000004",2);
insert into requests(natid,caseid,sid) values ("4567890123456",5,1);
insert into requests(natid,caseid,sid) values ("4567890123456",6,1);
insert into requests(natid,caseid,sid) values ("4567890123456",7,1);
insert into requests(natid,caseid,sid) values ("4567890123456",8,1);
insert into requests(natid,caseid,sid) values ("4567890123456",9,1);
insert into requests(natid,caseid,sid) values ("4567890123456",10,1);



