drop schema if exists se_project;
Create schema se_project;
use se_project;

drop table if exists authentication;
create table authentication(
	username varchar(15) primary key,
    pass varchar(100) not null,
    user_type varchar(2) not null
);


Create table contractor(
	fda_cer varchar(15) primary key,
	tax_id varchar(15) not null,
	comp_name varchar(60) not null,
	username varchar(20) not null UNIQUE,
	addr varchar(150),
	latitude float(9,7) default 0,
	longitude float(17,14) default 0,
	photo_url varchar(500),
    photo_fdacer varchar(500),
	paymentByCredit bool,
    paymentByBank bool,
	phone varchar(10),
	email varchar(30),
	rating float(3,2),
    verify_status bool not null default false,
	email_verify bool not null default false,
    bank_account varchar(15),
    bankname varchar(50),
    CONSTRAINT FK_userContractor FOREIGN KEY (username) REFERENCES authentication(username) ON DELETE CASCADE
);




drop table if exists service_type_map;
create table service_type_map(
   type_id varchar(10) primary key,
   type_name varchar(20)
);

drop table if exists service;
create table service(
  id int(10) primary key auto_increment,
  contractor_id varchar(15), 
  descr varchar(100),
  serv_type varchar(10) not null,
  price decimal(10,4) not null,
  start_time time,
  end_time time,
  warranty varchar(10),
  photo_url varchar(60),
  rating float(3,2),
  daily_limit int default 1,
  foreign key (contractor_id) references contractor(fda_cer)  on update cascade,
  foreign key (serv_type) references service_type_map(type_id)  on update cascade
);

select * from service inner join contractor on service.contractor_id = contractor.fda_cer ;

create table homeowner(
	NatID varchar(13) primary key,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
	username varchar(20) not null UNIQUE,
	addr varchar(150),
	latitude float(9,7) default 0,
	longitude float(17.14) default 0,
    phone varchar(10),
    email varchar(30),
    photo_url varchar(500),
    photo_idcard varchar(500),
    email_verify bool not null default false,
	CONSTRAINT FK_userHomeowner FOREIGN KEY (username) REFERENCES authentication(username) ON DELETE CASCADE
);


create table cstatus_name(
	cstatus varchar(10) primary key,
    status_name varchar(20)
);


create table req_case(
	id int auto_increment primary key,
    cstatus varchar(10) not null,
    feedback_rating float(3,2),
    feedback_comment varchar(255),
    request_date date,
    service_date date,
    foreign key (cstatus) references cstatus_name(cstatus) on update cascade
);


create table payment(
	id int auto_increment,
	cid int,
    ptype varchar(10),
    paydate datetime,
    method varchar(10),
    #if bank
    amount int,
    bankname varchar(50),
    foreign key (cid) references req_case(id) on update cascade,
    primary key(id,cid,ptype)    
);

create table requests(
	natid varchar(13),
    caseid int,
    sid int(10),
    primary key(natid,caseid,sid),
    foreign key (natid) references homeowner(natid) on update cascade,
    foreign key (caseid) references req_case(id) on update cascade,
    foreign key (sid) references service(id) on update cascade
);

insert into service_type_map(type_id, type_name) values("1","Rat");
insert into service_type_map(type_id, type_name) values("2","Snake");
insert into service_type_map(type_id, type_name) values("3","Ant");
insert into service_type_map(type_id, type_name) values("4","Termite");
insert into service_type_map(type_id, type_name) values("5","Cockroach");

insert into cstatus_name(cstatus,status_name) values ("1","request");
insert into cstatus_name(cstatus,status_name) values ("2","cancelled");
insert into cstatus_name(cstatus,status_name) values ("3","deposit pending");
insert into cstatus_name(cstatus,status_name) values ("4","servicing");
insert into cstatus_name(cstatus,status_name) values ("5","payment pending");
insert into cstatus_name(cstatus,status_name) values ("6","complete");

