const connection = require('../config/dbconnect')

const getAllcontractors = (req, res) => {
    let query = 'SELECT * FROM contractor'
    connection.query(query, (error, results) => {
        if (error) throw error
        res.status(200).send(JSON.parse(JSON.stringify(results)))
    })
}

const getContractorDetail = (req, res) => {
    let query = 'SELECT * FROM contractor WHERE fda_cer = ?'
    let searchResults = []
    connection.query(query, [req.params.id], (error, results) => {
        if (error) throw error
        searchResults = results
        let query2 = 'SELECT * FROM service inner join service_type_map on service.serv_type = service_type_map.type_id WHERE contractor_id = ?'
        connection.query(query2, [req.params.id], (error, results2) => {
            searchResults = searchResults.concat(results2)
            res.status(200).send(JSON.parse(JSON.stringify(searchResults)))
        })
    })
}

//TO DO search
const searchContractor = (req, res) => {
    console.log('searching contractor')
    console.log(req.body)
    var search_name = req.body['search_name'].trim()
    var fid = req.body['fid'].trim()
    console.log(search_name + ' ' + fid)
    let searchResults = []
    //let query = 'SELECT * FROM contractor WHERE fda_cer LIKE "'+fid+'%" and comp_name LIKE "' + search_name + '%"'
    let query = 'SELECT * FROM contractor WHERE fda_cer LIKE ?"%" and comp_name LIKE ?"%"'
    
    connection.query(query, [fid, search_name], (error, results) => {
        if (error) throw error
        console.log("QUERYYY")
        console.log(results)
        res.status(200).send(JSON.parse(JSON.stringify(results)))
    })

}




module.exports = {getAllcontractors, getContractorDetail, searchContractor}