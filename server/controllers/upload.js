// var express = require('express');
// const app = express();
const path = require('path')
var multer = require('multer');
const connection = require('../config/dbconnect')

//Profile 
const profile_storage = multer.diskStorage({
    destination: function (req, file, cb) {
        if(req.params.user_type.trim()=='1') cb(null, './public/image/profile/Homeowner');
        else cb(null, './public/image/profile/Contractor');
    },
    filename: function (req, file, cb) {
        // path.extname(file.originalname) ==> Don't delete

        let photo_path =  req.body['NatID'] + '.jpg';
        let photo_url = 'http://localhost:4200/image/profile/Homeowner/' + photo_path
        let query = `UPDATE se_project.homeowner SET photo_url = '${photo_url}' WHERE (NatID = '${req.body['NatID']}');`

        if(req.params.user_type.trim()=='0'){
            photo_url = 'http://localhost:4200/image/profile/Contractor/' + photo_path
            photo_path = req.body['fda_cer'] + '.jpg';
            query = `UPDATE se_project.contractor SET photo_url = 'http://localhost:4200/image/profile/Contractor/${photo_path}' WHERE (fda_cer = '${req.body['fda_cer']}');`
        }

        cb(null, photo_path)
        connection.query(query,(error,result)=>{
            if(error) throw error
            else console.log('Update Photo Profile');
        })
    }
})
const profile = multer({ storage: profile_storage}).single('profile_image');

const profile_upload = (req,res)=>{
    profile(req,res,(err)=>{
        if(err) throw err;
        else{
            console.log(req.file);
            res.json({status: "upload success"});
        }
    })
    
}


//idcard
const idcard_storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/image/idcard')
    },
    filename: function (req, file, cb) {
        let photo_path = req.body['NatID'] + '.jpg';
        let photo_url = 'http://localhost:4200/image/idcard/' + photo_path
        let query = `UPDATE se_project.homeowner SET photo_idcard = '${photo_url}' WHERE (NatID = '${req.body['NatID']}');`

        cb(null, photo_path)
        connection.query(query,(error,result)=>{
            if(error) throw error
            else console.log('Update Photo ID Card');
        })
    }
})
const idcard = multer({ storage: idcard_storage}).single('idcard_image');

const idcard_upload = (req,res)=>{
    idcard(req,res,(err)=>{
        if(err) throw err;
        else{
            console.log(req.file);
            res.json({status: "upload success"});
        }
    })
}

//fdacer
const fdacer_storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/image/fdacer')
    },
    filename: function (req, file, cb) {
        let photo_path = req.body['fda_cer'] + '.jpg';
        let photo_url = 'http://localhost:4200/image/fdacer/' + photo_path
        console.log(req.body);
        let query = `UPDATE se_project.contractor SET photo_fdacer = '${photo_url}' WHERE (fda_cer = '${req.body['fda_cer']}');`
        
        cb(null, photo_path)
        connection.query(query,(error,result)=>{
            if(error) throw error
            else console.log('Update Photo fdacer');
        })
    }
})
const fdacer = multer({ storage: fdacer_storage}).single('fdacer_image');

const fdacer_upload = (req,res)=>{
    console.log(req.body);
    fdacer(req,res,(err)=>{
        if(err) throw err;
        else{
            console.log(req.body, req.file);
            res.json({status: "upload success"});
        }
    })
}


// Service
const service_storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/image/service')
    },
    filename: function (req, file, cb) {
        let photo_path = req.body['fdacer'] + '-' + req.body['service_id'] + '.jpg';
        let photo_url = 'http://localhost:4200/image/service/' + photo_path
        let query = `UPDATE se_project.service SET photo_url = '${photo_url}' WHERE (id = '${req.body['service_id']}');`

        cb(null, photo_path)
        connection.query(query,(error,result)=>{
            if(error) throw error
            else console.log('Update Photo fdacer');
        })
    }
})
const service = multer({storage : service_storage}).single('service_image');

const service_upload = (req,res)=>{
    service(req,res,(err)=>{
        if(err) throw err;
        else{
            console.log(req.file);
            res.json({status: "upload success"});
        }
    })
}
   


// app.get('/',(req,res)=>{
//     res.sendFile(path.join(__dirname, '../', '/test/back_test.html'))
// })

// app.post('/upload/:user_type',profile_upload)

// app.listen(4200,()=>{
//     console.log('Test_upload on port 4200')
// });

module.exports = {profile_upload ,idcard_upload ,fdacer_upload ,service_upload};