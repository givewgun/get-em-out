const connection = require('../config/dbconnect')

const calculateRating = (serviceId) => {
    //console.log("calculateeee")
    let query = `SELECT @avg_rating :=  AVG(feedback_rating) FROM service inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id 
                    WHERE service.id = "${serviceId}";
                    UPDATE service SET rating = @avg_rating WHERE id = "${serviceId}"`
    connection.query(query , (err, result) => {
        if(err) throw err
        console.log("cal service rating")
        let query2 = `SELECT @avg_rating :=  AVG(feedback_rating) FROM contractor inner join service on service.contractor_id = contractor.fda_cer inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id 
                    WHERE contractor.fda_cer = (select contractor_id from service where service.id= "${serviceId}");
                    UPDATE contractor SET rating = @avg_rating WHERE fda_cer = (select contractor_id from service where service.id= "${serviceId}");`
        connection.query(query2, (err2, result2) => {
            if(err2) throw err
            console.log("cal contractor rating")
        })
    })

    
}

// use se_project;
// SELECT @avg_rating :=  AVG(feedback_rating) FROM contractor inner join service on service.contractor_id = contractor.fda_cer inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id 
// WHERE contractor.fda_cer in (select contractor_id from service where service.id=1);

const giveFeedback = (req, res) =>{

    console.log(req.body)
    let case_id = req.body["caseid"]
    let rating = req.body["rating"]
    let comment = req.body["comment"]
    

    let query = `UPDATE req_case SET feedback_rating = ${rating}, feedback_comment = "${comment}" WHERE id = "${case_id}"`
    connection.query(query, (err, result) => {
        if(err) throw err
        //console.log(query)
        //res.json({status: "success"})
        let query2 = `SELECT service.id FROM service inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id 
                        WHERE req_case.id = "${case_id}"`
        connection.query(query2 , (err2, result2) => {
            if(err2) throw err2
            let serviceId = result2[0].id
            calculateRating(serviceId)
            res.json({status: "success"})
        })
    })

}

module.exports = {giveFeedback, calculateRating}