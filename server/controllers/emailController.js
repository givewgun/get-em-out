var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'getemout.cu@gmail.com',
    pass: 'kantanat123'
  }
});

// var emailtoken = 'Hello'
// var url = `http://localhost:4200/user/confirmation/${emailtoken}`

const sendVerifyMail = (user_type,id,email)=>{
  console.log('send email ID ' , id)
  jwt.sign(
    {
      user:id,
      type:user_type,
    },
    'Hello',
    {
      expiresIn:'1d',
    },
    (err,emailToken)=>{
      const url = `http://localhost:4200/user/confirmation/${emailToken}`;

      transporter.sendMail({
        from: 'getemout.cu@gmail.com',
        to: `${email}`,
        subject: 'Please confirm your account',
        html: `Please click this link to complete your Get'Em'Out website registration  <a href = "${url}">${url}</a>`
      },(error,info)=>{
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
      });
    }
  )
};

const sendContractorVerifyMail = (contractor_email)=>{
  console.log('send email ID to ' , contractor_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${contractor_email}`,
    subject: 'Your account has been verified',
    html: `Congratulation!! Your account has been verified. You can now use Get'Em'Out to the fullest. <br>
           Thank you for registering with us.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}

const sendRequestMail = (contractor_email, service_desc, request_date)=>{
  console.log('send email ID to ' , contractor_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${contractor_email}`,
    subject: 'You have new request!',
    html: `Your service : "${service_desc}" have a new request at ${request_date}.<br> 
           Please visit your Get'Em'Out account for more detail.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}

const sendCancelMail = (contractor_email, service_desc, request_date)=>{
  console.log('send email ID to ' , contractor_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${contractor_email}`,
    subject: 'Bad news,  your service has been cancelled!',
    html: `Your service : "${service_desc}" request on ${request_date} has been cancelled by the homeowner.<br> 
            Please visit your Get'Em'Out account for more detail.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}

const sendAcceptMail = (comp_name, homeowner_email, service_desc, request_date)=>{
  console.log('send email ID to ' , homeowner_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${homeowner_email}`,
    subject: 'Good news!!!,  your service request has been accepted!',
    html: `Your service : "${service_desc}" of "${comp_name}" that you request for a service on ${request_date} has been accepted!.<br> 
          You need to pay your deposit costbefore the service can begin.<br>
          Please visit your Get'Em'Out account for more detail.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}

const sendRejectMail = (comp_name, homeowner_email, service_desc, request_date, remarks)=>{
  console.log('send email ID to ' , homeowner_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${homeowner_email}`,
    subject: 'Bad news,  your service request has been rejected',
    html: `Your service : "${service_desc}" of "${comp_name}" that you request for a service on ${request_date} has been rejected.<br> 
          Your contractor rejected the request with remarks : "${remarks}". <br>
          Please visit Get'Em'Out to find another alternative that best suited you. We apologise for any inconveniences.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}

const sendRemainingMail = (comp_name, homeowner_email, service_desc, request_date)=>{
  console.log('send email ID to ' , homeowner_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${homeowner_email}`,
    subject: 'Good news!!!,  your service request has been serviced!',
    html: `Your service : "${service_desc}" of "${comp_name}" that you request for a service on ${request_date} has been serviced!.<br> 
          You need to pay your remaining cost as soon as possible.<br>
          Please visit your Get'Em'Out account for more detail.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}


const sendPaymentMail = (contractor_email, service_desc, request_date, ptype)=>{
  console.log('send email ID to ' , contractor_email)
  transporter.sendMail({
    from: 'getemout.cu@gmail.com',
    to: `${contractor_email}`,
    subject: 'Good news!  Your "' + ptype + '" cost of a service has been paid!',
    html: `Your "` + ptype +  `" cost of service : "${service_desc}" request on ${request_date} has been paid by the homeowner.<br> 
            Please visit your Get'Em'Out account for more detail.`
  },(error,info)=>{
    if (error) {
        console.log(error);
    } else {
        console.log('Email sent: ' + info.response);
    }
  });
}
// jwt.sign(
//     {
//       user:'Zi',
//     },
//     'Hello',
//     {
//       expiresIn: '1d',
//     },
//     (err, emailToken) => {
//       const url = `http://localhost:4200/user/confirmation/${emailToken}`;

//       transporter.sendMail({
//         from: 'zi.autobot3@gmail.com',
//         to: 'zi.sk134@gmail.com',
//         subject: 'Sending Email using Node.js',
//         html: `That was easy! IKuy Prayut <a href = "${url}">${url}</a>`
//       },(err,info)=>{
//         if (error) {
//             console.log(error);
//         } else {
//             console.log('Email sent: ' + info.response);
//         }
//       });
//     },
//   );

// var mailOptions = {
//   from: 'zi.autobot3@gmail.com',
//   to: 'zi.sk134@gmail.com',
//   subject: 'Sending Email using Node.js',
//   html: `That was easy! IKuy Prayut <a href = "${url}">${url}</a>`
// };

// transporter.sendMail(mailOptions, function(error, info){
//   if (error) {
//     console.log(error);
//   } else {
//     console.log('Email sent: ' + info.response);
//   }
// });

module.exports = {sendVerifyMail, sendRequestMail, sendCancelMail, sendAcceptMail, sendRejectMail, sendRemainingMail, sendContractorVerifyMail, sendPaymentMail};