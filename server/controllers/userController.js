const connection = require('../config/dbconnect')
const bcrypt = require('bcryptjs');
const path = require('path')
const saltRounds = 10; // Hello salt, Have to change?
const send_mail = require('./emailController.js')
var jwt = require('jsonwebtoken');

function addHomeowner(req) {
    console.log('Enter adding homeowner mode');
    var NatID = req.body['NatID'].trim();
    var firstname = req.body['firstname'].trim();
    var lastname = req.body['lastname'].trim();
    var username = req.body['username'].trim();
    var addr = req.body['addr'].trim();
    var latitude = req.body['latitude'];
    var longitude = req.body['longitude'];
    var phone = req.body['phone'].trim();
    var email = req.body['email'].trim();
    var photo_url = req.body['photo_url'].trim();
    let query = 'insert into homeowner(natid, firstname, lastname, username, addr, latitude, longitude, phone, email,photo_url) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    connection.query(query, [NatID, firstname, lastname, username, addr, latitude, longitude, phone, email, photo_url], (error, result) => {
        if (error) throw error
        console.log('homeowner inserted!!!')
    })
    send_mail.sendVerifyMail('1',NatID,email);
    
}

function addContractor(req) {
    var fda_cer = req.body['fda_cer'].trim();
    var tax_id = req.body['tax_id'].trim();
    var comp_name = req.body['comp_name'].trim();
    var username = req.body['username'].trim();
    var addr = req.body['addr'].trim();
    var latitude = req.body['latitude'];
    var longitude = req.body['longitude'];
    var photo_url = req.body['photo_url'].trim();
    var paymentByCredit = req.body['paymentByCredit'];
    var paymentByBank = req.body['paymentByBank'];
    var phone = req.body['phone'].trim();
    var email = req.body['email'].trim();
    var bank_account = req.body['bank_account'].trim()
    var bankname = req.body['bankname']
    let query = 'insert into contractor(fda_cer, tax_id, comp_name, username, addr, latitude, longitude, photo_url, paymentByCredit, paymentByBank, phone, email, bank_account, bankname) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    connection.query(query, [fda_cer, tax_id, comp_name, username, addr, latitude, longitude, photo_url, paymentByCredit, paymentByBank, phone, email,bank_account, bankname], (error, result) => {
        if (error) throw error
        console.log('contractor inserted!!!')
    })
    send_mail.sendVerifyMail('0',fda_cer,email);
}

//-----------------experimental ---- not test yet

function editHomeowner(req) {
    console.log('Enter editing homeowner mode ' , req.body);
    var username = req.body['username']; // frontend send username of the editor (หน้าของแต่ละคน) (in session/cookie)
    var firstname = req.body['firstname']
    var lastname = req.body['lastname']
    var addr = req.body['addr']
    var latitude = req.body['latitude'];
    var longitude = req.body['longitude'];
    var phone = req.body['phone']
    var email = req.body['email']
    //var photo_url = req.body['photo_url']

    //query has been tested works OK!!! 
    //COALESCE will return first value that is not null, NULLIF return null if two sting is equal, else return first value
    let query = 'UPDATE homeowner SET \
                firstname = COALESCE(NULLIF(?,""), firstname), \
                lastname = COALESCE(NULLIF(?,""), lastname), \
                addr = COALESCE(NULLIF(?,""), addr), \
                latitude = COALESCE(NULLIF(?,""), latitude),\
                longitude = COALESCE(NULLIF(?,""), longitude), \
                phone = COALESCE(NULLIF(?,""), phone), \
                email = COALESCE(NULLIF(?,""), email)\
                WHERE username = ?'
    connection.query(query, [firstname, lastname, addr, latitude, longitude, phone, email, username], (error, result) => {
        if (error) throw error
        console.log('homeowner edited!!!')
    })
}

function editContractor(req) {
    console.log('Enter editing contractor mode');
    var fda_cer = req.body['fda_cer']; // frontend send username of the editor (หน้าของแต่ละคน) (in session/cookie)
    //var tax_id = req.body['tax_id'].trim(); can't edit
    var comp_name = req.body['comp_name'].trim();
    var addr = req.body['addr'].trim();
    var latitude = req.body['latitude'];
    var longitude = req.body['longitude'];
    //var photo_url = req.body['photo_url'].trim();
    var bank_account = req.body['bank_account'].trim();
    var paymentByCredit = req.body['paymentByCredit'];
    var paymentByBank = req.body['paymentByBank'];
    var phone = req.body['phone'].trim();
    var email = req.body['email'].trim();
    var bankname = req.body['bank_name'].trim();
    //var rating = req.body['rating'].trim(); can't edit

    //query has been tested works OK!!! 
    //COALESCE will return first value that is not null, NULLIF return null if two sting is equal, else return first value
    let query = 'UPDATE contractor SET \
                comp_name = COALESCE(NULLIF(?,""), comp_name), \
                addr = COALESCE(NULLIF(?,""), addr), \
                latitude = COALESCE(NULLIF(?,""), latitude),\
                longitude = COALESCE(NULLIF(?,""), longitude), \
                paymentByCredit = COALESCE(NULLIF(?,""), paymentByCredit),\
                paymentByBank = COALESCE(NULLIF(?,""), paymentByBank),\
                phone = COALESCE(NULLIF(?,""), phone), \
                email = COALESCE(NULLIF(?,""), email), \
                bank_account = COALESCE(NULLIF(?,""), bank_account),\
                bankname = COALESCE(NULLIF(?,""), bankname)\
                WHERE fda_cer = ?'
    connection.query(query, [comp_name, addr, latitude, longitude, paymentByCredit, paymentByBank, phone, email, bank_account, bankname, fda_cer], (error, result) => {
        if (error) throw error
        console.log('contractor edited!!!')
    })
}





const register = (req, res) => {
    var username = req.body['username'].trim();
    var password = req.body['password'].trim();
    var email = req.body['email'].trim();
    var user_type = req.params.user_type.trim(); // For homeowner user_type=1 , contractor user_type=0
    console.log('user_type '+user_type);
    let query = 'INSERT INTO authentication (username, pass, user_type) VALUES (?, ?, ?);'
    //check whether username is taken or not
    let check = "SELECT * FROM authentication WHERE username = ?; "
    let taken = false;
    connection.query(check, username, (error, result) => {
        if (error) throw error
        //already has username
        if (result.length != 0) {
            console.log('username already taken')
            taken = true
            res.send(JSON.parse(JSON.stringify({status: "username taken"})))

        }
        //if username is not taken it is ok!
        //console.log('takennn ', taken)
        if (!taken) {
            let checkId
            //check if homeowner , NatID cant be duplciate, contractor fda cant be duplicate
            let duplicate = false
            let param
            if(user_type == '1' || user_type == 1){
                checkId = 'SELECT * FROM homeowner WHERE natid = ?'
                param = req.body['NatID'].trim()
            }
            else{
                checkId = 'SELECT * FROM contractor WHERE fda_cer = ?'
                param = req.body['fda_cer'].trim()
            }
            connection.query(checkId, [param], (err, result) =>{
                if(err) throw err
                if(result.length == 1){
                    res.send({status:"duplicate ID"})
                }
                else{
                    console.log('hashing')
                    bcrypt.genSalt(saltRounds, function (err, salt) {
                        bcrypt.hash(password, salt, function (err, hash) {
                            connection.query(query, [username, hash, user_type], (error, result) => {
                                if (error) {
                                    throw error
                                }
                                console.log('Register!!!')
                                if (user_type == '1' || user_type == 1){
                                    addHomeowner(req)
                                }
                                else addContractor(req);
                                res.send(JSON.parse(JSON.stringify({status: "success"})));
                            })
                        });
                    });
                }
            })
        }
    })

}



const check_password = (req, res) => {
    var username = req.body['username'].trim();
    var PlaintextPassword = req.body['password'];
    var user_type = req.body['user_type']   // 0 is contractor 1 is homewoner
    console.log(req.body)
    let query = 'SELECT pass FROM authentication where username = ?'
    connection.query(query, [username], (err, results) => {
        if(results.length == 0){
            res.send({status: "no account"})
        }
        else{
            var hash = JSON.parse(JSON.stringify(results));
            bcrypt.compare(PlaintextPassword, hash[0].pass, function (err, results2) {
                if (results2) {
                    console.log('Login!!!');
                    let query2
                    if(user_type == '1'){
                        query2  = 'SELECT * FROM homeowner WHERE username = ?'
                    }
                    else if(user_type == '0'){
                        query2  = 'SELECT * FROM contractor WHERE username = ?'
                    }
                    else if(user_type =='99'){
                        query2  = 'SELECT username FROM authentication WHERE username = ? and user_type = "99"'
                    }

                    connection.query(query2, [username], function (err,results3){
                        if(err) throw err
                        if(results3.length == 0){
                            res.send(JSON.parse(JSON.stringify({status: 'no user'})))
                        }
                        else{
                            res.send([{status: 'success'}].concat(results3))
                        }       
                    })
                }
                else {
                    console.log('Login failed')
                    res.send(JSON.parse(JSON.stringify({status: 'wrong username or password'})))
                }
            });
        }
    })

}

const editProfile = (req,res) => {
    var user_type = req.body['user_type']
    console.log(user_type)
    console.log('editing !!!')
    if(user_type == '1'){
        console.log("going edit homeowner")
        editHomeowner(req)
    }
    else{
        console.log("going edit contractor")
        editContractor(req)
    }
    console.log('edit sucessful!!!')
    res.send('Edit Successful')
}

const confirm = (req,res)=>{
    var token  = req.params.token;
    var id = jwt.verify(token,'Hello').user
    var user_type = jwt.verify(token,'Hello').type
    console.log(id, user_type, typeof(user_type))

    // query to db verify = true;
    let query
    if(user_type == '1'){
        query = `UPDATE homeowner SET email_verify = '1' WHERE (NatID = ${id});`
    }
    else{
        query = `UPDATE contractor SET email_verify = '1' WHERE (fda_cer = ${id})`;
    }    
    console.log(query)
    connection.query(query,(err,result)=>{
        if(err) throw err;
        console.log('Confirm leaw Naja');
        console.log(id, user_type)
        // console.log("Redirect to "+path.join(__dirname, '../', '/test/back_test.html'))
        // res.sendFile(path.join(__dirname, '../', '/test/back_test.html'))
        res.redirect('http://localhost:3000/RegisterDone')
    })
    

}

module.exports = { register, check_password, editProfile ,confirm };

