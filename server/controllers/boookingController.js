const connection = require('../config/dbconnect')
const send_mail = require('./emailController.js')
const moment = require('moment');

const getAllRequest = (req, res) => {
    let user_type = req.body["user_type"]
    let query
    if(user_type == "0"){ // contractor
        let fda_cer = req.body['fda_cer']
        query = `SELECT caseid, firstname, lastname, homeowner.NatID,contractor_id,descr,type_name,price,status_name, request_date, service_date, feedback_rating, feedback_comment
                    FROM service inner join requests on service.id = requests.sid 
                    inner join req_case on requests.caseid = req_case.id
                    inner join homeowner on requests.natid = homeowner.NatID
                    #inner join contractor on service.contractor_id = contractor.fda_cer 
                    inner join service_type_map on service.serv_type = service_type_map.type_id
                    inner join cstatus_name on req_case.cstatus = cstatus_name.cstatus
                    where service.contractor_id = "${fda_cer}"`
    }
    else{
        let NatID = req.body['NatID']
        query = `SELECT caseid,comp_name,contractor_id,descr,type_name,price,status_name, request_date, service_date, feedback_rating, feedback_comment
                    FROM service inner join requests on service.id = requests.sid 
                    inner join req_case on requests.caseid = req_case.id
                    #inner join homeowner on requests.natid = homeowner.NatID
                    inner join contractor on service.contractor_id = contractor.fda_cer 
                    inner join service_type_map on service.serv_type = service_type_map.type_id
                    inner join cstatus_name on req_case.cstatus = cstatus_name.cstatus
                    where requests.natid = ${NatID}`
    }
    connection.query(query, (err, result) => {
        if(err) throw err
        res.send(result)
    })

}

const getRequestDetail = (req, res) =>{
    console.log(req.body)
    let case_id = req.body['caseid']
    let query = `SELECT caseid, descr, type_name, price, status_name, request_date, service_date,
                    firstname, lastname , homeowner.NatID, homeowner.addr as homeowner_addr,homeowner.latitude as homeowner_latitude, homeowner.longitude as homeowner_longitude, homeowner.photo_url as homeowner_photo_url,
                        homeowner.email as homeowner_email, homeowner.phone as homeowner_phone,
                    comp_name, fda_cer, contractor.addr as contractor_addr, contractor.latitude as contractor_latitude, contractor.longitude as contractor_longitude, contractor.photo_url as contractor_photo_url,
                        contractor.email as contractor_email, contractor.phone as contractor_phone, paymentByCredit, paymentByBank, bank_account, bankname, feedback_rating, feedback_comment
                FROM service inner join requests on service.id = requests.sid 
                inner join req_case on requests.caseid = req_case.id
                inner join homeowner on requests.natid = homeowner.NatID
                inner join contractor on service.contractor_id = contractor.fda_cer 
                inner join service_type_map on service.serv_type = service_type_map.type_id
                inner join cstatus_name on req_case.cstatus = cstatus_name.cstatus
                where requests.caseid = ${case_id}`
    connection.query(query, (err, result) => {
        if(err) throw err
        res.send(result)
    })
}


const checkAvailable = (req, res) => {
    let serviceID = req.body['sid']
    let request_date = req.body['request_date']

    //find daily limit
    let daily_limit
    let query1 = `SELECT * FROM service WHERE service.id = "${serviceID}"`
    connection.query(query1, (err, result1) =>{
        if(err) throw err
        daily_limit = result1[0].daily_limit
        let query2 = `select * from service inner join requests on service.id = requests.sid 
                        inner join req_case on requests.caseid = req_case.id
                        where service.id = "${serviceID}" and request_date = ? and cstatus not in ("2","6")`
        connection.query(query2, [request_date], (err2, result2) => {
            if(err2) throw err2
            console.log(result2.length)
            if(result2.length >= daily_limit){
                res.json({status : "full"})
            }
            else{
                res.json({status : "available"})
            }
        })
    
    })

}

const requestService = (req, res) => {
    // create case -> create request 
    //console.log(req.body)
    let user_type = req.body['user_type']

    //case table var
    let request_date = req.body['request_date']
    //console.log(typeof (request_date))
    // request_date2 = moment(request_date,"YYYY-MM-DD").format("YYYY-MM-DD");
    // console.log(request_date2, typeof (request_date2))

    // request table var
    let NatID = req.body['NatID']
    let serviceId = req.body['sid']
    let caseId
    console.log("request ser")
    if(user_type == '1'){
        const query = `INSERT INTO req_case (cstatus, request_date) VALUES ('1', ?);
                       INSERT INTO requests (natid, caseid, sid) VALUES (${NatID}, (SELECT MAX(id) from req_case), ${serviceId});`
        connection.query(query, [request_date], (err, result) =>{
            if(err) throw err
            //send email  
            let query2 = `SELECT email, descr FROM service inner join contractor on service.contractor_id = contractor.fda_cer where service.id = ${serviceId}`
            connection.query(query2, (err2, result2) => {
                if(err) throw err
                //console.log(result2)
                send_mail.sendRequestMail(
                    result2[0].email,
                    result2[0].descr,
                    request_date
                )
                connection.query(`SELECT MAX(id) as cid from req_case`, (err3, result3) =>{
                    if(err3) throw err3
                    console.log(result3[0])
                    res.send([{status:"success"}].concat(result3))

                })
            })
            //res.json({status:"success"})
        })

    }
    else{
        res.json({status:"not homeowner"})
    }
}

const cancelRequest = (req, res) => {
    let case_id = req.body["caseid"]
    let user_type = req.body["user_type"]
    if(user_type == '1'){
        let query = `UPDATE req_case SET cstatus = '2' WHERE (id = ${case_id})`
        connection.query(query, (err, result) => {
            if(err) throw err
            let query2 = `SELECT distinct * FROM service inner join requests on service.id = requests.sid 
                            inner join req_case on requests.caseid = req_case.id
                            inner join contractor on service.contractor_id = contractor.fda_cer 
                            where req_case.id = ${case_id}`
            connection.query(query2, (err2, result2) => {
                if(err) throw err
                //console.log(result2[0])
                send_mail.sendCancelMail(
                    result2[0].email,
                    result2[0].descr,
                    result2[0].request_date
                )
                res.json({status:"success"})
            })
        })
    }
    else{
        res.json({status:"not homeowner"})
    }

}


const acceptRequest = (req, res) => {
    let case_id = req.body["caseid"]
    let user_type = req.body["user_type"]
    if(user_type == '0'){ // contractor use
        let query = `UPDATE req_case SET cstatus = '3' WHERE (id = ${case_id})`
        connection.query(query, (err, result) => {
            if(err) throw err
            //send noti to homeowner
            let query2 = `SELECT DISTINCT comp_name, descr, request_date, homeowner.email FROM service inner join requests on service.id = requests.sid 
                            inner join req_case on requests.caseid = req_case.id
                            inner join homeowner on requests.natid = homeowner.NatID
                            inner join contractor on service.contractor_id = contractor.fda_cer 
                            where req_case.id = ${case_id}`
            connection.query(query2, (err2, result2) => {
                if(err) throw err
                //console.log(result2[0])
                send_mail.sendAcceptMail(
                    result2[0].comp_name,
                    result2[0].email, //homeowner mail
                    result2[0].descr,
                    result2[0].request_date
                )
                res.json({status:"success"})
            })
        })
    }
    else{
        res.json({status:"not contractor"})
    }
}

const rejectRequest = (req, res) => {
    let case_id = req.body["caseid"]
    let user_type = req.body["user_type"]
    let remarks = req.body["remarks"]
    if(user_type == '0'){ // contractor use
        let query = `UPDATE req_case SET cstatus = '2' WHERE (id = ${case_id})`
        connection.query(query, (err, result) => {
            if(err) throw err
            //send noti to homeowner
            let query2 = `SELECT DISTINCT comp_name, descr, request_date, homeowner.email FROM service inner join requests on service.id = requests.sid 
                            inner join req_case on requests.caseid = req_case.id
                            inner join homeowner on requests.natid = homeowner.NatID
                            inner join contractor on service.contractor_id = contractor.fda_cer 
                            where req_case.id = ${case_id}`
            connection.query(query2, (err2, result2) => {
                if(err) throw err
                //console.log(result2[0])
                send_mail.sendRejectMail(
                    result2[0].comp_name,
                    result2[0].email, //homeowner mail
                    result2[0].descr,
                    result2[0].request_date,
                    remarks
                )
                res.json({status:"success"})
            })
        })
    }
    else{
        res.json({status:"not contractor"})
    }
}

const serviceComplete = (req, res) => {
    let case_id = req.body["caseid"]
    let user_type = req.body["user_type"]
    if(user_type == '0'){ // contractor use
        let query = `UPDATE req_case SET cstatus = '5' WHERE (id = ${case_id});
                     UPDATE req_case SET service_date = DATE(NOW()) WHERE (id = ${case_id});`
        connection.query(query, (err, result) => {
            if(err) throw err
            //send noti to homeowner
            let query2 = `SELECT DISTINCT comp_name, descr, request_date, homeowner.email FROM service inner join requests on service.id = requests.sid 
                            inner join req_case on requests.caseid = req_case.id
                            inner join homeowner on requests.natid = homeowner.NatID
                            inner join contractor on service.contractor_id = contractor.fda_cer 
                            where req_case.id = ${case_id}`
            connection.query(query2, (err2, result2) => {
                if(err) throw err
                //console.log(result2[0])
                send_mail.sendRemainingMail(
                    result2[0].comp_name,
                    result2[0].email, //homeowner mail
                    result2[0].descr,
                    result2[0].request_date
                )
                res.json({status:"success"})
            })
        })
    }
    else{
        res.json({status:"not contractor"})
    }
}





module.exports = {requestService, checkAvailable, cancelRequest, acceptRequest, rejectRequest, getAllRequest, getRequestDetail, serviceComplete }