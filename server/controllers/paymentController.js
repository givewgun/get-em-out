// const express = require('express')
// const app = express()
const connection = require('../config/dbconnect')
const send_mail = require('./emailController.js')

const getDepositCost = (req, res) => {
    var cid = req.body['caseid']
    let query = `SELECT price*0.3 as cost FROM service inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id 
                WHERE req_case.id = "${cid}"`
    connection.query(query, (err, result) =>{
        if(err) throw err
        res.json({cost : result[0].cost})
    })
}

const getRemainingCost = (req, res) => {
    var cid = req.body['caseid']
    let query = `SELECT price*0.7 as cost FROM service inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id 
                WHERE req_case.id = "${cid}"`
    connection.query(query, (err, result) =>{
        if(err) throw err
        res.json({cost : result[0].cost})
    })
}


const pay = (req,res) => {
    var data = req.body
    console.log(data)
    var cid = data['caseid']
    var ptype = data['ptype']
    var paydate = data['paydate']
    var method = data['method']
    var amount = req.body['amount']
    var bankname = req.body['bankname']
    var query = `INSERT INTO payment (cid, ptype, paydate, method) VALUES ('${cid}', '${ptype}', '${paydate}', '${method}')`
    if(method=='bank'){
        query = `INSERT INTO payment (cid, ptype, paydate, method, amount, bankname) VALUES ('${cid}', '${ptype}', '${paydate}', '${method}', '${amount}', '${bankname}')`
    }
    connection.query(query,(err,result1)=>{
        if(err) throw err
        console.log('Pay Success')
        let query2
        if(ptype == "deposit"){
            query2 = `UPDATE req_case SET cstatus = '4' WHERE (id = ${cid})`
        }
        else{
            query2 = `UPDATE req_case SET cstatus = '6' WHERE (id = ${cid})`
        }
        connection.query(query2, (err2, result2) => {
            if(err2) throw err2
            let query3 = `SELECT distinct * FROM service inner join requests on service.id = requests.sid 
                            inner join req_case on requests.caseid = req_case.id
                            inner join contractor on service.contractor_id = contractor.fda_cer 
                            where req_case.id = ${cid}`
            connection.query(query3, (err3, result3) => {
                if(err) throw err
                //console.log(result2[0])
                send_mail.sendPaymentMail(
                    result3[0].email,
                    result3[0].descr,
                    result3[0].request_date,
                    ptype
                )
                res.json({status:"success"})
            })
        })
    })

};

const getReceipt = (req,res) =>{
    console.log("gen receipt")
    var cid = req.body['caseid']
    var ptype = req.body['ptype']
    let query =`
    select payment.paydate,payment.ptype,payment.amount,homeowner.firstname,homeowner.lastname,contractor.comp_name,contractor.fda_cer,\
    contractor.tax_id,contractor.addr,contractor.phone,contractor.email\
    from payment inner join req_case on payment.cid = req_case.id\
    inner join requests on requests.caseid = req_case.id\
    inner join homeowner on homeowner.NatID = requests.natid\
    inner join service on requests.sid = service.id\
    inner join contractor on service.contractor_id = contractor.fda_cer
    where req_case.id = "${cid}" and payment.ptype = "${ptype}"`

    connection.query(query,(err,result)=>{
        if(err) throw err;
        console.log(result)
        if(result.length == 0){
            res.json({status: 'case not found'})
        } //no case that match
        else{
            res.send([{status: 'success'}].concat(result))
        }
    })
}

module.exports = {pay ,getReceipt, getDepositCost, getRemainingCost}