const connection = require('../config/dbconnect')
const calDistance= require('./miscFunc')
const calculateRating = require('./feedbackController').calculateRating

///server start calculate rating first
connection.query('SELECT * FROM service', (e, r) => {
    if (e) throw e
    console.log("calculate all rating")
    r.forEach(service => {
        //console.log(service.id)
        calculateRating(service.id)
    })
});

const getAllServices = (req, res) => {
    let query = 'SELECT * FROM service'
    connection.query(query, (error, results) => {
        if (error) throw error
        res.status(200).send(JSON.parse(JSON.stringify(results)))
    })
}

const getServiceDetail = (req, res) => {
    const serviceId = req.params.id
    let query = 'SELECT DISTINCT * FROM \
        ( SELECT * FROM service inner join service_type_map on service.serv_type = service_type_map.type_id) AS A \
        inner join( SELECT fda_cer,comp_name,latitude,longitude,paymentByCredit,paymentByBank,phone,email FROM service inner join contractor on service.contractor_id = contractor.fda_cer) AS B \
        ON A.contractor_id = B.fda_cer where id = ?'
    let searchResults = []
    connection.query(query, [serviceId], (error, results) => {
        if (error) throw error
        searchResults = results
    })
    let query2 = 'SELECT feedback_rating, feedback_comment, username FROM service inner join requests on service.id = requests.sid inner join req_case on requests.caseid = req_case.id \
                    inner join homeowner on homeowner.natid = requests.natid where sid = ?' ;
    connection.query(query2, [serviceId], (error, results2) => {
        if (error) throw error
        searchResults = searchResults.concat(results2)
        res.status(200).send(JSON.parse(JSON.stringify(searchResults)))
    })
    
}

//TO DO search
const searchService = (req, res) => {
    console.log('searching service')
    console.log(req.body)

    var service_type = req.body['service_type'].trim()
    var contractor_name = req.body['con_name'].trim()
    var contractor_fda = req.body['con_fda'].trim()
    var lat1 = parseFloat(req.body['lat1'])
    var lon1= parseFloat(req.body['lon1'])

    console.log(lat1, lon1)
    // console.log(service_type ,contractor_name ,contractor_fda)
    let searchResults = []
    
    let query0 = 'ALTER TABLE contractor change COLUMN rating crating float(3,2)'
    connection.query(query0, (error, results) => {
        if (error) throw error
    })

    let query2 = 'ALTER TABLE contractor change COLUMN photo_url c_photo_url varchar(500)'
    connection.query(query2, (error, results) => {
        if (error) throw error
    })

    let query = 'select * from service inner join contractor on service.contractor_id= contractor.fda_cer inner join service_type_map on service.serv_type = service_type_map.type_id  \
                WHERE type_name LIKE ?"%" and comp_name LIKE ?"%" and fda_cer LIKE ?"%"';    
    connection.query(query,[service_type, contractor_name, contractor_fda], (error, results) => {
        if (error) throw error
        console.log(query)
        results.forEach(service => {
            const lat2 = service['latitude']
            const lon2 = service['longitude']
            console.log('loop',lat2,lon2)
            const dst = calDistance.distance(lat1, lon1,lat2, lon2)
            console.log(dst)
            service['distance'] = dst
            searchResults.push(service)
        });
        res.status(200).send(JSON.parse(JSON.stringify(searchResults)))
    })

    let query1 = 'ALTER TABLE contractor change COLUMN crating rating float(3,2)'
    connection.query(query1, (error, results) => {
        if (error) throw error
    })

    let query3 = 'ALTER TABLE contractor change COLUMN c_photo_url photo_url varchar(500)'
    connection.query(query3, (error, results) => {
        if (error) throw error
    })

}

const sortService = (req,res) => {
    console.log('sorting service')
    console.log(req.body)
    var sortParam = req.body['sortParam'].trim()
    var order = req.body['order'].trim()
    console.log(sortParam, order)
    var query = 'SELECT * FROM service ORDER BY ' + sortParam + ' ' + order
    connection.query(query, (error, results) =>{
        if(error) throw error
       // console.log(results)
       res.status(200).send(JSON.parse(JSON.stringify(results)))
    })

}

//----------------------------
const addService = (req,res)=>{
    var input = req.body;
    console.log(input);
    var contractor_id = input['contractor_id'];
    console.log(contractor_id);
    var descr = input['descr'];
    var serv_type = input['serv_type'];
    var price = parseFloat(input['price']).toFixed(2);
    var warranty = input['warranty'];
    var daily_limt = parseInt(input['daily_limit'], 10)
    var start_time = input['start_time'];
    var end_time = input['end_time'];
    console.log(price, daily_limt)
    //var avail_time = input['avail_time'].trim();
    //var photo_url = input['photo_url'].trim();
    console.log('[userController]...Try AddService');

    let query = `insert into service(contractor_id, serv_type, price, descr, warranty,daily_limit,start_time,end_time) values ("${contractor_id}", "${serv_type}", ${price},"${descr}",${warranty}, ${daily_limt}, "${start_time}", "${end_time}");`
    connection.query(query,(err,result)=>{
        if(err) throw err;
        console.log('[userController]...AddService!!!');
        let query2 = `SELECT MAX(id) as service_id FROM service`
        connection.query(query2, (err2, result2) =>{
            if(err2) throw err2
            res.send([{status: "success"}].concat(result2))
        })
    })

    // In case you want check
    // let check = `select * from service where contractor_id = ${contractor_id};`
    // connection.query(check,(err,res)=>{
    //     if(err) throw err;
    //     //already has this service
    //     if(res.length!=0){
    //         console.log('[userController]...Duplicate service');
    //     }
    //     else{
    //         connection.query(query,(err,res)=>{
    //             console.log('[userController]...AddService!!!');
    //         })
    //     }
    // })
}

const editService = (req,res)=>{
    var input = req.body;
    var id = input['id'];
    var contractor_id = input['contractor_id'];
    var descr = input['descr'];
    var serv_type = input['serv_type'];
    var price = parseFloat(input['price']).toFixed(2);
    var start_time = input['start_time'];
    var end_time = input['end_time'];
    var warranty = input['warranty'];
    var daily_limt = parseInt(input['daily_limit'], 10)

    let query = `UPDATE service SET \
                contractor_id = COALESCE(NULLIF("${contractor_id}",""), contractor_id), \
                descr = COALESCE(NULLIF("${descr}",""), descr), \
                serv_type = COALESCE(NULLIF(${serv_type},""), serv_type),\
                price = ${price}, \
                start_time = COALESCE(NULLIF("${start_time}",""), start_time), \
                end_time = COALESCE(NULLIF("${end_time}",""), end_time),\
                warranty = COALESCE(NULLIF("${warranty}",""), warranty), \
                daily_limit = ${daily_limt}
                WHERE id = ${id}`

    connection.query(query,(err,result)=>{
        console.log(query)
        if (err) throw err
        console.log('service edited!!!')
        res.send({status: "success"})
    })

}

module.exports = {getAllServices, getServiceDetail, searchService, sortService, addService, editService}