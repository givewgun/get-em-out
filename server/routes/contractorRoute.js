const Router = require('express')
const router = new Router()

const controller = require('../controllers/contractorController')
//get all contractor
router.get('/all', controller.getAllcontractors)

//get specific contractor
router.get('/get_detail/:id', controller.getContractorDetail)

//search contractor
router.post('/search', controller.searchContractor)
module.exports = router