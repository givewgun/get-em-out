const Router = require('express')
const router = new Router()

const controller = require('../controllers/adminController')

router.post('/confirm', controller.confirm)
module.exports = router