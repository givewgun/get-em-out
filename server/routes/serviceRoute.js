const Router = require('express')
const router = new Router()

const serviceController = require('../controllers/serviceController')
const upload = require('../controllers/upload')
//get all service
router.get('/all', serviceController.getAllServices)

//get specific service
router.get('/get_detail/:id', serviceController.getServiceDetail)

//search service
router.post('/search', serviceController.searchService)

//sort service
router.post('/sort', serviceController.sortService)

//add service
router.post('/addService',serviceController.addService)

router.post('/editservice', serviceController.editService)

router.post('/upload', upload.service_upload)

module.exports = router