const Router = require('express')
const router = new Router()

const controller = require('../controllers/boookingController')

router.post('/request', controller.requestService)
router.post('/checkAvailable', controller.checkAvailable)
router.post('/cancel', controller.cancelRequest)
router.post('/accept', controller.acceptRequest)
router.post('/reject', controller.rejectRequest)
router.post('/getAllRequest', controller.getAllRequest)
router.post('/getRequestDetail', controller.getRequestDetail)
router.post('/serviced', controller.serviceComplete)
module.exports = router