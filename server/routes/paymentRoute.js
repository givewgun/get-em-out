const express = require('express')
const router = new express()

const paymentController = require('../controllers/paymentController')

router.post('/pay',paymentController.pay)
router.post('/receipt',paymentController.getReceipt)
router.post('/getDepositCost',paymentController.getDepositCost)
router.post('/getRemainingCost',paymentController.getRemainingCost)

module.exports = router