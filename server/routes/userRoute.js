const Router = require('express')
const router = new Router()
const path = require('path')
const upload = require('../controllers/upload');
userController = require('../controllers/userController');

router.post('/register/:user_type', userController.register)

router.post('/login', userController.check_password)

router.post('/edit_profile', userController.editProfile)

router.get('/confirmation/:token', userController.confirm)

router.post('/upload/profile/:user_type', upload.profile_upload)

router.post('/upload/idcard' , upload.idcard_upload)

router.post('/upload/fdacer', upload.fdacer_upload)

router.post('/upload/service', upload.service_upload)
module.exports = router