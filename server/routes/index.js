const Router = require('express')
const router = new Router()
const path = require('path')

router.get('/', (req, res) => {
    console.log('going to homepage')
    res.json('Test Route')
})

router.get('/advance_search', (req, res) => {
    console.log('going to advance_search page')
    console.log(__dirname)
    res.sendFile(path.join(__dirname, '../', '/client/public/advance_search.html'))
    
})

router.get('/test_back', (req, res) => {
    console.log('for testing')
    console.log(__dirname)
    res.sendFile(path.join(__dirname, '../', '/test/back_test.html'))
    
})



module.exports = router
