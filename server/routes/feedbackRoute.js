const Router = require('express')
const router = new Router()

const controller = require('../controllers/feedbackController')

router.post('/giveFeedback', controller.giveFeedback)
module.exports = router