const Router = require('express')
const router = new Router()

const controller = require('../controllers/homeownerController')


router.post('/getHomeownerDetail', controller.getHomeownerDetail)
module.exports = router