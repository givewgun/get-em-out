//require dependencies
const express = require('express')
const bodyParser = require('body-parser')
const expressJwt = require('express-jwt')
const cors = require('cors')
const app = express()
const router = express.Router()

const swaggerUi = require('swagger-ui-express')
const YAML = require('yamljs')
const swaggerDocument = YAML.load('./swagger.yaml')

const port = process.env.PORT || 4200

const connection = require('./config/dbconnect')

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//define a route, usually this would be a bunch of routes imported from another file
app.use('/', require('./routes/index.js'))
app.use('/contractor', require('./routes/contractorRoute'))
app.use('/homeowner', require('./routes/homeownerRoute'))
app.use('/service', require('./routes/serviceRoute'))
app.use('/feedback', require('./routes/feedbackRoute'))
app.use('/user', require('./routes/userRoute') )
app.use('/admin', require('./routes/adminRoute') )
app.use('/booking', require('./routes/bookingRoute') )
app.use('/payment',require('./routes/paymentRoute'))

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use(express.static('public'))


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
//log to console to let us know it's working
console.log('Kushy API server started on: ' + port);