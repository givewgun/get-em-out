import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import WholeSearchAndResult from "./component/WholeSearchAndResult"
import ContractorDetailPage from "./component/ContractorDetailPage"
import ServiceDetailPage from "./component/ServiceDetailPage"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import Home from "./Page/home"
import Login from "./Page/Login"
import Registration from "./Page/Registration"
import AboutUs from "./Page/AboutUs"
import ThankAndEmail from "./Page/ThankAndEmail"
import VerifyEmail from "./Page/Verify"
import Admin from "./Page/Admin"
import Profile from "./Page/Profile"
import RequestDashboard from "./Page/RequestDashboard"
import CaseDetail from "./Page/CaseDetail"
import Booking from "./Page/Booking"
import Payment from "./Page/payment"
import EditHome from "./Page/EditHome"
import EditCon from "./Page/EditCon"
import Receipt from "./Page/Receipt"


import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';

library.add(fab, fas)
class App extends Component {
  //sessionStorage to change login navbar to profile navbar

  constructor() {
    super();
    this.setNavbarLoggedinUserType = this.setNavbarLoggedinUserType.bind(this);
    this.logoutMember = this.logoutMember.bind(this);

    this.state = {
      navbarLoggedinUserType: localStorage.getItem('userType')
    }
  }

  setNavbarLoggedinUserType(userType) {
    this.setState({ navbarLoggedinUserType: userType });
  }

  async logoutMember() {
    localStorage.clear();
    sessionStorage.clear();
    this.setNavbarLoggedinUserType('');
    window.location.reload();
    window.location.href = "/Login";

  }

  render() {
    return (
      <Router>
        <div className="App">
          {console.log(window.location)}
          <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">Get 'em Out</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/advance_search">Search</Nav.Link>
                <Nav.Link href="/AboutUs">About Us</Nav.Link>
              </Nav>

              { (this.state.navbarLoggedinUserType === 'homeowner' || this.state.navbarLoggedinUserType === 'contractor') ?

                (<Nav inline>
                  <Nav.Link href="/Profile">{localStorage.getItem('username')}</Nav.Link>
                  <Nav.Link href="/RequestDashboard">Dashboard</Nav.Link>
                  <Nav.Link onClick={this.logoutMember}>Log out</Nav.Link>

                </Nav>)
                :
                (
                   (this.state.navbarLoggedinUserType ==='Admin' || window.location.pathname==='/Admin') ?
                    (<Nav inline>
                      <Nav.Link href="/Admin">Admin</Nav.Link>
                      <Nav.Link onClick={this.logoutMember}>Log out</Nav.Link>
                    </Nav>)
                  :
                    (<Nav inline>
                      <Nav.Link href="/Register">Register</Nav.Link>
                      <Nav.Link href="/Login">Login</Nav.Link>
                    </Nav>)
                )
              }

            </Navbar.Collapse>
          </Navbar>
          <div className="Content">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/advance_search" component={WholeSearchAndResult} />
              <Route path="/contractor/detail/" component={ContractorDetailPage} />
              <Route path="/service/detail/" component={ServiceDetailPage} />
              <Route path="/Login" render={(props) => <Login {...props} setNavbarLoggedinUserType={this.setNavbarLoggedinUserType} />} />
              <Route path="/Register" component={Registration} />
              <Route path="/AboutUs" component={AboutUs} />
              <Route path="/RegisterDone" component={ThankAndEmail} />
              <Route path="/Verified" component={VerifyEmail} />
              <Route path="/Admin" component={Admin} />
              <Route path="/Profile" component={Profile} />
              <Route path="/RequestDashboard" component={RequestDashboard} />
              <Route path="/CaseDetail" component={CaseDetail} />
              <Route path="/Booking" component={Booking} />
              <Route path="/Payment" component={Payment} />
              <Route path="/EditHome" component={EditHome} />
              <Route path="/EditCon" component={EditCon} />
              <Route path="/Receipt" component={Receipt} />

            </Switch>
          </div>

        </div>

      </Router>

    );
  }
}

export default App;
