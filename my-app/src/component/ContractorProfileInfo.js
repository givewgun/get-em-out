import React, { Component } from "react";
import { Button, Card, Container, Row, Col,Image } from "react-bootstrap"
import Img from 'react-image';
import LocationPicker from 'react-location-picker';
//each variable in here is to be checked with the vars from back-end
export class ContractorProfileInfo extends Component {
    constructor() {
        super();
        this.state = {
            address: "Bangkok, Thailand",
            position: {
                lat: 0,
                lng: 0
            }
        };
        this.handleLocationChange = this.handleLocationChange.bind(this);    
    }
    
    handleLocationChange({ position, address }) {
        this.setState({ position, address });
    }

    render() {
        return (
            <div id="item" align="center">
                <Card style={{ width: '100%' }}>
                    <Container align="left">
                        <Row >
                            <Col xl={4} >
                                <Col align="center" style={{ marginTop: '10%' }}>
                                <Image class="Infoimage" width={250} src={this.props.ImageUrl} rounded/>
                                </Col>
                            </Col>
                            <Col xl={4} >
                                <Card.Body>
                                    <Card.Title>{this.props.name} <small>{this.props.fda_cer}</small> </Card.Title>
                                    <Card.Text>

                                        <Row>
                                            <Col> Tax id : {this.props.tax_id}</Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Phone : {this.props.phone}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Email : {this.props.email}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Rating : {this.props.rating}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col> Address :  {this.props.addr}</Col>
                                        </Row>


                                    </Card.Text>
                                    
                                </Card.Body>
                            </Col>
                            <Col xl={4}>
                                <LocationPicker
                                    containerElement={<div style={{ width: '100%', height: '100%' }} />}
                                    mapElement={<div style={{ height: '400px' }} />}
                                    defaultPosition={{lat: this.props.lat, lng: this.props.lng}}
                                    onChange={this.handleLocationChange}
                                />
                            </Col>
                        </Row>
                    </Container>
                </Card>
            </div>
        );
    }
}


export default ContractorProfileInfo;