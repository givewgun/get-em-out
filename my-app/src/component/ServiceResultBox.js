import React, { Component } from "react";
import ServiceSearchResult from "./ServiceSearchResult"
import {CardDeck,Container} from 'react-bootstrap';
export class ServiceResultBox extends Component {
  render() {
    // console.log("from cbox");
    // console.log(this.props.searchResults);
    // console.log("from cbox");

    return (
      <div id="ResultBox">

            {this.props.searchResults.length ? (
              <CardDeck id="servResultCardDeck">
              <ServiceSearchResult searchResults={this.props.searchResults} />
              </CardDeck>
            ) : (
              <div style={noSearchResultMessageStyle}>
                <p> Sorry,</p>
                <p> We couldn't find any service that matches your search.</p>
              </div>
            )}
        {/*<div id="sortBarContainer">
          <h1> sort bar goes here</h1>
        </div>

        {/* check if the search results if empty or not 
        if (notEmpty) -> show searchResults Class
        if (empty)    -> show no results message */}

        
      </div>
    );
  }
}
const noSearchResultMessageStyle = {
  textAlign: "center",
  marginTop: "70px",
  marginBottom: "120px",
  fontSize: "50px",
  fontWeight: "bold"
};
export default ServiceResultBox;