import React, { Component } from "react";
import { Button, Form, Row, Col, Image,Breadcrumb } from 'react-bootstrap';
import LocationPicker from 'react-location-picker';

const axios = require("axios");
const defaultPosition = {
    lat: 13.738735,
    lng: 100.529781
};

export class HomeownerRegisterForm extends Component {
    constructor() {
        super();
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onPictureSubmit = this.onPictureSubmit.bind(this);
        this.profilePictureChange = this.profilePictureChange.bind(this);
        this.natIDPictureChange = this.natIDPictureChange.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.natIDChange = this.natIDChange.bind(this);
        this.firstnameChange = this.firstnameChange.bind(this);
        this.lastnameChange = this.lastnameChange.bind(this);
        this.addrChange = this.addrChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.phoneChange = this.phoneChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);

        this.validateUsername = this.validateUsername.bind(this);       //return 0 if OK
        this.validatePassword = this.validatePassword.bind(this);       //else return 1
        this.validateNatID = this.validateNatID.bind(this);
        this.validateFirstname = this.validateFirstname.bind(this);
        this.validateLastname = this.validateLastname.bind(this);
        this.validateAddr = this.validateAddr.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.validatePhone = this.validatePhone.bind(this);
        this.validateAllField = this.validateAllField.bind(this);

        this.validateProfilePicture = this.validateProfilePicture.bind(this);
        this.validateNatIDPicture = this.validateNatIDPicture.bind(this);

        this.state =
            {
                page: 0,
                username: "",
                password: "",
                NatID: "",
                firstname: "",
                lastname: "",
                addr: "",
                email: "",
                phone: "",
                latitude: 13.738735,
                longitude: 100.529781,
                photo_url: "",
                profilePicture: null,
                profilePictureURL: null,
                natIDPicture: null,
                natIDPictureURL: null,
                isUpload: 0,

                usernameValid: 0,   //0 mean Valid. Other numbers are specific error codes
                passwordValid: 0,   //1 is empty. 2 is invalid. 3 is duplicated.
                natIDValid: 0,
                firstnameValid: 0,
                lastnameValid: 0,
                addrValid: 0,
                emailValid: 0,
                phoneValid: 0,

                profilePictureValid: 0, //0 OK, 1 null, 2 invalid type, 3 file size too large
                natIDPictureValid: 0
            };
    }

    async onFormSubmit() {
        console.log(this.state);
        if (this.validateAllField() == 0) {
            try {
                const data = this.state;
                const response = await fetch("/user/register/1", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                });
                const results = await response.json();
                console.log(results);
                if (results.status == "success") {
                    this.setState({ page: 1 });
                } else {
                    if (results.status == "username taken") {
                        this.setState({ usernameValid: 3 }); //Error code 3 mean Username already taken
                    }
                    if (results.status == "duplicate ID") {
                        this.setState({ natIDValid: 3 }); //Error code 3 mean natID already registered
                    }
                    alert("Error. Please check your form information");
                }
            } catch (error) {
                console.log("Form Submission Failed", error);
            }
        } else {
            alert("Error. Please check your form information");
        }
    }

    async onPictureSubmit() {
        if (this.validateAllPicture() == 0) {
            try {
                const formData = new FormData();
                formData.set('NatID', this.state.NatID);
                formData.append('profile_image', this.state.profilePicture);
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    },
                };
                axios.post("/user/upload/profile/1", formData, config)
                    .then((response) => {
                        alert("The file is successfully uploaded");
                    }).catch((error) => {
                    });
            } catch (error) {
                console.log("ProfilePic Submission Failed", error);
            }
            try {
                const formData = new FormData();
                formData.set('NatID', this.state.NatID);
                formData.append('idcard_image', this.state.natIDPicture);
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    },
                };
                axios.post("/user/upload/idcard", formData, config)
                    .then((response) => {
                        alert("The file is successfully uploaded");
                    }).catch((error) => {
                    });
            } catch (error) {
                console.log("NatIDPic Submission Failed", error);
            }
            //window.location.href = "/";
            this.setState({ isUpload: 1 })
        } else {
            alert("Error. Please check your selected picture.");
        }
    }

    async profilePictureChange(event) {
        console.log(event.target.files[0]);
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                profilePicture: event.target.files[0],
                profilePictureURL: URL.createObjectURL(event.target.files[0])
            }, () => this.validateProfilePicture());
        } else {
            this.setState({
                profilePicture: null,
                profilePictureURL: null
            }, () => this.validateProfilePicture());
        }
    }

    async natIDPictureChange(event) {
        console.log(event.target.files[0]);
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                natIDPicture: event.target.files[0],
                natIDPictureURL: URL.createObjectURL(event.target.files[0])
            }, () => this.validateNatIDPicture());
        } else {
            this.setState({
                natIDPicture: null,
                natIDPictureURL: null
            }, () => this.validateNatIDPicture());
        }
    }

    async usernameChange(event) {
        this.setState({
            username: event.target.value.trim()
        }, () => this.validateUsername());
    }

    async passwordChange(event) {
        this.setState({
            password: event.target.value
        }, () => this.validatePassword());
    }

    async natIDChange(event) {
        this.setState({
            NatID: event.target.value.trim()
        }, () => this.validateNatID());
    }

    async firstnameChange(event) {
        this.setState({
            firstname: event.target.value.trim()
        }, () => this.validateFirstname());
    }
    async lastnameChange(event) {
        this.setState({
            lastname: event.target.value.trim()
        }, () => this.validateLastname());
    }
    async addrChange(event) {
        this.setState({
            addr: event.target.value.trim()
        }, () => this.validateAddr());
    }
    async emailChange(event) {
        this.setState({
            email: event.target.value.trim()
        }, () => this.validateEmail());
    }
    async phoneChange(event) {
        this.setState({
            phone: event.target.value.trim()
        }, () => this.validatePhone());
    }

    handleLocationChange({ position, address }) {
        this.setState({ latitude: position.lat, longitude: position.lng });
    }

    validateUsername() {
        let input = this.state.username;
        let regex = /^\S{1,15}$/
        if (regex.test(input)) {
            this.setState({
                usernameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    usernameValid: 1
                });
            } else {
                this.setState({
                    usernameValid: 2
                });
            }
            return 1;
        }
    }

    validatePassword() {
        let input = this.state.password;
        let regex = /^.{8,20}$/
        if (regex.test(input)) {
            this.setState({
                passwordValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    passwordValid: 1
                });
            } else {
                this.setState({
                    passwordValid: 2
                });
            }
            return 1;
        }
    }

    validateNatID() {
        let input = this.state.NatID;
        let regex = /^[0-9]{13}$/
        if (regex.test(input)) {
            let nid = input;
            let multiplierConst = 13;
            let sum = 0;
            parseInt(nid, 10);
            for (let i = 0; i < 12; i++) {
                sum += multiplierConst * parseInt(nid[i], 10);
                multiplierConst--;
            }
            if (parseInt(nid[12], 10) == (11 - (sum % 11)) % 10) {
                this.setState({
                    natIDValid: 0
                });
                return 0;
            } else {
                this.setState({
                    natIDValid: 2
                });
                return 1;
            }

        } else {

            if (input.length == 0) {
                this.setState({
                    natIDValid: 1
                });
            } else {
                this.setState({
                    natIDValid: 2
                });
            }
            return 1;
        }
    }

    validateFirstname() {
        let input = this.state.firstname;
        let regex = /^[^0-9]{1,50}$/
        if (regex.test(input)) {
            this.setState({
                firstnameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    firstnameValid: 1
                });
            } else {
                this.setState({
                    firstnameValid: 2
                });
            }
            return 1;
        }
    }

    validateLastname() {
        let input = this.state.lastname;
        let regex = /^[^0-9]{1,50}$/
        if (regex.test(input)) {
            this.setState({
                lastnameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    lastnameValid: 1
                });
            } else {
                this.setState({
                    lastnameValid: 2
                });
            }
            return 1;
        }
    }

    validateAddr() {
        let input = this.state.addr;
        let regex = /^.{1,150}$/
        if (regex.test(input)) {
            this.setState({
                addrValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    addrValid: 1
                });
            } else {
                this.setState({
                    addrValid: 2
                });
            }
            return 1;
        }
    }

    validateEmail() {
        let input = this.state.email;
        let regex = /\S+@\S+\.\S+/
        if (regex.test(input)) {
            this.setState({
                emailValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    emailValid: 1
                });
            } else {
                this.setState({
                    emailValid: 2
                });
            }
            return 1;
        }
    }

    validatePhone() {
        let input = this.state.phone;
        let regex = /^[0-9]{9,10}$/     //regex for allowing only number charactor of length 9 to 10
        if (regex.test(input)) {
            this.setState({
                phoneValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    phoneValid: 1
                });
            } else {
                this.setState({
                    phoneValid: 2
                });
            }
            return 1;
        }
    }

    validateAllField() {
        return (this.validateUsername() + this.validatePassword() + this.validateNatID() + this.validateFirstname() + this.validateLastname() + this.validateAddr() + this.validateEmail() + this.validatePhone()) == 0 ? 0 : 1;
    }

    validateProfilePicture() {
        let input = this.state.profilePicture;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension

        if (input === null) {
            this.setState({
                profilePictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        profilePictureValid: 0
                    });
                    return 0
                } else {
                    this.setState({
                        profilePictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    profilePictureValid: 2
                });
                return 1;
            }
        }
    }

    validateNatIDPicture() {
        let input = this.state.natIDPicture;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension

        if (input === null) {
            this.setState({
                natIDPictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        natIDPictureValid: 0
                    });
                    return 0
                } else {
                    this.setState({
                        natIDPictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    natIDPictureValid: 2
                });
                return 1;
            }
        }
    }

    validateAllPicture() {
        return (this.validateProfilePicture() + this.validateNatIDPicture()) == 0 ? 0 : 1;
    }

    render() {

        return (
            <div>

                {this.state.page === 0 ? (
                    <div>
                        <Breadcrumb>
                            <Breadcrumb.Item>select user type</Breadcrumb.Item>
                            <Breadcrumb.Item active>fill the information</Breadcrumb.Item>
                            <Breadcrumb.Item>Upload your profile</Breadcrumb.Item>
                            <Breadcrumb.Item>Confirm</Breadcrumb.Item>
                        </Breadcrumb>
                        <h1 style={format}>Register as Homeowner</h1>
                        <Form style={format}>
                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridUsername">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control type="text" placeholder="Enter username" onChange={this.usernameChange} className={`form-control ${this.state.usernameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.usernameValid == 1 ? "Please enter your username." : this.state.usernameValid == 2 ? "Username must be at most 15 characters with no space in between." : "This username has already been taken."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" onChange={this.passwordChange} className={`form-control ${this.state.passwordValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.passwordValid == 1 ? "Please enter your password." : "Password must be between 8 to 20 characters."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 10 }} controlId="formGridNatID">
                                    <Form.Label>National ID</Form.Label>
                                    <Form.Control type="text" onChange={this.natIDChange} className={`form-control ${this.state.natIDValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.natIDValid == 1 ? "Please enter your national identification number." : this.state.natIDValid == 2 ? "Invalid ID entered. Please check." : "This ID already registered."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>


                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridFirstName">
                                    <Form.Label>Firstname</Form.Label>
                                    <Form.Control type="text" onChange={this.firstnameChange} className={`form-control ${this.state.firstnameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.firstnameValid == 1 ? "What’s your name?" : "Invalid name entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridLastName">
                                    <Form.Label>Lastname</Form.Label>
                                    <Form.Control type="text" onChange={this.lastnameChange} className={`form-control ${this.state.lastnameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.lastnameValid == 1 ? "What’s your last name?" : "Invalid last name entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 10 }} controlId="formGridAddress">
                                    <Form.Label>Address</Form.Label>
                                    <Form.Control type="text" onChange={this.addrChange} className={`form-control ${this.state.addrValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.addrValid == 1 ? "Please fill your address." : "Entered address is too long."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control type="email" onChange={this.emailChange} className={`form-control ${this.state.emailValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.emailValid == 1 ? "Please enter your email." : "Invalid email entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPhone">
                                    <Form.Label>Phone</Form.Label>
                                    <Form.Control type="tel" onChange={this.phoneChange} className={`form-control ${this.state.phoneValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.phoneValid == 1 ? "Please enter your phone number" : "Invalid phone number entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Group controlId="formGridPosition">
                                <Form.Label style={format2}>Pin your position</Form.Label>
                                <LocationPicker
                                    containerElement={<div style={{ marginLeft: "auto", marginRight: "auto", width: '80%', height: '100%' }} />}
                                    mapElement={<div style={{ height: '400px' }} />}
                                    defaultPosition={defaultPosition}
                                    onChange={this.handleLocationChange}
                                />
                            </Form.Group>
                            <Form.Row>
                                <Button as={Col} md={{ offset: 8, span: 2 }} variant="primary" onClick={this.onFormSubmit}>Next</Button>
                            </Form.Row>
                            <br />

                        </Form>
                    </div>
                ) : (
                        <div>
                            <Breadcrumb>
                                <Breadcrumb.Item>select user type</Breadcrumb.Item>
                                <Breadcrumb.Item>fill the information</Breadcrumb.Item>
                                <Breadcrumb.Item active>Upload your profile</Breadcrumb.Item>
                                <Breadcrumb.Item>Confirm</Breadcrumb.Item>
                            </Breadcrumb>
                            <h1 style={format}>Register as Homeowner</h1>
                            <Form style={format}>
                                <Form.Group controlId="formGridProfilePic">
                                    <Form.Label>Profile Picture</Form.Label>
                                    <Form.Control type="file" onChange={this.profilePictureChange} className={`form-control ${this.state.profilePictureValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.profilePictureValid == 1 ? "Please choose your profile picture." : this.state.profilePictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                    {this.state.profilePictureValid == 0 ? <Image id="profilePicture" src={this.state.profilePictureURL}></Image> : null}
                                </Form.Group>
                                <Form.Group controlId="formGridNatIDPic">
                                    <Form.Label>National ID Picture</Form.Label>
                                    <Form.Control type="file" onChange={this.natIDPictureChange} className={`form-control ${this.state.natIDPictureValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.natIDPictureValid == 1 ? "Please choose your national ID card picture." : this.state.natIDPictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                    {this.state.natIDPictureValid == 0 ? <Image id="natIDPicture" src={this.state.natIDPictureURL}></Image> : null}
                                </Form.Group>
                                <Form.Row>
                                    <Col md={{ offset: 6, span: 2 }}>
                                        <Button variant="secondary" onClick={this.onPictureSubmit}>Upload</Button>
                                    </Col>
                                    <Col md={{ offset: 1, span: 2 }}>
                                        {this.state.isUpload === 0 ? (
                                            <Button variant="primary" href="/RegisterDone" disabled>Submit</Button>
                                        ) : (
                                                <Button variant="primary" href="/RegisterDone" >Submit</Button>
                                            )
                                        }
                                    </Col>

                                    <br />
                                </Form.Row>
                            </Form>
                        </div>
                    )
                }

            </div>
        );
    }
}




const current = {
    color: "blue"
}

const format = {
    paddingLeft: "2%",
    paddingRight: "2%"
}
const format2 = {
    paddingLeft: "10%",

}

export default HomeownerRegisterForm;