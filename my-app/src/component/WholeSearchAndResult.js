import React, { Component } from "react";
import ContractorResultBox from "./ContractorResultBox";
import ContractorSearchBox from "./ContractorSearchBox";
import ServiceResultBox from "./ServiceResultBox";
import ServiceSearchBox from "./ServiceSearchBox";
import { Container } from 'react-bootstrap';
//import { Container } from "react-bootstrap";
export class WholeSearchAndResult extends Component {
  constructor() {
    super();
    // mockup courses from searchResults
    this.state = {
      contractorSearchResults: [],
      serviceSearchResults: [],
      searchFor : 0  //1 for searching contractor, 2 for searching service
    };
  }

  // functions -> need to be passed via props to inner components

  // upDateSearchResults (parameters) then call backend

  // SearchBox.js will trigger upDateSearchResults 
  upDateContractorSearchResults = e => {
    this.setState({
      contractorSearchResults: e,
      searchFor : 1
    });
  };
  upDateServiceSearchResults = e => {
    this.setState({
      serviceSearchResults: e,
      searchFor : 2
    });
  };


  render() {
    console.log(this.state);
    let searchScreen;
    if(this.state.searchFor === 1) 
      searchScreen =  <ContractorResultBox searchResults={this.state.contractorSearchResults} />
    else if(this.state.searchFor === 2)
      searchScreen =  <ServiceResultBox searchResults={this.state.serviceSearchResults} />
    else if(this.state.searchFor === 0)
      searchScreen = <div id="defaultMsg" style={defaultMessageStyle}>
      <p> Fill a form and press 'Search'</p>
      <p> to search the contractor or service you want.</p>
    </div>

    return (
      
      <div id="wholeContainer" class="Page">
              <div id="searchContainer">
                <div id="contractorSearchContainer">
                <ContractorSearchBox upDateContractorSearchResults={this.upDateContractorSearchResults}/>
                </div>
                <div id="serviceSearchContainer">
                <ServiceSearchBox upDateServiceSearchResults={this.upDateServiceSearchResults}/>
                </div>
              </div> 
              <br></br>
        <div id="resultsContainer">
           {searchScreen}
        </div>
      </div>
    );
  }
}
const defaultMessageStyle = {
  textAlign: "center",
  marginTop: "70px",
  //marginBottom: "120px",
  fontSize: "50px",
  fontWeight: "bold"
};

export default WholeSearchAndResult;