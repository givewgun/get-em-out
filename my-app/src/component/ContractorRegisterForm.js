import React, { Component } from "react";
import { Button, Form, Row, Col, Image,Breadcrumb } from 'react-bootstrap';
import LocationPicker from 'react-location-picker';

const axios = require("axios");
const defaultPosition = {
    lat: 13.738735,
    lng: 100.529781
};

export class ContractorRegisterForm extends Component {
    constructor() {
        super();

        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onPictureSubmit = this.onPictureSubmit.bind(this);
        this.profilePictureChange = this.profilePictureChange.bind(this);
        this.FDACerPictureChange = this.FDACerPictureChange.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.FDACerChange = this.FDACerChange.bind(this);
        this.compnameChange = this.compnameChange.bind(this);
        this.taxIDChange = this.taxIDChange.bind(this);
        this.addrChange = this.addrChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.phoneChange = this.phoneChange.bind(this);
        this.bankaccountChange = this.bankaccountChange.bind(this);
        this.bankNameChange = this.bankNameChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.payByCreditChange = this.payByCreditChange.bind(this);
        this.payByBankChange = this.payByBankChange.bind(this);

        this.validateUsername = this.validateUsername.bind(this);       //return 0 if OK
        this.validatePassword = this.validatePassword.bind(this);       //else return 1
        this.validateFda_cer = this.validateFda_cer.bind(this);
        this.validateCompname = this.validateCompname.bind(this);
        this.validateTax_id = this.validateTax_id.bind(this);
        this.validateAddr = this.validateAddr.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.validatePhone = this.validatePhone.bind(this);
        this.validateBankaccount = this.validateBankaccount.bind(this);
        this.validateBankName = this.validateBankName.bind(this);
        this.validateAllField = this.validateAllField.bind(this);

        this.validateProfilePicture = this.validateProfilePicture.bind(this);
        this.validateFDACerPicture = this.validateFDACerPicture.bind(this);

        this.state =
            {
                page: 0,
                username: "",
                password: "",
                fda_cer: "",
                comp_name: "",
                tax_id: "",
                addr: "",
                email: "",
                phone: "",
                bank_account: "",
                bankname:"",
                latitude: 13.738735,
                longitude: 100.529781,
                photo_url: "",
                paymentByCredit: 0,
                paymentByBank: 0,
                profilePicture: null,
                profilePictureURL: null,
                FDACerPicture: null,
                FDACerPictureURL: null,
                isUpload: 0,

                usernameValid: 0,   //0 mean Valid. Other numbers are specific error codes
                passwordValid: 0,   //1 is empty. 2 is invalid. 3 is duplicated.
                fdacerValid: 0,
                compnameValid: 0,
                taxidValid: 0,
                addrValid: 0,
                emailValid: 0,
                phoneValid: 0,
                bankaccountValid: 0,
                banknameValid:0,

                profilePictureValid: 0, //0 OK, 1 null, 2 invalid type, 3 file size too large
                fdacerPictureValid: 0
            };
    }

    async onFormSubmit() {
        console.log(this.state);
        if (this.validateAllField() == 0) {
            try {
                const data = this.state;
                const response = await fetch("/user/register/0", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                });
                const results = await response.json();
                console.log(results);
                if (results.status == "success") {
                    this.setState({ page: 1 });
                } else {
                    if (results.status == "username taken") {
                        this.setState({ usernameValid: 3 }); //Error code 3 mean Username already taken
                    }
                    if (results.status == "duplicate ID") {
                        this.setState({ fdacerValid: 3 }); //Error code 3 mean fda_cer already registered
                    }
                    alert("Error. Please check your form information");
                }
            } catch (error) {
                console.log("Form Submission Failed", error);
            }
        } else {
            alert("Error. Please check your form information");
        }
    }

    async onPictureSubmit() {
        if (this.validateAllPicture() == 0) {
            try {
                //console.log(this.state);
                const formData = new FormData();
                formData.set('fda_cer', this.state.fda_cer);
                formData.append('profile_image', this.state.profilePicture);
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    },
                };
                axios.post("/user/upload/profile/0", formData, config)
                    .then((response) => {
                        alert("The file is successfully uploaded");
                    }).catch((error) => {
                    });
            } catch (error) {
                console.log("ProfilePic Submission Failed", error);
            }
            try {
                //console.log(this.state);
                const formData = new FormData();
                formData.set('fda_cer', this.state.fda_cer);
                formData.append('fdacer_image', this.state.FDACerPicture);
                const config = {
                    headers: {
                        'content-type': 'multipart/form-data'
                    },
                };
                axios.post("/user/upload/fdacer", formData, config)
                    .then((response) => {
                        alert("The file is successfully uploaded");
                    }).catch((error) => {
                    });
            } catch (error) {
                console.log("NatIDPic Submission Failed", error);
            }
            //window.location.href = "/";
            this.setState({ isUpload: 1 })
        } else {
            alert("Error. Please check your selected picture.");
        }
    }

    async profilePictureChange(event) {
        console.log(event.target.files[0]);
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                profilePicture: event.target.files[0],
                profilePictureURL: URL.createObjectURL(event.target.files[0])
            }, () => this.validateProfilePicture());
        } else {
            this.setState({
                profilePicture: null,
                profilePictureURL: null
            }, () => this.validateProfilePicture());
        }
    }

    async FDACerPictureChange(event) {
        console.log(event.target.files[0]);
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                FDACerPicture: event.target.files[0],
                FDACerPictureURL: URL.createObjectURL(event.target.files[0])
            }, () => this.validateFDACerPicture());
        } else {
            this.setState({
                FDACerPicture: null,
                FDACerPictureURL: null
            }, () => this.validateFDACerPicture());
        }
    }

    async usernameChange(event) {
        this.setState({
            username: event.target.value.trim()
        }, () => this.validateUsername());
    }

    async passwordChange(event) {
        this.setState({
            password: event.target.value
        }, () => this.validatePassword());
    }

    async FDACerChange(event) {
        this.setState({
            fda_cer: event.target.value.trim()
        }, () => this.validateFda_cer());
    }

    async compnameChange(event) {
        this.setState({
            comp_name: event.target.value.trim()
        }, () => this.validateCompname());
    }
    async taxIDChange(event) {
        this.setState({
            tax_id: event.target.value.trim()
        }, () => this.validateTax_id());
    }
    async addrChange(event) {
        this.setState({
            addr: event.target.value.trim()
        }, () => this.validateAddr());
    }
    async emailChange(event) {
        this.setState({
            email: event.target.value.trim()
        }, () => this.validateEmail());
    }
    async phoneChange(event) {
        this.setState({
            phone: event.target.value.trim()
        }, () => this.validatePhone());
    }
    async bankaccountChange(event) {
        this.setState({
            bank_account: event.target.value.trim()
        }, () => this.validateBankaccount());
    }
    async bankNameChange(event) {
        this.setState({
            bankname: event.target.value.trim()
        }, () => this.validateBankName());
    }

    async payByCreditChange(event) {
        this.setState({
            paymentByCredit: 1 - this.state.paymentByCredit
        }, function () { console.log(this.state.paymentByCredit); });
    }

    async payByBankChange(event) {
        this.setState({
            paymentByBank: 1 - this.state.paymentByBank
        }, function () { console.log(this.state.paymentByBank); });

    }

    handleLocationChange({ position, address }) {
        this.setState({ latitude: position.lat, longitude: position.lng });
    }

    validateUsername() {
        let input = this.state.username;
        let regex = /^\S{1,15}$/
        if (regex.test(input)) {
            this.setState({
                usernameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    usernameValid: 1
                });
            } else {
                this.setState({
                    usernameValid: 2
                });
            }
            return 1;
        }
    }

    validatePassword() {
        let input = this.state.password;
        let regex = /^.{8,20}$/
        if (regex.test(input)) {
            this.setState({
                passwordValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    passwordValid: 1
                });
            } else {
                this.setState({
                    passwordValid: 2
                });
            }
            return 1;
        }
    }

    validateFda_cer() {
        let input = this.state.fda_cer;
        let regex = /^.{1,15}$/
        if (regex.test(input)) {
            this.setState({
                fdacerValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    fdacerValid: 1
                });
            } else {
                this.setState({
                    fdacerValid: 2
                });
            }
            return 1;
        }
    }

    validateCompname() {
        let input = this.state.comp_name;
        let regex = /^.{1,60}$/
        if (regex.test(input)) {
            this.setState({
                compnameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    compnameValid: 1
                });
            } else {
                this.setState({
                    compnameValid: 2
                });
            }
            return 1;
        }
    }

    validateTax_id() {
        let input = this.state.tax_id;
        let regex = /^[0-9]{13}$/
        if (regex.test(input)) {
            this.setState({
                taxidValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    taxidValid: 1
                });
            } else {
                this.setState({
                    taxidValid: 2
                });
            }
            return 1;
        }
    }

    validateAddr() {
        let input = this.state.addr;
        let regex = /^.{1,150}$/
        if (regex.test(input)) {
            this.setState({
                addrValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    addrValid: 1
                });
            } else {
                this.setState({
                    addrValid: 2
                });
            }
            return 1;
        }
    }

    validateEmail() {
        let input = this.state.email;
        let regex = /\S+@\S+\.\S+/
        if (regex.test(input)) {
            this.setState({
                emailValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    emailValid: 1
                });
            } else {
                this.setState({
                    emailValid: 2
                });
            }
            return 1;
        }
    }

    validatePhone() {
        let input = this.state.phone;
        let regex = /^[0-9]{9,10}$/     //regex for allowing only number charactor of length 9 to 10
        if (regex.test(input)) {
            this.setState({
                phoneValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    phoneValid: 1
                });
            } else {
                this.setState({
                    phoneValid: 2
                });
            }
            return 1;
        }
    }

    validateBankName() {
        let input = this.state.banknameValid;
        let regex = /^.{1,150}$/
        if (regex.test(input)) {
            this.setState({
                banknameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    banknameValid: 1
                });
            } else {
                this.setState({
                    banknameValid: 2
                });
            }
            return 1;
        }
    }

    validateBankaccount() {
        let input = this.state.bank_account;
        let regex = /^[0-9]{10,15}$/
        if (regex.test(input)) {
            this.setState({
                bankaccountValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    bankaccountValid: 1
                });
            } else {
                this.setState({
                    bankaccountValid: 2
                });
            }
            return 1;
        }
    }

    validateAllField() {
        return (this.validateUsername() + this.validatePassword() + this.validateFda_cer() + this.validateCompname() + this.validateTax_id() + this.validateAddr() + this.validateEmail() + this.validatePhone() + this.validateBankaccount()) +this.validateBankName() == 0 ? 0 : 1;
    }

    validateProfilePicture() {
        let input = this.state.profilePicture;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension

        if (input === null) {
            this.setState({
                profilePictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        profilePictureValid: 0
                    });
                    return 0
                } else {
                    this.setState({
                        profilePictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    profilePictureValid: 2
                });
                return 1;
            }
        }
    }

    validateFDACerPicture() {
        let input = this.state.FDACerPicture;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension

        if (input === null) {
            this.setState({
                fdacerPictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        fdacerPictureValid: 0
                    });
                    return 0
                } else {
                    this.setState({
                        fdacerPictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    fdacerPictureValid: 2
                });
                return 1;
            }
        }
    }

    validateAllPicture() {
        return (this.validateProfilePicture() + this.validateFDACerPicture()) == 0 ? 0 : 1;
    }

    render() {

        return (
            <div>
                {this.state.page === 0 ? (
                    <div>                        
                        <Breadcrumb>
                            <Breadcrumb.Item>select user type</Breadcrumb.Item>
                            <Breadcrumb.Item active>fill the information</Breadcrumb.Item>
                            <Breadcrumb.Item>Upload your profile</Breadcrumb.Item>
                            <Breadcrumb.Item>Confirm</Breadcrumb.Item>
                        </Breadcrumb>
                        <h1 style={format}>Register as Contractor</h1>
                        <Form style={format}>
                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridUsername">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control type="text" placeholder="Enter username" onChange={this.usernameChange} className={`form-control ${this.state.usernameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.usernameValid == 1 ? "Please enter your username." : this.state.usernameValid == 2 ? "Username must be at most 15 characters with no space in between." : "This username has already been taken."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" onChange={this.passwordChange} className={`form-control ${this.state.passwordValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.passwordValid == 1 ? "Please enter your password." : "Password must be between 8 to 20 characters."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridFDACer">
                                    <Form.Label>FDA Certification number</Form.Label>
                                    <Form.Control type="text" onChange={this.FDACerChange} className={`form-control ${this.state.fdacerValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.fdacerValid == 1 ? "Please enter your FDA certification number." : this.state.fdacerValid == 2 ? "Invalid number entered. Please check." : "This ID already registered."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridTaxID">
                                    <Form.Label>Tax ID</Form.Label>
                                    <Form.Control type="text" onChange={this.taxIDChange} className={`form-control ${this.state.taxidValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.taxidValid == 1 ? "Please enter your Tax ID" : "Invalid Tax ID entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 10 }} controlId="formGridCompName">
                                    <Form.Label>Company name</Form.Label>
                                    <Form.Control type="text" onChange={this.compnameChange} className={`form-control ${this.state.compnameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.compnameValid == 1 ? "Please enter your company name" : "The entered name is too long"}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 10 }} controlId="formGridAddress">
                                    <Form.Label>Address</Form.Label>
                                    <Form.Control type="text" onChange={this.addrChange} className={`form-control ${this.state.addrValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.addrValid == 1 ? "Please fill your address." : "Entered address is too long."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control type="email" onChange={this.emailChange} className={`form-control ${this.state.emailValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.emailValid == 1 ? "Please enter your email." : "Invalid email entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPhone">
                                    <Form.Label>Phone</Form.Label>
                                    <Form.Control type="tel" onChange={this.phoneChange} className={`form-control ${this.state.phoneValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.phoneValid == 1 ? "Please enter your phone number" : "Invalid phone number entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridBankAccount">
                                    <Form.Label>Bank account number</Form.Label>
                                    <Form.Control type="text" onChange={this.bankaccountChange} className={`form-control ${this.state.bankaccountValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.bankaccountValid == 1 ? "Please enter your bank account number" : "Invalid bank account number entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridBankName">
                                    <Form.Label>Bank Name</Form.Label>
                                    <Form.Control type="text" onChange={this.bankNameChange} className={`form-control ${this.state.banknameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.banknameValid == 1 ? "Please enter your bank Name" : "Invalid bank Name entered. Please check."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>

                            <Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 2 }} controlId="formGridPayByCredit">
                                    <Form.Label>Payment: </Form.Label>
                                </Form.Group>
                                <Form.Group as={Col} md={{ span: 3 }} controlId="formGridPayByCredit">
                                    <input type="checkbox" checked={this.state.paymentByCredit} onChange={this.payByCreditChange} />
                                    <Form.Label>Credit card</Form.Label>

                                </Form.Group>
                                <Form.Group as={Col} md={{ span: 3 }} controlId="formGridPayByBank">
                                    <input type="checkbox" checked={this.state.paymentByBank} onChange={this.payByBankChange} />
                                    <Form.Label>Bank Transferring</Form.Label>

                                </Form.Group>
                            </Form.Row>
                            <Form.Group controlId="formGridPosition">
                                <Form.Label style={format2}>Pin your position</Form.Label>
                                <LocationPicker
                                    containerElement={<div style={{ marginLeft: "auto", marginRight: "auto", width: '80%', height: '100%' }} />}
                                    mapElement={<div style={{ height: '400px' }} />}
                                    defaultPosition={defaultPosition}
                                    onChange={this.handleLocationChange}
                                />
                            </Form.Group>
                            <Form.Row>
                                <Button as={Col} md={{ offset: 8, span: 2 }} variant="primary" onClick={this.onFormSubmit}>Next</Button>
                            </Form.Row>
                            <br />
                        </Form>
                    </div>
                ) : (
                        <div>
                            <Breadcrumb>
                                <Breadcrumb.Item>select user type</Breadcrumb.Item>
                                <Breadcrumb.Item>fill the information</Breadcrumb.Item>
                                <Breadcrumb.Item active>Upload your profile</Breadcrumb.Item>
                                <Breadcrumb.Item>Confirm</Breadcrumb.Item>
                            </Breadcrumb>
                            <h1>Register as Contractor</h1>
                            <Form>
                                <Form.Group controlId="formGridProfilePic">
                                    <Form.Label>Profile Picture</Form.Label>
                                    <Form.Control type="file" onChange={this.profilePictureChange} className={`form-control ${this.state.profilePictureValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.profilePictureValid == 1 ? "Please choose your profile picture." : this.state.profilePictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                    {this.state.profilePictureValid == 0 ? <Image id="profilePicture" src={this.state.profilePictureURL}></Image> : null}
                                </Form.Group>
                                <Form.Group controlId="formGridFDACerPic">
                                    <Form.Label>FDA Certification Picture</Form.Label>
                                    <Form.Control type="file" onChange={this.FDACerPictureChange} className={`form-control ${this.state.fdacerPictureValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.fdacerPictureValid == 1 ? "Please choose your FDA certificate picture." : this.state.fdacerPictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                    {this.state.fdacerPictureValid == 0 ? <Image id="natIDPicture" src={this.state.FDACerPictureURL}></Image> : null}
                                </Form.Group>
                                <Form.Row>
                                    <Col md={{ offset: 6, span: 2 }}>
                                        <Button variant="secondary" onClick={this.onPictureSubmit}>Upload</Button>
                                    </Col>
                                    <Col md={{ offset: 1, span: 2 }}>
                                        {this.state.isUpload === 0 ? (
                                            <Button variant="primary" href="/RegisterDone" disabled>Submit</Button>
                                        ) : (
                                                <Button variant="primary" href="/RegisterDone" >Submit</Button>
                                            )
                                        }
                                    </Col>
                                </Form.Row>
                                <br />
                            </Form>
                        </div>
                    )
                }
            </div>
        );
    }
}



const current = {
    color: "blue"
}

const format = {
    paddingLeft: "2%",
    paddingRight: "2%"
}
const format2 = {
    paddingLeft: "10%",

}


export default ContractorRegisterForm;