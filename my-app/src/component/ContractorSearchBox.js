import React, { Component } from "react";
import { Form, FormControl, Button,Col } from "react-bootstrap";
//import FormCheckInput from "react-bootstrap/FormCheckInput";

export class ContractorSearchBox extends Component {
  constructor() {
    super();
    this.onSearchSubmit=this.onSearchSubmit.bind(this);
    this.state = {
      search_name: "",
      fid: ""
    };
  }

  //capture keyword change in searchBar
  onSearchNameBarChange = e => {
    this.setState({ search_name: e.target.value });
    //console.log(this.state);
  };

  onSearchFIDBarChange = e => {
    this.setState({ fid: e.target.value });
    //console.log(this.state);
  };

  async onSearchSubmit() {
    try {
      const data = this.state;
      console.log(JSON.stringify(data));
      const response = await fetch("/contractor/search", { //not sure where to fetch from
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      //console.log(results);
      this.props.upDateContractorSearchResults(results);
    } catch (error) {
      console.log("Search failed", error);
    }
  };

  render() {  //wait for re-deco
    return (
      <div id = "ContractorSearchBox">
        <h5 class="mHead">Search Contractor </h5>

          <Form id="searchForm">
            <div class="Container">

              <Form.Row>
                <Col xl={6} md={6} xs={8}>
                  <FormControl
                   
                    id="search_name"
                    type="text"
                    placeholder="Name"
                    className="mr-sm-2"
                    onChange={this.onSearchNameBarChange}
                  />
                </Col>
                   
                {/*<Col xl={6} md={5} xs={4}>
                 <FormControl
                    
                    id="fid"
                    type="text"
                    placeholder="FDA ID"
                    className="mr-sm-2"
                    onChange={this.onSearchFIDBarChange}
                    />

                </Col>*/}

                <Col xl={1} md={1} xs={1}>
                  <Button varient="outline-primary" 
                  onClick={this.onSearchSubmit}>Search</Button>

                </Col>

              </Form.Row>

              
                
                

            </div>
              
          </Form>


         
      </div>
    );
  }
}
export default ContractorSearchBox;