import React, { Component } from "react";
import { Form, FormControl, Button, Col, Dropdown, DropdownButton } from "react-bootstrap";
//import FormCheckInput from "react-bootstrap/FormCheckInput";


const sortParamList = ["rating", "distance", "price"];
const sortParamListStr = ["Rating", "Distance", "Price"];
const sortOrderList = ["desc", "asc"];
const sortOrderListStr = ["High to Low", "Low to High"];
const serviceTypeList = ["", "Rat", "Snake", "Ant", "Termite", "Cockroach"];
const serviceTypeListStr = ["ALL TYPES", "Rat", "Snake", "Ant", "Termite", "Cockroach"];

export class ServiceSearchBox extends Component {
  constructor() {
    super();
    this.onSortParamBarChange = this.onSortParamBarChange.bind(this);
    this.onSortOrderBarChange = this.onSortOrderBarChange.bind(this);
    this.onSearchTypeBarChange = this.onSearchTypeBarChange.bind(this);
    this.onSearchSubmit = this.onSearchSubmit.bind(this);
    this.onSortSubmit = this.onSortSubmit.bind(this);
    this.state = {
      service_type: "",
      service_typeStr: "ALL TYPES",
      con_name: "",
      con_fda: "",
      sortParam: "",
      sortParamStr: "",
      sortOrder: "",
      sortOrderStr: "",
      searchResult: [],
      lat1: "13.738735",
      lon1: "100.529781"
    };
  }

  //capture keyword change in searchBar
  onSearchTypeBarChange = e => {
    console.log("Search type bar!");
    console.log(e);
    console.log(serviceTypeList[e]);
    this.setState({ service_type: serviceTypeList[e], service_typeStr: serviceTypeListStr[e] });
    //console.log(this.state);
  };

  onSearchNameBarChange = e => {
    this.setState({ con_name: e.target.value });
    //console.log(this.state);
  };

  onSearchFDABarChange = e => {
    this.setState({ con_fda: e.target.value });
    //console.log(this.state);
  };

  onSortParamBarChange = e => {
    console.log("Param bar!");
    console.log(e);
    console.log(sortParamList[e]);
    this.setState({ sortParam: sortParamList[e], sortParamStr: sortParamListStr[e] }, function () { this.onSortSubmit(); });
  };

  onSortOrderBarChange = e => {
    console.log("Order bar!");
    console.log(e);
    this.setState({ sortOrder: sortOrderList[e], sortOrderStr: sortOrderListStr[e] }, function () { this.onSortSubmit(); });
  };

  async onSearchSubmit() {
    try {
      //console.log(this.state);
      const data = this.state;
      //console.log(JSON.stringify(data));
      const response = await fetch("/service/search", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      this.setState({ searchResult: results }, function () { this.props.upDateServiceSearchResults(results); });
    } catch (error) {
      console.log("Search failed", error);
    }
  };

  async onSortSubmit() {
    let tmp_result = this.state.searchResult;
    let sortBy = this.state.sortParam;
    let sortOrder = this.state.sortOrder;
    console.log(this.state.sortParam);
    console.log(this.state.sortOrder);
    if (sortBy == "rating")
      tmp_result.sort(byRating);
    else if (sortBy == "price")
      tmp_result.sort(byPrice);
    else if (sortBy == "distance")
      tmp_result.sort(byDistance);
    if (sortOrder == "asc")
      tmp_result.reverse();
    this.setState({ searchResult: tmp_result }, function () { console.log(tmp_result); this.props.upDateServiceSearchResults(tmp_result); });
    //console.log(tmp_result);
  }


  render() {  //wait for re-deco
  	let sortBtn

  	if(this.state.searchResult.length > 0) 
      sortBtn =  <Form.Row id="sortRow">
            <Col xl={6} md={3}>
            </Col>
            <Col xl={2.5} md={2.5}>
              <DropdownButton
                title={"Sort By: " + this.state.sortParamStr}
                id="category-dropdown"
                onSelect={this.onSortParamBarChange}>
                Sort By:
			            <Dropdown.Item eventKey="0" >{sortParamListStr[0]}</Dropdown.Item>
                <Dropdown.Item eventKey="1" >{sortParamListStr[1]}</Dropdown.Item>
                <Dropdown.Item eventKey="2" >{sortParamListStr[2]}</Dropdown.Item>
              </DropdownButton>
            </Col>
            <Col xl={2.5} md={2.5}>
              <DropdownButton
                title={"Sort Order: " + this.state.sortOrderStr}
                id="category-dropdown"
                onSelect={this.onSortOrderBarChange}>
                Sort Order:
			            <Dropdown.Item eventKey="0" >{sortOrderListStr[0]}</Dropdown.Item>
                <Dropdown.Item eventKey="1" >{sortOrderListStr[1]}</Dropdown.Item>
              </DropdownButton>
            </Col>

          </Form.Row>
    else sortBtn = <div></div>
    return (  //The two sorting params will be changed to dropdown later.
      <div id="ContractorSearchBox">
        <h5 class="mHead">Search Service</h5>
        <Form id="SearchSortForm">

          <Form.Row>
            <Col xl={3} md={6}>
              <FormControl
                id="con_name"
                type="text"
                placeholder="Contractor Name"
                className="mr-sm-2"
                onChange={this.onSearchNameBarChange}
              />
            </Col>
            <Col xl={3} md={4}>
              <DropdownButton
                title={"Service Type: " + this.state.service_typeStr}
                id="category-dropdown"
                onSelect={this.onSearchTypeBarChange}>
                Service type:
				        <Dropdown.Item eventKey="0" >{serviceTypeListStr[0]}</Dropdown.Item>
                <Dropdown.Item eventKey="1" >{serviceTypeListStr[1]}</Dropdown.Item>
                <Dropdown.Item eventKey="2" >{serviceTypeListStr[2]}</Dropdown.Item>
                <Dropdown.Item eventKey="3" >{serviceTypeListStr[3]}</Dropdown.Item>
                <Dropdown.Item eventKey="4" >{serviceTypeListStr[4]}</Dropdown.Item>
                <Dropdown.Item eventKey="5" >{serviceTypeListStr[5]}</Dropdown.Item>
              </DropdownButton>

            </Col>
            {/*<Col xl={2} md={4}>
		              <FormControl
		                id="con_fda"
		                type="text"
		                placeholder="Contractor FDA ID"
		                className="mr-sm-2"
		                onChange={this.onSearchFDABarChange}
		              />
		            </Col>*/}
            <Col xl={1} md={1}>
              <Button onClick={this.onSearchSubmit}>Search</Button>
            </Col>
          </Form.Row>
          {sortBtn}

          


        </Form>
      </div>
    );
  }
}

function byRating(a, b) {
  if (parseFloat(a.rating) > parseFloat(b.rating)) return -1;
  if (parseFloat(a.rating) < parseFloat(b.rating)) return 1;
  return 0;
}

function byPrice(a, b) {
  if (parseInt(a.price) > parseInt(b.price)) return -1;
  if (parseInt(a.price) < parseInt(b.price)) return 1;
  return 0;
}

function byDistance(a, b) {
  if (parseInt(a.distance) > parseInt(b.distance)) return -1;
  if (parseInt(a.distance) < parseInt(b.distance)) return 1;
  return 0;
}

export default ServiceSearchBox;