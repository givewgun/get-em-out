import React, { Component } from "react";
import { Button, Card } from "react-bootstrap";
//each variable in here is to be checked with the vars from back-end
export class ContractorServiceListingBox extends Component {

  constructor() {
    super();
    this.onRequestBooking = this.onRequestBooking.bind(this);
  }
  
  moreInfo() {
    window.location.href = "/service/detail/" + this.props.id;
  }

  contractorInfo() {
    window.location.href = "/contractor/detail/" + this.props.fda_cer;
  }

  onRequestBooking() {
    sessionStorage.setItem('serviceID',this.props.id);
    window.location.href = "/Booking";
  }

  render() {
    return (
      <div id="item">
        <Card id="ccard" style={{ width: '15rem' }}>
          <Card.Img variant="top" src={this.props.ImageUrl ? this.props.ImageUrl:"https://cdn1.iconfinder.com/data/icons/pest-control-glyph-silhouettes/300/65717902Untitled-3-512.png"} />
          <Card.Body>
            <Card.Title >{this.props.type}</Card.Title>
            <Card.Text>
              <p> Rating : {this.props.rating ? this.props.rating:"No rating yet"} </p>
              <p> Price : THB {this.props.price} </p>
              {this.props.phone == null ? null : <p> Phone : {this.props.phone}</p>}
              {this.props.email == null ? null : <p> Email : {this.props.email}</p>}
            </Card.Text>
            <Button size="sm"
              title="button_to_service_info" onClick={(e) => this.moreInfo(e)}>
              View Info
        </Button>
        {(localStorage.getItem('userType') === "homeowner") ?
              <Button size="sm"
                onClick={this.onRequestBooking}>
                Book
              </Button> : null}
          </Card.Body>
        </Card>
      </div>
    );
  }
}



export default ContractorServiceListingBox;