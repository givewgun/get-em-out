import React, { Component } from "react";
import { Button,Card } from "react-bootstrap";
//each variable in here is to be checked with the vars from back-end
export class ServiceCommentBox extends Component {

  render() {
    return (
      <div id="item">
      <Card style={{ width: '15rem' }}>
        <Card.Img variant="top" src={this.props.ImageUrl} />
        <Card.Body>
          <Card.Title><b>{this.props.username} </b></Card.Title>
          <Card.Text>
            <p> Rating : {this.props.feedback_rating} </p>
            <p> Comment : {this.props.feedback_comment}</p>
          </Card.Text>
        </Card.Body>
      </Card>
      </div>
    );
  }
}


export default ServiceCommentBox;