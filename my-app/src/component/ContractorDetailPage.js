import React, { Component } from "react";
import ContractorProfileInfo from "./ContractorProfileInfo";
import ContractorServiceListingBox from "./ContractorServiceListingBox";
import { CardDeck, Container } from 'react-bootstrap';
import SurprisedPikachu from './SurprisedPikachu'
export class ContractorDetailPage extends Component {
    constructor() {
        super();
        this.state = { profile: [], services: [], error: false };
    }

    componentDidMount() {
        let path = window.location.pathname.split('/');
        if (path.length === 4 && path[1] === "contractor" && path[2] === "detail" && path[3] !== "") {

            fetch("/contractor/get_detail/" + path[3])
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Internal error');
                    }
                })
                .then(data => {
                    if (data.length !== 0) {
                        this.setState({ profile: data[0], services: data.slice(1) });
                    } else {
                        this.setState({ profile: false });
                    }
                })
                .catch((error) => {
                    this.setState({ error: true });
                    console.log(error);
                });

        }
        console.log(this.state.profile.latitude, this.state.profile.longitude);
    }

    render() {
        let path = window.location.pathname.split('/');
        if (path.length === 4 && path[1] === "contractor" && path[2] === "detail" && path[3] !== "") {
            //console.log(this.state.services);
            //console.log(this.state.profile);
            let services, profile;
            if (!this.state.error) {
                if (this.state.profile !== false) {
                    profile = <ContractorProfileInfo
                        ImageUrl={this.state.profile.photo_url}
                        name={this.state.profile.comp_name}
                        fda_cer={this.state.profile.fda_cer}
                        phone={this.state.profile.phone}
                        rating={this.state.profile.rating}
                        email={this.state.profile.email}
                        tax_id={this.state.profile.tax_id}
                        addr={this.state.profile.addr}
                        lat={this.state.profile.latitude}
                        lng={this.state.profile.longitude}
                    ></ContractorProfileInfo>
                    services = this.state.services.length > 0 ? this.state.services.map(resultItem => (
                        <ContractorServiceListingBox
                            ImageUrl={resultItem.photo_url}
                            id={resultItem.id}  //service_id
                            fda_cer={resultItem.fda_cer}
                            type={resultItem.type_name}
                            name={resultItem.name}
                            price={resultItem.price}
                            description={resultItem.descr}
                            rating={resultItem.rating}
                            contractor={resultItem.comp_name}
                        />
                    )) : "This contractor has no service.";
                } else {
                    profile = "No contractor found";
                }
            }else{
                profile=null;
                services=null;
            }
            return (
                <div class="Page">
                    {this.state.error ? <div><h1>Internal Server Error</h1><SurprisedPikachu></SurprisedPikachu></div> : null}
                    <div id="infodev">
                    {profile}
                    </div>
                    
                    {this.state.profile&&!this.state.error ?
                        <Container>
                            <h1>Services</h1>
                            <CardDeck id="Carddeck">{services}</CardDeck>
                        </Container> : null
                    }
                </div>
            );
        } else {
            return (
                <div id="item">
                    <h1>Oops <br></br> {window.location.pathname} <br></br> {path[3]} </h1>
                </div>
            );
        }
    }
}




export default ContractorDetailPage;