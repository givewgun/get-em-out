import React, { Component } from "react";
import { Button,Card,ListGroup,ListGroupItem,Image } from "react-bootstrap";
import Img from 'react-image';
//each variable in here is to be checked with the vars from back-end
export class ContractorResultItemBox extends Component {


  moreInfo() {
    window.location.href = "/contractor/detail/" + this.props.fda_cer;
  }

  render() {
    return (
      <div id="item">
      <Card id="ccard" style={{ width: '15rem', height:'29rem' }}>
        <Image style={{ width: '15rem',height:'10rem',marginLeft:'auto',marginRight:'auto' }}  src={this.props.ImageUrl} fluid rounded />
        <Card.Body>
          <Card.Title>{this.props.name} <small>{this.props.fda_cer}</small> </Card.Title>
          <Card.Text>
            <p> Phone : {this.props.phone}</p>
            <p> Rating : {this.props.rating} </p>
            <p> Email : {this.props.email}</p>
          </Card.Text>
          <Card.Link href="url_to_contractor_profile">{this.props.contractor}</Card.Link>
        </Card.Body>
        <ListGroup>
            <ListGroupItem>
              <Button size="sm"
                  title="button_to_service_info" onClick={(e) => this.moreInfo(e)}>
                  View Info
              </Button>
            </ListGroupItem>
        </ListGroup>
      </Card>
      </div>
    );
  }
}


export default ContractorResultItemBox;