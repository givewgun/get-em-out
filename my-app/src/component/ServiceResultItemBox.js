import React, { Component } from "react";
import { Button, Card, ButtonGroup, ListGroupItem, ListGroup, Image } from "react-bootstrap";
import Img from 'react-image';
//each variable in here is to be checked with the vars from back-end
export class ServiceResultItemBox extends Component {
  constructor() {
    super();
    this.onRequestBooking = this.onRequestBooking.bind(this);
  }
  moreInfo() {
    window.location.href = "/service/detail/" + this.props.id;
  }

  contractorInfo() {
    window.location.href = "/contractor/detail/" + this.props.fda_cer;
  }

  onRequestBooking() {
    sessionStorage.setItem('serviceID', this.props.id);
    window.location.href = "/Booking";
  }

  render() {
    return (
      <div id="item">
        <Card id="ccard" style={{ width: '15rem', height: '29rem' }}>
          <Image style={{ width: '15rem', height: '10rem', marginLeft: 'auto', marginRight: 'auto' }} src={this.props.ImageUrl ? this.props.ImageUrl : "https://cdn1.iconfinder.com/data/icons/pest-control-glyph-silhouettes/300/65717902Untitled-3-512.png"} fluid rounded />
          <Card.Body>
            <Card.Title >{this.props.type}<small> by <a href={"/contractor/detail/" + this.props.fda_cer}>{this.props.contractor}</a> </small></Card.Title>
            <Card.Text>
              <p> Rating : {this.props.rating} </p>
              <p> Price : THB {this.props.price}  </p>
              <p> Distance : {this.props.distance} km.</p>
            </Card.Text>
          </Card.Body>
          <ListGroup>
            {/*<ListGroupItem>
              <Button size="sm"
                  title="button_to_contractor_info" onClick={(e) => this.contractorInfo(e)}>
                  Contractor Info...
              </Button>
            </ListGroupItem>*/}
            <ListGroupItem>
              <Button size="sm"
                title="button_to_service_info" onClick={(e) => this.moreInfo(e)}>
                View Info
              </Button>
              {(localStorage.getItem('userType') === "homeowner") ?
                <Button size="sm"
                  onClick={this.onRequestBooking}>
                  Book
              </Button> : null}
            </ListGroupItem>
          </ListGroup>
        </Card>
      </div>
    );
  }
}



export default ServiceResultItemBox;