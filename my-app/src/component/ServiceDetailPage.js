import React, { Component } from "react";
import ServiceInfoBanner from "./ServiceInfoBanner";
import ServiceCommentBox from "./ServiceCommentBox";
import { CardDeck, Container } from 'react-bootstrap';
import SurprisedPikachu from './SurprisedPikachu'
export class ServiceDetailPage extends Component {
    constructor() {
        super();
        this.state = { info: [], comment: [], error: false };
    }

    componentDidMount() {
        let path = window.location.pathname.split('/');
        console.log(localStorage.getItem('userType'));
        if (path.length === 4 && path[1] === "service" && path[2] === "detail" && path[3] !== "") {
            fetch("/service/get_detail/" + path[3])
                .then((response) => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Internal error');
                    }
                })
                .then(data => {
                    console.log(data);
                    if (data.length !== 0) {
                        this.setState({ info: data[0], comment: data.slice(1) });
                    } else {
                        this.setState({ info: false });
                    }
                })
                .catch((error) => {
                    this.setState({ error: true });
                    console.log(error);
                });
        }

    }

    render() {
        let path = window.location.pathname.split('/');
        if (path.length === 4 && path[1] === "service" && path[2] === "detail" && path[3] !== "") {
            console.log(this.state.comment);
            console.log(this.state.info);
            let comments, info;
            if (!this.state.error) {
                if (this.state.info !== false) {
                    info = <ServiceInfoBanner
                        id={path[3]} //service_id
                        contractor_id={this.state.info.contractor_id}
                        comp_name={this.state.info.comp_name}
                        descr={this.state.info.descr}
                        type_name={this.state.info.type_name}
                        price={this.state.info.price}
                        warranty={this.state.info.warranty}
                        photo_url={this.state.info.photo_url}
                        rating={this.state.info.rating}
                        lat={parseFloat(this.state.info.latitude)}
                        lng={parseFloat(this.state.info.longitude)}
                        paymentByCredit={this.state.info.paymentByCredit}
                        paymentByBank={this.state.info.paymentByBank}
                        phone={this.state.info.phone}
                        email={this.state.info.email}
                        start_time={this.state.info.start_time}
                        end_time={this.state.info.end_time}
                    ></ServiceInfoBanner>
                    comments = this.state.comment.length > 0 ?
                        this.state.comment.map(c => (c.feedback_rating != null ? (
                            <ServiceCommentBox
                                username={c.username}
                                feedback_rating={c.feedback_rating}
                                feedback_comment={c.feedback_comment}
                            />
                        ) : null))
                        : "No comment";
                } else {
                    info = "No service found";
                }
            } else {
                info = null;
                comments = null;
            }
            return (

                <div class="Page">
                    {this.state.error ? <div><h1>Internal Server Error</h1><SurprisedPikachu></SurprisedPikachu></div> : null}
                    <div id="infodiv">
                        {info}
                    </div>

                    {this.state.info && !this.state.error ?
                        <Container>
                            <h1>Comments</h1>
                            <CardDeck>{comments}</CardDeck>
                        </Container> : null
                    }
                </div>
            );
        } else {
            return (
                <div id="item">
                    <h1>Oops <br></br> {window.location.pathname} <br></br> {path[3]} </h1>
                </div>
            );
        }
    }
}




export default ServiceDetailPage;