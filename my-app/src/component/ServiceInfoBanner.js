import React, { Component } from "react";
import { Card, Container, Row, Col, Button ,Image} from "react-bootstrap";
import LocationPicker from 'react-location-picker';
import Img from 'react-image';
//import { GoogleMapLoader } from "react-google-maps";
const defaultPosition = {
    lat: 13.738735,
    lng: 100.529781
};
const google = window.google;
export class ServiceInfoBanner extends Component {
    constructor() {
        super();
        this.state = {
            address: "Bangkok, Thailand",
            position: {
                lat: 0,
                lng: 0
            }
        };
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.onRequestBooking = this.onRequestBooking.bind(this);
    }


    onRequestBooking() {
        sessionStorage.setItem('serviceID', this.props.id);
        window.location.href = "/Booking";
    }

    handleLocationChange({ position, address }) {
        this.setState({ position, address });
    }

    render() {
        return (
            <div id="item" align="center">
                <Card style={{ width: '100%' }}>
                    <Container align="left">
                        <Row>
                            <Col xl={4} >
                                <Row>
                                    <Col  align="center" style={{ marginTop: '10%' }}>
                                        <Image class="Infoimage" width={250} src={this.props.photo_url ? this.props.photo_url : "https://cdn1.iconfinder.com/data/icons/pest-control-glyph-silhouettes/300/65717902Untitled-3-512.png"} rounded />
                                    </Col>
                                </Row>
                                <Row align="center">
                                    <Col align="center">
                                        {(localStorage.getItem('userType') === "homeowner") ? <div align="center"><Button onClick={this.onRequestBooking}>Book Service</Button></div> : null}
                                    </Col>
                                </Row>
                            </Col>
                            <Col xl={4}>
                                <Card.Body>
                                    <Card.Title>Service Type : <strong><big>{this.props.type_name}</big></strong> <br></br> <small>by <a href={"/contractor/detail/" + this.props.contractor_id}>{this.props.comp_name}</a> </small> </Card.Title>
                                    <Card.Text>
                                        <Row>
                                            <Col>
                                                Rating : {this.props.rating ? this.props.rating + "☆" : "No rating yet"}
                                            </Col>
                                        </Row>
                                        <Row><br></br></Row>
                                        <Row>
                                            <Col>
                                                Price : THB {this.props.price}
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col>
                                                Available Time : {this.props.start_time === null || this.props.end_time === null ?
                                                    localStorage.getItem('userType') === 'homeowner' ? 'Contact the contractor' : 'Not specified'
                                                    : this.props.start_time + " - " + this.props.end_time}
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col>
                                                Phone : {this.props.phone == null ? 'None' : this.props.phone}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Email : {this.props.email == null ? 'None' : this.props.email}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Warranty(Months) : {this.props.warranty == null ? 'None' : this.props.warranty}
                                            </Col>
                                        </Row>
                                        <Row><br></br></Row>
                                        <Row>
                                            <Col>
                                                Payment Methods
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Credit Card : {this.props.paymentByCredit == 1 ? 'Yes' : 'No'}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                Bank : {this.props.paymentByBank == 1 ? 'Yes' : 'No'}
                                            </Col>
                                        </Row>

                                        <Row><br></br></Row>
                                        <Row>
                                            <Col>Description</Col>
                                        </Row>
                                        <Row>
                                            <Col>"{this.props.descr}"</Col>
                                        </Row>

                                    </Card.Text>

                                </Card.Body>
                            </Col>
                            <Col xl={4}  >
                                <LocationPicker
                                    containerElement={<div style={{ width: '100%', height: '100%' }} />}
                                    mapElement={<div style={{ height: '400px' }} />}
                                    defaultPosition={{ lat: this.props.lat, lng: this.props.lng }}
                                    onChange={this.handleLocationChange}
                                />
                            </Col>
                        </Row>
                    </Container>
                </Card>
            </div>
        );
    }
}


export default ServiceInfoBanner;