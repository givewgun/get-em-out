import React, { Component } from "react";
import ContractorResultItemBox from "./ContractorResultItemBox";
//each variable in here is to be checked with the vars from back-end
export class ContractorSearchResult extends Component {
  render() {
    // display the course item for each courseItem
    // in searchResults( passed as props from state of SearchCoursesPage)
    return this.props.searchResults.map(resultItem => (
      <ContractorResultItemBox
        ImageUrl={resultItem.photo_url}
        name={resultItem.comp_name}
        fda_cer={resultItem.fda_cer}
        phone={resultItem.phone}
        rating={resultItem.rating}
        email={resultItem.email}
      />
    ));
  }
}

export default ContractorSearchResult;