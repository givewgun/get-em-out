import React, { Component } from "react";
import ServiceResultItemBox from "./ServiceResultItemBox";
//each variable in here is to be checked with the vars from back-end
export class ServiceSearchResult extends Component {
  render() {
    // display the course item for each courseItem
    // in searchResults( passed as props from state of SearchCoursesPage)
    return this.props.searchResults.map(resultItem => (
      <ServiceResultItemBox
        ImageUrl={resultItem.photo_url}
        id={resultItem.id}  //service_id
        fda_cer={resultItem.fda_cer}
        type={resultItem.type_name}
        name={resultItem.name}
        distance={resultItem.distance.toFixed(2)}
        price={resultItem.price}
        description={resultItem.descr}
        rating={resultItem.rating}
        contractor={resultItem.comp_name}
      />
    ));
  }
}

export default ServiceSearchResult;