import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal, Dropdown } from 'react-bootstrap';
//import Image from 'react-image';
import { CardDeck, Container } from 'react-bootstrap';
const axios = require("axios");
export class EditServiceForm extends Component {
    constructor(props) {
        super(props);
        this.typeChange = this.typeChange.bind(this);
        this.priceChange = this.priceChange.bind(this);
        this.descrChange = this.descrChange.bind(this);
        this.startTimeChange = this.startTimeChange.bind(this);
        this.endTimeChange = this.endTimeChange.bind(this);
        this.warrantyChange = this.warrantyChange.bind(this);
        this.dailyLimitChange = this.dailyLimitChange.bind(this);
        this.servicePictureChange = this.servicePictureChange.bind(this);

        this.onSaveChangeServiceButton = this.onSaveChangeServiceButton.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onPictureSubmit = this.onPictureSubmit.bind(this);

        this.ValidateType = this.ValidateType.bind(this);
        this.ValidatePrice = this.ValidatePrice.bind(this);
        this.ValidateDescr = this.ValidateDescr.bind(this);
        this.ValidateStartTime = this.ValidateStartTime.bind(this);
        this.ValidateEndTime = this.ValidateEndTime.bind(this);
        this.ValidateWarranty = this.ValidateWarranty.bind(this);
        this.ValidateDailyLimit = this.ValidateDailyLimit.bind(this);
        this.ValidateAll = this.ValidateAll.bind(this);
        this.validateServicePicture = this.validateServicePicture.bind(this);

        this.state = {
            id: this.props.id,
            type: this.props.type === null ? "" : this.props.type,
            price: this.props.price === null ? "" : this.props.price,
            descr: this.props.descr === null ? "" : this.props.descr,
            startTime: this.props.startTime === null ? "" : this.props.startTime,
            endTime: this.props.endTime === null ? "" : this.props.endTime,
            warranty: this.props.warranty === null ? "" : this.props.warranty,
            dailyLimit: this.props.dailyLimit === null ? "" : this.props.dailyLimit,
            temp_photo: "",
            temp_valid_photo: "",
            photo_url: this.props.photo_url === null ? "" : this.props.photo_url,

            typeValid: 0,
            priceValid: 0,
            descrValid: 0,
            startTimeValid: 0,
            endTimeValid: 0,
            warrantyValid: 0,
            dailyLimitValid: 0,
            servicePictureValid: 0,
            isUpload: 0,
            submitSuccess: 0
        }

    }

    async onSaveChangeServiceButton() {
        if (this.ValidateAll() !== 0)
            alert("Error. Please recheck your service form");
        else if (this.state.isUpload === 1 && this.state.servicePictureValid !== 0) {
            alert("Error. Please recheck your service picture");
        }
        else {
            this.onFormSubmit();
            // if (this.state.submitSuccess === 0) {
            //     this.props.reloadData();
            //     this.props.handleClose();
            // }
        }
    }

    async onFormSubmit() {
        try {
            const data = {};
            data.id = this.state.id;
            data.contractor_id = localStorage.getItem('fda_cer');
            data.descr = this.state.descr;
            data.serv_type = this.state.type;
            data.price = this.state.price;
            data.warranty = this.state.warranty;
            data.daily_limit = this.state.dailyLimit;
            data.start_time = this.state.startTime;
            data.end_time = this.state.endTime;
            const response = await fetch("/service/editservice", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            });
            const results = await response.json();
            console.log(results);
            if (results.status == "success") {
                console.log("Service Form Edit Success");
                if (this.state.isUpload === 1) { this.onPictureSubmit(); }
                else {
                    this.props.reloadData();
                    this.props.handleClose();
                }
            } else {
                alert("Error. Edit service failed");
                this.setState({ submitSuccess: 1 });
            }
        } catch (error) {
            console.log("Edit service failed", error);
            this.setState({ submitSuccess: 1 });
        }
    }

    async servicePictureChange(event) {
        console.log(event.target.files[0]);
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                temp_photo: event.target.files[0]
            }, () => this.validateServicePicture());
        } else {
            this.setState({
                temp_photo: null
            }, () => this.validateServicePicture());
        }
    }

    async onPictureSubmit() {
        try {
            //console.log(this.state);
            const formData = new FormData();
            formData.set('service_id', this.state.id);
            formData.append('fdacer', localStorage.getItem('fda_cer'));
            formData.append('service_image', this.state.temp_valid_photo);
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                },
            };
            axios.post("/service/upload", formData, config)
                .then((response) => {
                    this.props.reloadData();
                    this.props.handleClose();
                }).catch((error) => {
                    alert("Failed to submit picture");
                    this.setState({ submitSuccess: 1 });
                });
        } catch (error) {
            console.log("ServicePic Submission Failed", error);
            this.setState({ submitSuccess: 1 });
        }
    }

    typeChange(event) {
        this.setState({
            type: event.target.value.trim()
        }, () => this.ValidateType());
    }

    priceChange(event) {
        this.setState({
            price: parseFloat(event.target.value.trim())
        }, () => this.ValidatePrice());
    }

    descrChange(event) {
        this.setState({
            descr: event.target.value.trim()
        }, () => this.ValidateDescr());
    }

    startTimeChange(event) {
        this.setState({
            startTime: event.target.value.trim()
        }, () => this.ValidateStartTime());
    }

    endTimeChange(event) {
        this.setState({
            endTime: event.target.value.trim()
        }, () => this.ValidateEndTime());
    }

    warrantyChange(event) {
        this.setState({
            warranty: parseFloat(event.target.value.trim())
        }, () => this.ValidateWarranty());
    }

    dailyLimitChange(event) {
        this.setState({
            dailyLimit: parseInt(event.target.value.trim())
        }, () => this.ValidateDailyLimit());
    }

    ValidateType() {
        let input = this.state.type;
        let regex = /^[^0][0-9]*$/
        if (regex.test(input)) {
            this.setState({
                typeValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    typeValid: 1
                });
            } else {
                this.setState({
                    typeValid: 2
                });
            }
            return 1;
        }
    }

    ValidatePrice() {
        let input = this.state.price;
        let regex = /(^[0-9]{1,6}$|^[0-9]{1,6}\.[0-9]{1,2}$)/
        if (regex.test(input)) {
            this.setState({
                priceValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    priceValid: 1
                });
            } else {
                this.setState({
                    priceValid: 2
                });
            }
            return 1;
        }

    }

    ValidateDescr() {
        let input = this.state.descr;
        let regex = /^.{1,100}$/
        if (regex.test(input)) {
            this.setState({
                descrValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    descrValid: 1
                });
            } else {
                this.setState({
                    descrValid: 2
                });
            }
            return 1;
        }
    }

    ValidateStartTime() {
        let input = this.state.startTime;
        let regex = /(^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$|^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]:[0-5][0-9]$)/
        if (regex.test(input)) {
            this.setState({
                startTimeValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    startTimeValid: 1
                });
            } else {
                this.setState({
                    startTimeValid: 2
                });
            }
            return 1;
        }
    }

    ValidateEndTime() {
        let input = this.state.endTime;
        let regex = /(^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$|^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]:[0-5][0-9]$)/
        if (regex.test(input)) {
            this.setState({
                endTimeValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    endTimeValid: 1
                });
            } else {
                this.setState({
                    endTimeValid: 2
                });
            }
            return 1;
        }
    }

    ValidateWarranty() {
        let input = this.state.warranty;
        let regex = /(^[0-9]{1,10}$|^[0-9]{1,7}\.[0-9]{1,2}$)/
        if (regex.test(input)) {
            this.setState({
                warrantyValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    warrantyValid: 1
                });
            } else {
                this.setState({
                    warrantyValid: 2
                });
            }
            return 1;
        }
    }

    ValidateDailyLimit() {
        let input = this.state.dailyLimit;
        let regex = /^[0-9]{1,10}$/
        if (regex.test(input)) {
            if (input > 2147483647) {
                this.setState({
                    dailyLimitValid: 2
                });
                return 1;
            } else {
                this.setState({
                    dailyLimitValid: 0
                });
                return 0;
            }
        } else {
            if (input.length == 0) {
                this.setState({
                    dailyLimitValid: 1
                });
            } else {
                this.setState({
                    dailyLimitValid: 2
                });
            }
            return 1;
        }
    }

    ValidateAll() {
        return (this.ValidateType() + this.ValidatePrice() + this.ValidateDescr() + this.ValidateStartTime() + this.ValidateEndTime() + this.ValidateWarranty() + this.ValidateDailyLimit()) == 0 ? 0 : 1;
    }

    validateServicePicture() {
        let input = this.state.temp_photo;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension
        console.log(input);

        if (input === null) {
            this.setState({
                servicePictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        servicePictureValid: 0,
                        isUpload: 1,
                        temp_valid_photo: input,
                        photo_url: URL.createObjectURL(input)
                    });
                    return 0
                } else {
                    this.setState({
                        servicePictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    servicePictureValid: 2
                });
                return 1;
            }
        }
    }

    render() {
        return (

            <Modal show={this.props.show} onHide={this.props.handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Edit service</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form style={format}>
                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }}>
                                <Form.Label>Service Type</Form.Label>
                                <Form.Control as="select" onChange={this.typeChange} className={`form-control ${this.state.typeValid == 0 ? '' : 'is-invalid'}`} value={this.state.type}>
                                    <option value="">Select</option>
                                    <option value="1">Rat</option>
                                    <option value="2">Snake</option>
                                    <option value="3">Ant</option>
                                    <option value="4">Termite</option>
                                    <option value="5">Cockroach</option>
                                </Form.Control>
                                <Form.Control.Feedback type="invalid">{this.state.typeValid == 1 ? "Please select service type." : "Invalid type"}</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} >
                                <Form.Label>Price (Baht)</Form.Label>
                                <Form.Control type="number" onChange={this.priceChange} className={`form-control ${this.state.priceValid == 0 ? '' : 'is-invalid'}`} value={this.state.price} />
                                <Form.Control.Feedback type="invalid">{this.state.priceValid == 1 ? "Please enter service price." : "Too high or Invalid price."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} >
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" onChange={this.descrChange} className={`form-control ${this.state.descrValid == 0 ? '' : 'is-invalid'}`} value={this.state.descr} />
                                <Form.Control.Feedback type="invalid">{this.state.descrValid == 1 ? "Please enter your service description." : "Entered description is too long"}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>


                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} >
                                <Form.Label>Start time</Form.Label>
                                <Form.Control type="time" onChange={this.startTimeChange} className={`form-control ${this.state.startTimeValid == 0 ? '' : 'is-invalid'}`} value={this.state.startTime} />
                                <Form.Control.Feedback type="invalid">{this.state.startTimeValid == 1 ? "Please enter start time" : "Invalid time"}</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} >
                                <Form.Label>End time</Form.Label>
                                <Form.Control type="time" onChange={this.endTimeChange} className={`form-control ${this.state.endTimeValid == 0 ? '' : 'is-invalid'}`} value={this.state.endTime} />
                                <Form.Control.Feedback type="invalid">{this.state.endTimeValid == 1 ? "Please enter end time" : "Invalid time"}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>


                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} >
                                <Form.Label>Warranty (months)</Form.Label>
                                <Form.Control type="number" onChange={this.warrantyChange} className={`form-control ${this.state.warrantyValid == 0 ? '' : 'is-invalid'}`} value={this.state.warranty} />
                                <Form.Control.Feedback type="invalid">{this.state.warrantyValid == 1 ? "Please enter the warranty time." : "Too high or Invalid number entered"}</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} >
                                <Form.Label>Daily limit</Form.Label>
                                <Form.Control type="number" onChange={this.dailyLimitChange} className={`form-control ${this.state.dailyLimitValid == 0 ? '' : 'is-invalid'}`} value={this.state.dailyLimit} />
                                <Form.Control.Feedback type="invalid">{this.state.dailyLimitValid == 1 ? "Please enter the daily limit" : "Too high or Invalid number entered"}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group controlId="formGridServicePic">
                                <Form.Label>Service Picture</Form.Label>
                                <Form.Control type="file" onChange={this.servicePictureChange} className={`form-control ${this.state.servicePictureValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.servicePictureValid == 1 ? "Please choose your service picture." : this.state.servicePictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                {this.state.servicePictureValid == 0 ? <Image id="servicePicture" src={this.state.photo_url}></Image> : null}
                            </Form.Group>
                        </Form.Row>
                        <br />

                    </Form>

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.props.handleClose}>
                        Close
                  </Button>

                    <Button variant="primary" onClick={this.onSaveChangeServiceButton} >Save Change</Button>

                </Modal.Footer>
            </Modal>






        );

    }

}

const current = {
    color: "blue"
}

const format = {
    paddingLeft: "2%",
    paddingRight: "2%"
}
const format2 = {
    paddingLeft: "10%",

}

export default EditServiceForm;