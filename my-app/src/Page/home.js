import React, { Component } from "react";
import { Carousel,Jumbotron,Button } from 'react-bootstrap';
import f1 from './pic/1_3.jpg'
import f2 from './pic/2_1.jpg'
import f3 from './pic/3_1.jpg'

//import firstImg from "../img/girl_training.jpg";
//import secondImg from "../img/training.jpg";

export class Home extends Component {
  constructor() {
    super();
    this.state = { width: 0, height: 0 };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }
  render() {
    return (
      <div>

        <Carousel id="homeItem1">
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={f1}
              alt="First slide"
            />
            {this.state.width <= 600 ?
              (
                null
                )
              :
              (
                <Carousel.Caption>
                  <h3 style={corouselLable}>Get in there, take care of it, and Get out</h3>
                  <p></p>
                </Carousel.Caption>
                )
            }
            
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={f2}
              alt="Third slide"
            />

            {this.state.width <= 600 ?
              (
                null
                )
              :
              (
                <Carousel.Caption>
                  <h3 style={corouselLable}>They are harm. They are scary. They damaged</h3>
                  <p></p>
                </Carousel.Caption>
                )
            }

            
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src={f3}
              alt="Third slide"
            />

            {this.state.width <= 600 ?
              (
                null
                )
              :
              (
                <Carousel.Caption>
                  <h3 style={corouselLable}>Make sure they won't come back. NEVER. EVER.</h3>
                  <p></p>
                </Carousel.Caption>
                )
            }
          </Carousel.Item>
        </Carousel>

        <Jumbotron id="homeItem2" style={homeItem2}>
		  <h1> Get 'em Out </h1>
		  <p>
		    Know more about the revolutionary pest control platform
		  </p>
		  <p>
		    <Button href="/AboutUs" variant="primary">About Us</Button>
		  </p>
		</Jumbotron>
		<Jumbotron id="homeItem2" style={homeItem3} >
		  <h1> Newcomer? Get over here!! </h1>
		  <p>
		    Join us and be free from your pest problem
		  </p>
		  <p>
		    <Button href="/Register" variant="primary">Join Us</Button>
		  </p>
		</Jumbotron>
		<Jumbotron id="homeItem2" style={homeItem4} >
		  <h1> Find Something? </h1>
		  <p>
		    
		  </p>
		  <p>
		    <Button href="/advance_search" variant="primary">Search</Button>
		  </p>
		</Jumbotron>
      </div>
    );
  }
}

const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  fontSize: "35px",
  fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  paddingLeft: "40px",
  paddingRight: "40px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "25px"
};

const homeItem2 = {
	backgroundImage: 'url("https://wallpapersite.com/images/pages/pic_h/13936.jpg")',
	backgroundSize: "cover",
	backgroundPosition: "center",
	color: "#ffffff",
	textShadow: "0px 0px 5px #000000",

}
const homeItem3 = {
	textAlign: "right",
	backgroundImage: 'url("https://images5.alphacoders.com/942/thumb-1920-942342.png")',
	backgroundSize: "cover",
	//backgroundPosition: "center",
	color: "#ffffff",
	textShadow: "0px 0px 5px #000000",

}
const homeItem4 = {
	textAlign: "center",
	backgroundImage: 'url("https://img5.goodfon.com/original/1920x1080/0/53/battlefield-electronic-arts-dice-battlefield-5-battlefield-v.jpg")',
	backgroundSize: "cover",
	backgroundPosition: "center",
	color: "#ffffff",
	textShadow: "0px 0px 5px #000000",
	textStroke: "rgb(0, 0, 0) 2px 0px 0px, rgb(0, 0, 0) 1.75517px 0.958851px 0px, rgb(0, 0, 0) 1.0806px 1.68294px 0px, rgb(0, 0, 0) 0.141474px 1.99499px 0px, rgb(0, 0, 0) -0.832294px 1.81859px 0px, rgb(0, 0, 0) -1.60229px 1.19694px 0px, rgb(0, 0, 0) -1.97998px 0.28224px 0px, rgb(0, 0, 0) -1.87291px -0.701566px 0px, rgb(0, 0, 0) -1.30729px -1.5136px 0px, rgb(0, 0, 0) -0.421592px -1.95506px 0px, rgb(0, 0, 0) 0.567324px -1.91785px 0px, rgb(0, 0, 0) 1.41734px -1.41108px 0px, rgb(0, 0, 0) 1.92034px -0.558831px 0px",

}

const corouselLable = {
	color: "#ffffff",
	textShadow: "0px 0px 5px #000000",
  fontSize: "calc(1em + 1vw)"
}

const forceHidden={
  fontSize:"0px",
}

export default Home;
