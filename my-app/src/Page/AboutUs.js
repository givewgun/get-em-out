import React, { Component } from "react";
import {Image, Row , Col} from 'react-bootstrap';
export class AboutUs extends Component {
    constructor() {
        super();
    }

    render() {

        return (
            <div class="Page">
                {/*<h1 style={headerStyle}>About Get 'em Out</h1>*/}

                <Row>
                  <Col>
                    <div align="center">
                      <Image as={Col} src={process.env.PUBLIC_URL + "/banner.jpg"} fluid />
                    </div>
                  </Col>
                </Row>

                <p style={paragraphStyle}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nowadays, household infestations have become one of the biggest problems for many families. Due to the increase in population and houses, the number of household pests also multiply. These pests cause many problems not only for the homeowners but the society as well due to the fact that many of them are the carrion of diseases and will damage the structures of the building that has been infested.</p>
                <p style={paragraphStyle}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From the above reason, some of the homeowners decide to hire a pest controller for clear pest out of their place(house). However, as of current, there are many problems that the homeowners have to face. One of the problems is that it is very difficult for homeowners to find a contractor as the majority of the advertisement and contact are found on the electric poles (as a poster) and there is not much to choose from the online sources either. Another problem is that it is hard to check the credibility of the contractor as there is no history or review to look at first.</p>
                <p style={paragraphStyle}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This project is initiated to provide a platform for the homeowners to connect with various of the pest control agents and find the contractor that suits them the best.</p>
            </div>
        );

    }

}


const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  paddingLeft: "40px",
  paddingRight: "40px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "25px"
};

export default AboutUs;