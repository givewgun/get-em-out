import React, { Component } from "react";
import ContractorRegisterForm from "../component/ContractorRegisterForm"
import HomeownerRegisterForm from "../component/HomeownerRegisterForm"
import { Carousel, Jumbotron, ButtonToolbar, Button, Form, FormControl,Row,Col,Breadcrumb } from 'react-bootstrap';
export class Registration extends Component {
    constructor() {
        super();
        this.state = { step: 0, type: 0 };
    }

    render() {

        return (
            <div class="Page">   
                {this.state.step == 0 ? (
                    <div>
                        <Breadcrumb>
                            <Breadcrumb.Item active>select user type</Breadcrumb.Item>
                            <Breadcrumb.Item >fill the information</Breadcrumb.Item>
                            <Breadcrumb.Item >Upload your profile</Breadcrumb.Item>
                            <Breadcrumb.Item >Confirm</Breadcrumb.Item>
                        </Breadcrumb>
                    <div class="PageBody">
                    <h1 style={headerStyle} >Who are you?</h1>
                    <br/><br/><br/><br/>
                    <Row>
                        <Col style={headerStyle} >
                            <Button size="lg" onClick={() => { this.setState({ step: 2, type: 1 }) }}>Homeowner</Button>
                        </Col>
                        <Col style={headerStyle} >
                            <Button size="lg" onClick={() => { this.setState({ step: 2, type: 2 }) }}>Contractor</Button>
                        </Col>
                    </Row>
                    
                    </div>
                    </div>
                    
                ) : (


                        this.state.type == 1 ? (
                                <HomeownerRegisterForm />
                        ) : (
                                <ContractorRegisterForm />
                            )

                    )}
                <div>

                </div>
            </div>
        );

    }

}

const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  paddingLeft: "40px",
  paddingRight: "40px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "25px"
};

const current ={
    color: "blue"
}


export default Registration;