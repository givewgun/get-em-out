import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
export class Payment extends Component {
  constructor() {
    super();

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.getRequestDetail = this.getRequestDetail.bind(this);
    this.getCost = this.getCost.bind(this);

    this.cardNoChange = this.cardNoChange.bind(this);
    this.cvvNoChange = this.cvvNoChange.bind(this);
    this.expDateChange = this.expDateChange.bind(this);
    this.firstnameChange = this.firstnameChange.bind(this);
    this.lastnameChange = this.lastnameChange.bind(this);
    this.banknameChange = this.banknameChange.bind(this);
    this.transferDateTimeChange = this.transferDateTimeChange.bind(this);

    this.onPayCardButton = this.onPayCardButton.bind(this);
    this.onPayBankButton = this.onPayBankButton.bind(this);

    this.validateCardNo = this.validateCardNo.bind(this);
    this.validateCvvNo = this.validateCvvNo.bind(this);
    this.validateExpDate = this.validateExpDate.bind(this);
    this.validateFirstname = this.validateFirstname.bind(this);
    this.validateLastname = this.validateLastname.bind(this);
    this.validateBankname = this.validateBankname.bind(this);
    this.validateTransferDateTime = this.validateTransferDateTime.bind(this);
    this.validateAllCardField = this.validateAllCardField.bind(this);
    this.validateAllBankField = this.validateAllBankField.bind(this);

    this.state = {
      caseID: sessionStorage.getItem('paymentCaseID'),
      ptype: "",
      contractorBankNo: "",
      contractorBankname:"",
      show: false,
      cardNo: "",
      cvvNo: "",
      expDate: "",
      firstname: "",
      lastname: "",
      bankname: "",
      transferDateTime: "",

      cardNoValid: 0,
      cvvNoValid: 0,
      expDateValid: 0,
      firstnameValid: 0,
      lastnameValid: 0,
      banknameValid: 0,
      transferDateTimeValid: 0
    };
  }

  componentDidMount() {
    if (sessionStorage.getItem('paymentCaseID') === null) {
      window.location.href = "/RequestDashboard";
    }
    this.getRequestDetail();
  }

  async handleClose() {
    this.setState({ show: false });
  }

  async handleShow() {
    this.setState({ show: true });

  }

  async getRequestDetail() {
    try {
      const data = {};
      data.caseid = sessionStorage.getItem('paymentCaseID');
      const response = await fetch("/booking/getRequestDetail", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      let ptype = (results[0].status_name === "deposit pending" ? "deposit" : (results[0].status_name === "payment pending" ? "remaining" : "invalid"));
      if (ptype === "invalid") {
        window.location.href = "/RequestDashboard";
      }
      this.setState({
        contractorBankNo: results[0].bank_account,
        contractorBankname: results[0].bankname,
        ptype: ptype
      }, () => this.getCost());
    } catch (error) {
      console.log("Get request detail Failed", error);
    }
  }

  async getCost() {
    console.log(this.state.ptype);
    try {
      const data = {};
      data.caseid = sessionStorage.getItem('paymentCaseID');
      let response;
      if (this.state.ptype === 'deposit') {
        response = await fetch("/payment/getDepositCost", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
      } else if (this.state.ptype === 'remaining') {
        response = await fetch("/payment/getRemainingCost", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
      }

      const results = await response.json();
      console.log(results);
      this.setState({ cost: results.cost });
    } catch (error) {
      console.log("Get cost Failed", error);
    }
  }

  async onPayCardButton() {
    if (this.validateAllCardField() == 0) {
      try {
        let now = new Date();
        let date = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();
        let time = now.getHours() + ":" + now.getMinutes();
        let dateTime = date + ' ' + time;
        console.log(dateTime);

        const data = {};
        data.caseid = this.state.caseID;
        data.ptype = this.state.ptype;
        data.paydate = dateTime;
        data.method = "credit";
        data.amount = this.state.cost;
        data.bankname = null;

        const response = await fetch("/payment/pay", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
        const results = await response.json();
        console.log(results);
        if (results.status == "success") {
          sessionStorage.removeItem("paymentCaseID");
          //sessionStorage.removeItem("paymentPtype");
          alert("Payment successful");
          window.location.href = "/RequestDashboard"
        } else {
          alert("Error. Payment failed");
        }
      } catch (error) {
        console.log("Payment Failed", error);
      }
    } else {
      alert("Error. Please check your card information");
    }
  }

  async onPayBankButton() {
    if (this.validateAllBankField() == 0) {
      try {
        const data = {};
        data.caseid = this.state.caseID;
        data.ptype = this.state.ptype;
        data.paydate = this.state.transferDateTime;
        data.method = "bank";
        data.amount = this.state.cost;
        data.bankname = this.state.bankname;

        const response = await fetch("/payment/pay", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
        const results = await response.json();
        console.log(results);
        if (results.status == "success") {
          sessionStorage.removeItem("paymentCaseID");
          //sessionStorage.removeItem("paymentPtype");
          alert("Payment successful");
          window.location.href = "/RequestDashboard"
        } else {
          alert("Error. Payment failed");
        }
      } catch (error) {
        console.log("Payment Failed", error);
      }
    } else {
      alert("Error. Please check your information");
    }
  }

  async cardNoChange(event) {
    this.setState({
      cardNo: event.target.value.trim()
    }, () => this.validateCardNo());
  }

  async cvvNoChange(event) {
    this.setState({
      cvvNo: event.target.value.trim()
    }, () => this.validateCvvNo());
  }

  async expDateChange(event) {
    this.setState({
      expDate: event.target.value.trim()
    }, () => this.validateExpDate());
  }

  async firstnameChange(event) {
    this.setState({
      firstname: event.target.value.trim()
    }, () => this.validateFirstname());
  }

  async lastnameChange(event) {
    this.setState({
      lastname: event.target.value.trim()
    }, () => this.validateLastname());
  }

  async banknameChange(event) {
    this.setState({
      bankname: event.target.value.trim()
    }, () => this.validateBankname());
  }

  async transferDateTimeChange(event) {
    this.setState({
      transferDateTime: event.target.value
    }, () => this.validateTransferDateTime());
  }

  validateCardNo() {
    let input = this.state.cardNo;

    let visaPattern = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
    let mastPattern = /^(?:5[1-5][0-9]{14})$/;
    let amexPattern = /^(?:3[47][0-9]{13})$/;
    let discPattern = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;

    if (visaPattern.test(input)) {
      this.setState({
        cardNoValid: 0
      });
      return 0;
    } else if (mastPattern.test(input)) {
      this.setState({
        cardNoValid: 0
      });
      return 0;
    } else if (amexPattern.test(input)) {
      this.setState({
        cardNoValid: 0
      });
      return 0;
    } else if (discPattern.test(input)) {
      this.setState({
        cardNoValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          cardNoValid: 1
        });
      } else {
        this.setState({
          cardNoValid: 2
        });
      }
      return 1;
    }

  }

  validateCvvNo() {
    let input = this.state.cvvNo;
    let regex = /^[0-9]{3,4}$/;
    if (regex.test(input)) {
      this.setState({
        cvvNoValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          cvvNoValid: 1
        });
      } else {
        this.setState({
          cvvNoValid: 2
        });
      }
      return 1;
    }
  }

  validateExpDate() {
    let input = this.state.expDate;
    let regex = /^(?:0[1-9]|1[0-2]) *\/ *[1-9][0-9]$/;
    if (regex.test(input)) {
      this.setState({
        expDateValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          expDateValid: 1
        });
      } else {
        this.setState({
          expDateValid: 2
        });
      }
      return 1;
    }
  }

  validateFirstname() {
    let input = this.state.firstname;
    let regex = /^[^0-9]{1,50}$/
    if (regex.test(input)) {
      this.setState({
        firstnameValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          firstnameValid: 1
        });
      } else {
        this.setState({
          firstnameValid: 2
        });
      }
      return 1;
    }
  }

  validateLastname() {
    let input = this.state.lastname;
    let regex = /^[^0-9]{1,50}$/
    if (regex.test(input)) {
      this.setState({
        lastnameValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          lastnameValid: 1
        });
      } else {
        this.setState({
          lastnameValid: 2
        });
      }
      return 1;
    }
  }

  validateBankname() {
    let input = this.state.bankname;
    let regex = /^.{1,50}$/
    if (regex.test(input)) {
      this.setState({
        banknameValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          banknameValid: 1
        });
      } else {
        this.setState({
          banknameValid: 2
        });
      }
      return 1;
    }
  }

  validateTransferDateTime() {
    let input = this.state.transferDateTime;
    let regex = /\d{4}-\d{2}-\d{2}(\s|\T)\d{2}:\d{2}/;
    // console.log(input);
    if (regex.test(input)) {
      let chkValid = new Date(input);
      // console.log(chkValid instanceof Date && !isNaN(chkValid.valueOf()));
      if (chkValid instanceof Date && !isNaN(chkValid.valueOf())) {
        this.setState({
          transferDateTimeValid: 0
        });
        return 0;
      } else {
        this.setState({
          transferDateTimeValid: 2
        });
        return 1;
      }

    } else {
      if (input.length == 0) {
        this.setState({
          transferDateTimeValid: 1
        });
      } else {
        this.setState({
          transferDateTimeValid: 2
        });
      }
      return 1;
    }
  }

  validateAllCardField() {
    return (this.validateCardNo() + this.validateCvvNo() + this.validateExpDate() + this.validateFirstname() + this.validateLastname()) == 0 ? 0 : 1;
  }

  validateAllBankField() {
    return (this.validateBankname() + this.validateTransferDateTime()) == 0 ? 0 : 1;
  }

  render() {

    return (
      <div class="Page">
        <h1 style={headerStyle3}>Payment <small>Request ID: {this.state.caseID}</small></h1>
        <br />
        <Row>
          <Col>
            <h3 style={headerStyle}>Pay via Credit/Debit Card</h3>
            <br /><br />
            <Row>
              <Col>
                <Form>
                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 3, offset: 1 }}>Card Number</Form.Label>
                    <Col sm="6" style={headerStyle}>
                      <Form.Control type="text" placeHolder="ex. 1234123412341234" onChange={this.cardNoChange} className={`form-control ${this.state.cardNoValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.cardNoValid == 1 ? "Please enter your card number." : "Invalid card number entered. Please check."}</Form.Control.Feedback>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 2, offset: 1 }}>CVV</Form.Label>
                    <Col sm="3" style={headerStyle}>
                      <Form.Control type="text" placeHolder="3 or 4 digit" onChange={this.cvvNoChange} className={`form-control ${this.state.cvvNoValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.cvvNoValid == 1 ? "Please enter your Card Verification Value." : "Invalid CVV entered. Please check."}</Form.Control.Feedback>
                    </Col>
                    <Form.Label column sm={{ span: 2, offset: 0 }}>Exp. Date</Form.Label>
                    <Col sm="3" style={headerStyle}>
                      <Form.Control type="text" placeHolder="mm/yy" onChange={this.expDateChange} className={`form-control ${this.state.expDateValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.expDateValid == 1 ? "Please enter your card expiry date." : "Invalid date entered. Please check."}</Form.Control.Feedback>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 3, offset: 1 }}>First name</Form.Label>
                    <Col sm="6" style={headerStyle}>
                      <Form.Control type="text" placeHolder="ex. Prayuth" onChange={this.firstnameChange} className={`form-control ${this.state.firstnameValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.firstnameValid == 1 ? "What’s your name?" : "Invalid name entered. Please check."}</Form.Control.Feedback>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 3, offset: 1 }}>Last name</Form.Label>
                    <Col sm="6" style={headerStyle}>
                      <Form.Control type="text" placeHolder="ex. Chan-o-cha" onChange={this.lastnameChange} className={`form-control ${this.state.lastnameValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.lastnameValid == 1 ? "What’s your last name?" : "Invalid last name entered. Please check."}</Form.Control.Feedback>
                    </Col>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
            <Form>
              <Form.Group as={Row}>
                <Col sm={{ span: 1 }}>
                  <Form.Control plaintext readOnly />
                </Col>
              </Form.Group>
            </Form>
            <Row>
              <Col style={headerStyle}>
                <Button onClick={this.onPayCardButton}>Confirm</Button>
              </Col>
            </Row>
          </Col>

          <Col>
            <h3 style={headerStyle}>Pay via Bank Transfer</h3>
            <br /><br />
            <Row>
              <Col>
                <Form>
                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 5, offset: 1 }}>Contractor Bank Account</Form.Label>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Col sm={{ span: 5, offset: 1 }} style={headerStyle}>
                      <Form.Control type="text" readOnly defaultValue={this.state.contractorBankNo} />
                    </Col>
                    <Col sm={{ span: 5, offset: 0 }} style={headerStyle}>
                      <Form.Control type="text" readOnly defaultValue={this.state.contractorBankname} />
                    </Col>

                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 3, offset: 1 }}>Total</Form.Label>
                    <Col sm={{ span: 6, offset: 1 }} style={headerStyle}>
                      <Form.Control type="text" readOnly defaultValue={this.state.cost} />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 3, offset: 1 }}>Bank Name</Form.Label>
                    <Col sm={{ span: 6, offset: 1 }} style={headerStyle}>
                      <Form.Control type="text" placeHolder="ex. Kasikorn" onChange={this.banknameChange} className={`form-control ${this.state.banknameValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.banknameValid == 1 ? "Please enter the bank name of your transfer account." : "Entered bank name is too long."}</Form.Control.Feedback>
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column sm={{ span: 4, offset: 1 }}>Transfer Date&time (dd/mm/yyyy hh:mm) </Form.Label>
                    <Col sm="6" style={headerStyle}>
                      <Form.Control type="datetime-local" placeHolder="dd/mm/yyyy hh:mm" onChange={this.transferDateTimeChange} className={`form-control ${this.state.transferDateTimeValid == 0 ? '' : 'is-invalid'}`} />
                      <Form.Control.Feedback type="invalid">{this.state.transferDateTimeValid == 1 ? "Invalid date&time entered. Please check." : "Invalid date&time format entered. Please check."}</Form.Control.Feedback>
                    </Col>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
            <Row>
              <Col style={headerStyle}>
                <Button onClick={this.onPayBankButton}>Confirm</Button>
              </Col>
            </Row>
          </Col>
        </Row>


      </div>
    );

  }

}

const forceCenter = {
  width: "100vh",
  paddingLeft: "2%",
  paddingRight: "2%"
}

const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default Payment;