import React, { Component } from "react";
import { Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
export class RequestDashboard extends Component {
  constructor() {
    super();

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.fetchAllRequest = this.fetchAllRequest.bind(this);
    this.onFeedbackSaveChangesButton = this.onFeedbackSaveChangesButton.bind(this);

    this.state = {
      show: false,
      list: [],
      caseID: null,
      feedback_comment: "",
      feedback_rating: "",
      saveChangeButtonDisable: true
    };
  }

  componentDidMount() {
    if (localStorage.getItem('userType') !== 'contractor' && localStorage.getItem('userType') !== 'homeowner') {
      window.location.href = "/Login";
    } else {
      this.fetchAllRequest();
    }
  }

  onFeedbackCommentChange = e => {
    //console.log(e.target.value);
    if (e.target.value.trim().length > 255) {
      e.target.value = e.target.value.substring(0, 255);
    }
    this.setState({ feedback_comment: e.target.value.trim() });

    if (String(this.state.feedback_rating).length == 1) {
      this.setState({ saveChangeButtonDisable: false });
    }
  };

  onFeedbackRatingChange = e => {
    //console.log(e.target.value);
    let regex = /^[0-5]{1,2}$/
    if (regex.test(e.target.value)) {
      if (e.target.value.length >= 2) {
        e.target.value = e.target.value.substring(1);
      }
      this.setState({
        feedback_rating: e.target.value,
        saveChangeButtonDisable: false
      });
    } else {
      if (e.target.value.length < 2) {
        e.target.value = "";
      } else {
        e.target.value = e.target.value.substring(0, 1);
      }
    }
    if (e.target.value.length === 0) {
      this.setState({
        saveChangeButtonDisable: true
      });
    }
    //this.setState({ password: e.target.value });
    //console.log(this.state);
  };

  onCaseDetailButton = (request) => {
    sessionStorage.setItem('caseID', request.caseid);
    window.location.href = "/CaseDetail";
  }

  onPayButton = (request) => {
    sessionStorage.setItem('paymentCaseID', request.caseid);
    window.location.href = "/Payment";
  }

  async handleClose() {
    this.setState({ show: false });
  }

  async handleShow(request) {
    this.setState({
      show: true,
      caseID: request.caseid,
      feedback_rating: request.feedback_rating === null ? "" : request.feedback_rating,
      feedback_comment: request.feedback_comment === null ? "" : request.feedback_comment,
      saveChangeButtonDisable: true
    });

  }

  async fetchAllRequest() {
    try {
      const data = {};
      if (localStorage.getItem('userType') == 'homeowner') {
        data.user_type = '1';
        data.fda_cer = "";
        data.NatID = localStorage.getItem('NatID');
      } else if (localStorage.getItem('userType') == 'contractor') {
        data.user_type = '0';
        data.fda_cer = localStorage.getItem('fda_cer');
        data.NatID = "";
      }
      console.log(data);
      const response = await fetch("/booking/getAllRequest", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      this.setState({ list: results });
    } catch (error) {
      console.log("Request fetch failed", error);
    }
  }

  async onFeedbackSaveChangesButton() {
    try {
      const data = {};
      data.caseid = this.state.caseID;
      data.rating = this.state.feedback_rating;
      data.comment = this.state.feedback_comment;
      console.log(data);
      const response = await fetch("/feedback/giveFeedback", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        this.fetchAllRequest();
        this.handleClose();
      }
    } catch (error) {
      console.log("Give feedback failed", error);
    }
  }

  render() {
    let i = 1;
    return (
      <div class="Page">
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Feedback of CaseID: {this.state.caseID}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group>
                <Form.Label>Comment</Form.Label>
                {localStorage.getItem('userType') === 'contractor' ?
                  <Form.Control as="textarea" rows="3" readOnly defaultValue={this.state.feedback_comment} />
                  :
                  <Form.Control as="textarea" rows="3" onChange={this.onFeedbackCommentChange} defaultValue={this.state.feedback_comment} />}

                <Form.Label>☆ Rating [ 0 - 5 ]</Form.Label>
                {localStorage.getItem('userType') === 'contractor' ?
                  <Form.Control type="text" readOnly defaultValue={this.state.feedback_rating} />
                  :
                  <Form.Control type="text" onChange={this.onFeedbackRatingChange} defaultValue={this.state.feedback_rating} />
                }
              </Form.Group>
            </Form>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
                  </Button>
            {localStorage.getItem('userType') === 'contractor' ? "" :
              this.state.saveChangeButtonDisable ?
                <Button variant="primary" disabled>Save Changes</Button> :
                <Button variant="primary" onClick={this.onFeedbackSaveChangesButton} >Save Changes</Button>
            }
          </Modal.Footer>
        </Modal>


        <div style={headerStyle3}>
          <h3> Request Dashboard</h3>
        </div>
        <div class="PageBody2">
          <Table striped bordered hover responsive id="ConTable">
            <thead>
              <th>#</th>{/*CaseID*/}
              <th>Request</th>{/*request description*/}
              <th>{localStorage.getItem('userType') == 'contractor' ? "Homeowner" : "Contractor"}</th>
              <th>Request Date</th>
              <th>Service Date</th>
              <th>Price</th>
              <th>Status</th>
              <th>feedback</th>
              {localStorage.getItem('userType') == 'homeowner' ? <th>payment</th> : null}
              <th>Detail</th>
            </thead>
            <tbody>

              {this.state.list.map((request) =>
                <tr>
                  <td>{i++}</td>
                  <td>{request.type_name}</td>
                  <td>{localStorage.getItem('userType') == 'contractor' ? (request.firstname + " " + request.lastname) : request.comp_name}</td>
                  <td>{request.request_date}</td>
                  <td>{request.service_date}</td>
                  <td>{request.price}</td>
                  <td>{request.status_name}</td>
                  <td style={headerStyle}><Button size="sm" variant="success" onClick={() => this.handleShow(request)}>{request.feedback_rating !== null ? request.feedback_rating + "☆" : "no rating"}</Button></td>
                  {localStorage.getItem('userType') == 'homeowner' ?
                    (
                      (request.status_name === "deposit pending" || request.status_name === "payment pending") ?
                        <td style={headerStyle}><Button size="sm" variant="success" onClick={() => this.onPayButton(request)}>PAY</Button></td>
                        : <td style={headerStyle}><Button size="sm" variant="success" disabled >PAY</Button></td>
                    )
                    : null
                  }
                  <td style={headerStyle}><Button size="sm" variant="success" onClick={() => this.onCaseDetailButton(request)}>Detail</Button></td>
                </tr>
              )}

            </tbody>
          </Table>

        </div>


      </div>
    );

  }

}



const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default RequestDashboard;