import React, { Component } from "react";
import { Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
export class Admin extends Component {
  constructor() {
    super();
    //this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.loginAdmin = this.loginAdmin.bind(this);
    this.logoutAdmin = this.logoutAdmin.bind(this);
    this.GoBackHomeKid = this.GoBackHomeKid.bind(this);

    this.validateUsername = this.validateUsername.bind(this);       //return 0 if OK
    this.validatePassword = this.validatePassword.bind(this);       //else return 1
    this.validateAllField = this.validateAllField.bind(this);

    this.fetchAllContractor = this.fetchAllContractor.bind(this);
    this.confirmContractor = this.confirmContractor.bind(this);

    this.state = {
      isLogin: false,
      user_type: "",
      username: "",
      password: "",
      errorMessage: "",
      show: true,

      usernameValid: 0,   //0 mean Valid. Other numbers are specific error codes
      passwordValid: 0,   //1 is empty. 2 is invalid. 3 is duplicated.

      list: []
    };
  }
  onUsernameBarChange = e => {
    this.setState({ username: e.target.value.trim() }, () => this.validateUsername());
    //console.log(this.state);
  };

  onPasswordBarChange = e => {
    this.setState({ password: e.target.value }, () => this.validatePassword());
    //console.log(this.state);
  };

  async GoBackHomeKid(){
    localStorage.clear();
    sessionStorage.clear();
    window.location.href="/";
  }
  async handleClose() {
    try {
      if (this.state.isLogin === false) {
        this.setState({ show: true });
      }
      else if (this.state.userame === "") {
        this.setState({ show: true });
      }
      else if (this.state.password === "") {
        this.setState({ show: true });
      }
      else if (this.state.user_type !== "99") {
        this.setState({ show: true });
      }
      else {
        this.setState({ show: false });
      }
    }
    catch (error) {
      console.log("Something wrong", error);
    }
  };

  async loginAdmin() {
    if (this.validateAllField() == 0) {
      try {
        //console.log(this.state);
        this.setState({ user_type: "99" })
        const data = this.state;
        data.user_type = "99";
        //console.log(JSON.stringify(data));
        const response = await fetch("/user/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
        const results = await response.json();
        //console.log(results, results.status);
        if (results.status === "wrong username or password") {
          this.setState({ errorMessage: "Invalid Password" });
        }
        else if (results.status === "no account") {
          this.setState({ errorMessage: "Account not exist!" });
        }
        else if (results.status === "no user") {
          this.setState({ errorMessage: "No user" });
        }
        else {
          localStorage.clear();
          sessionStorage.clear();
          this.setState({ isLogin: true, show: false });
          this.fetchAllContractor();
          localStorage.setItem('userType', 'Admin')
        }
      } catch (error) {
        console.log("Login Failed", error);
      }
    } else {
      alert("Error. Please check your username and password");
    }
  }

  async logoutAdmin() {
    localStorage.setItem('userType', "");
    localStorage.clear();
    sessionStorage.clear();
    //this.setState({ isLogin: false, show: true });
    window.location.href = '/';
  }

  async fetchAllContractor() {
    const response = await fetch("/contractor/all", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    });
    const results = await response.json();
    console.log(results);
    this.setState({ list: results });
  }

  async confirmContractor(contractor) {
    try {
      //console.log(this.state);
      const data = contractor;
      data.user_type = "99";
      //console.log(JSON.stringify(data));
      const response = await fetch("/admin/confirm", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        this.fetchAllContractor();
      }else{
        alert("Confirm Failed");
      }

    } catch (error) {
      console.log("Confirm Failed", error);
    }
  }

  validateUsername() {
    let input = this.state.username;
    let regex = /^\S{1,15}$/
    if (regex.test(input)) {
      this.setState({
        usernameValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          usernameValid: 1
        });
      } else {
        this.setState({
          usernameValid: 2
        });
      }
      return 1;
    }
  }

  validatePassword() {
    let input = this.state.password;
    let regex = /^.{8,20}$/
    if (regex.test(input)) {
      this.setState({
        passwordValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          passwordValid: 1
        });
      } else {
        this.setState({
          passwordValid: 2
        });
      }
      return 1;
    }
  }

  validateAllField() {
    return (this.validateUsername() + this.validatePassword()) == 0 ? 0 : 1;
  }

  render() {
    let i = 1;
    return (
      <div class="Page">

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>What's up, Admin</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div id="loginbox" style={headerStyle}>
              <Form>
                <Row className="justify-content-md-center" >
                  <Col md={{ span: 6 }}>
                    <FormControl id="usernameLogin" type="text" placeholder="Username" onChange={this.onUsernameBarChange} className={`form-control ${this.state.usernameValid == 0 ? '' : 'is-invalid'}`} />
                    <Form.Control.Feedback type="invalid">{this.state.usernameValid == 1 ? "Please enter your username." : "Username must be at most 15 characters with no space in between."}</Form.Control.Feedback>
                  </Col>
                </Row>
                <br />
                <Row className="justify-content-md-center" >
                  <Col md={{ span: 6 }}>
                    <FormControl id="passwordLogin" type="password" placeholder="Password" onChange={this.onPasswordBarChange} className={`form-control ${this.state.passwordValid == 0 ? '' : 'is-invalid'}`} />
                    <Form.Control.Feedback type="invalid">{this.state.passwordValid == 1 ? "Please enter your password." : "Password must be between 8 to 20 characters."}</Form.Control.Feedback>
                  </Col>
                </Row>
                <br />
              </Form>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.GoBackHomeKid}>
              Home
            </Button>
            <Button variant="primary" onClick={this.loginAdmin}>
              login
            </Button>
          </Modal.Footer>
        </Modal>
        <div style={headerStyle3}>
          <h3> Contractor List</h3>
        </div>
        <div class="PageBody2">
          <Table striped bordered hover responsive id="ConTable">
            <thead>
              <th>#</th>
              <th colSpan="1">Company Name</th>
              <th>Tax ID</th>
              <th>FDA Certificate</th>
              <th>Confirm Status</th>
            </thead>
            <tbody>
              {this.state.list.map((contractor) =>
                <tr>
                  <td>{i++}</td>
                  <td>{contractor.comp_name}</td>
                  <td>{contractor.tax_id}</td>
                  {contractor.photo_fdacer === null ? <td style={headerStyle}>No image</td> : <td style={headerStyle}><Button variant="secondary" href={contractor.photo_fdacer} target="_blank">Image</Button></td>}
                  
                  { contractor.verify_status===0 ?
                  <td style={headerStyle}><Button size="sm" variant="success" onClick={() => {
                    this.confirmContractor(contractor);
                  }}>Confirm</Button></td> 
                  : 
                  <td style={headerStyle}><Button size="sm" variant="success" disabled>Verified</Button></td>
                  }

                </tr>
              )}
            </tbody>

          </Table>

          <Button variant="primary" onClick={this.logoutAdmin}>logout</Button>

        </div>
      </div>
    );

  }

}


const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default Admin;