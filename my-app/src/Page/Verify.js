import React, { Component } from "react";
import {Image,Carousel, Jumbotron, Button, Form, FormControl, Row, Col } from 'react-bootstrap';
export class VerifyEmail extends Component {
    constructor() {
        super();
    }

    render() {

        return (
            <div class="Page">
            <div class="PageBody3">
              <Jumbotron  style={headerStyle} >
                <h1> Your Email Has Been Verified!! </h1>
                <p style={paragraphStyle}>
                  Please enjoy.
                </p>
                <p>
                  Thank you
                </p>
                <Row style={paragraphStyle} >
                  <Col>
                    <Button href="/Login" variant="primary">Go to Login</Button>
                  </Col>
                  <Col>
                    <Button href="/" variant="primary">Go to Homepage</Button>
                  </Col>
                </Row>
              </Jumbotron>
              </div>
            </div>
        );

    }

}


const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default VerifyEmail;