import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal, Card } from 'react-bootstrap';
//import Image from 'react-image';
import ContractorServiceListingBox from "./ContractorServiceListingBox2";
import AddServiceForm from "../component/AddServiceForm";
import { CardDeck, Container } from 'react-bootstrap';
import LocationPicker from 'react-location-picker';
export class Profile extends Component {
  constructor() {
    super();
    this.handleClose = this.handleClose.bind(this);
    this.state = {
      // lat: parseFloat(localStorage.getItem('latitude')),
      // lng: parseFloat(localStorage.getItem('longitude')),
      profile: [], services: [], error: false,
      show: false,
    }
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.AddServiceHandle = this.AddServiceHandle.bind(this);
    this.fetchContractorDetail = this.fetchContractorDetail.bind(this);
    this.fetchHomeownerDetail = this.fetchHomeownerDetail.bind(this);
    this.handleDashBoard = this.handleDashBoard.bind(this);
  }

  AddServiceHandle() {
    if (this.state.profile.verify_status !== 1) {
      alert("Can't add services.\nYour account haven't been verified yet!");
    } else {
      this.setState({ show: true });
    }
  }



  componentDidMount() {
    if ((localStorage.getItem('userType') == 'homeowner')) {
      this.fetchHomeownerDetail();
    } else {
      this.fetchContractorDetail();
    }
  }

  async fetchContractorDetail() {
    fetch("/contractor/get_detail/" + localStorage.getItem('fda_cer'))
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Internal error');
        }
      })
      .then(data => {
        if (data.length !== 0) {
          this.setState({ profile: data[0], services: data.slice(1) });
        } else {
          this.setState({ profile: false });
        }
      })
      .catch((error) => {
        this.setState({ error: true });
        console.log(error);
      });
  }

  async fetchHomeownerDetail() {
    try {
      const data = {};
      data.NatID = localStorage.getItem("NatID")
      console.log(data);
      const response = await fetch("/homeowner/getHomeownerDetail", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.length !== 0) {
        this.setState({ profile: results[0] });
      } else {
        this.setState({ profile: false });
      }
    } catch (error) {
      console.log("Give feedback failed", error);
    }
  }



  handleLocationChange({ position, address }) {
  }

  EditHandle() {
    if (localStorage.getItem('userType') == 'homeowner') {
      window.location.href = "/EditHome";
    }
    else if (localStorage.getItem('userType') == 'contractor') {
      window.location.href = "/EditCon";
    }

  }

  handleDashBoard() {
    window.location.href = '/RequestDashboard';
  }

  async handleClose() {
    this.setState({ show: false });
  }
  render() {
    console.log(localStorage.getItem('userType'));
    let profileBlock, services;
    if (localStorage.getItem('userType') == 'homeowner') profileBlock =
      <div>
        <Form>
          <Form.Group as={Row}>
            <Form.Label column sm={{ span: 2, offset: 1 }}>First name</Form.Label>
            <Col sm="2" style={headerStyle}>
              <Form.Control disabled defaultValue={this.state.profile.firstname} />
            </Col>

            <Form.Label column sm={{ span: 2, offset: 0 }}>Last name</Form.Label>
            <Col sm="2" style={headerStyle}>
              <Form.Control disabled defaultValue={this.state.profile.lastname} />
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={{ span: 2, offset: 1 }}>National ID Number</Form.Label>
            <Col sm="6" style={headerStyle}>
              <Form.Control disabled defaultValue={this.state.profile.NatID} />
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={{ span: 2, offset: 1 }}>Address</Form.Label>
            <Col sm="6" style={headerStyle}>
              <Form.Control as="textarea" rows="3" disabled value={this.state.profile.addr} />
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={{ span: 2, offset: 1 }}>Phone</Form.Label>
            <Col sm="2" style={headerStyle}>
              <Form.Control disabled defaultValue={this.state.profile.phone} />
            </Col>

            <Form.Label column sm={{ span: 1, offset: 0 }}>Email</Form.Label>
            <Col sm="3" style={headerStyle}>
              <Form.Control disabled defaultValue={this.state.profile.email} />
            </Col>
          </Form.Group>
        </Form>
      </div>

    else if (localStorage.getItem('userType') == 'contractor') {
      let paymentMethods = '';
      if (this.state.profile.paymentByCredit == 1)
        paymentMethods += 'Credit Card';
      if (this.state.profile.paymentByBank == 1) {
        if (paymentMethods.length > 0) {
          paymentMethods += ' / '
        }
        paymentMethods += 'Bank Transfer ';
      }
      if (!this.state.error) {
        if (this.state.profile !== false) {
          services = this.state.services.length > 0 ? this.state.services.map(resultItem => (
            <ContractorServiceListingBox
              photo_url={resultItem.photo_url}
              id={resultItem.id}  //service_id
              fda_cer={resultItem.fda_cer}
              type={resultItem.type_name}
              name={resultItem.name}
              price={resultItem.price}
              description={resultItem.descr}
              rating={resultItem.rating}
              contractor={resultItem.comp_name}
              typeid={resultItem.type_id}
              startTime={resultItem.start_time}
              endTime={resultItem.end_time}
              warranty={resultItem.warranty}
              dailyLimit={resultItem.daily_limit}
              reloadData={() => this.componentDidMount()}
            />
          )) : null;
        } else { }
      } else { services = null; }


      profileBlock =
        <div>
          <Form>
            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Contractor</Form.Label>
              <Col sm="2" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.comp_name} />
              </Col>

              <Form.Label column sm={{ span: 2, offset: 0 }}>FDA Certification NO.</Form.Label>
              <Col sm="2" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.fda_cer} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Tax ID</Form.Label>
              <Col sm="2" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.tax_id} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Address</Form.Label>
              <Col sm="6" style={headerStyle}>
                <Form.Control as="textarea" rows="3" disabled value={this.state.profile.addr} />
              </Col>
            </Form.Group>

            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Phone</Form.Label>
              <Col sm="2" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.phone} />
              </Col>

              <Form.Label column sm={{ span: 1, offset: 0 }}>Email</Form.Label>
              <Col sm="3" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.email} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Rating</Form.Label>
              <Col sm="2" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.rating} />
              </Col>

              <Form.Label column sm={{ span: 1, offset: 0 }}>Payment</Form.Label>
              <Col sm="3" style={headerStyle}>
                <Form.Control disabled value={paymentMethods} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Bank Account</Form.Label>
              <Col sm="2" style={headerStyle}>
                <Form.Control disabled value={this.state.profile.bank_account} />
              </Col>
              <Form.Label column sm={{ span: 1, offset: 0 }}>Bank Name</Form.Label>
              <Col sm="3" style={headerStyle}>
                <Form.Control disabled defaultValue={this.state.profile.bankname} />
              </Col>
            </Form.Group>

          </Form>

        </div>
    }

    console.log(__dirname);

    return (
      <div class="Page">

        <AddServiceForm show={this.state.show} handleClose={() => this.handleClose()} reloadData={() => this.fetchContractorDetail()} />

        <br />
        <div style={headerStyle}>
          <div align="center">
            <Image class="Infoimage" width={250} height={250} src={this.state.profile.photo_url} thumbnail fluid />
          </div>
          <br />
          <div style={headerStyle}>
            <h2>{localStorage.getItem('username')}</h2>
          </div>
          <br />
        </div>
        <Row>
          <Col style={force}>
            <Row>
              <Col>
                {profileBlock}
              </Col>
            </Row>
            <Form.Group as={Row}>
              <Form.Label column sm={{ span: 2, offset: 1 }}>Location</Form.Label>
              <Col align="center" sm="6">
                <LocationPicker
                  containerElement={<div style={{ width: '100%', height: '100%' }} />}
                  mapElement={<div style={{ height: '400px' }} />}
                  defaultPosition={{ lat: this.state.profile.latitude, lng: this.state.profile.longitude }}
                  onChange={this.handleLocationChange}
                />
              </Col>
            </Form.Group>
            <Row>
              {localStorage.getItem('userType') == 'contractor' ?
                (
                  <Col>
                    {this.state.profile && !this.state.error ?
                      <Container>
                        <Form.Group as={Row}>
                          <Form.Label column sm={{ span: 2, offset: 1 }}>Service</Form.Label>
                        </Form.Group>
                        <Form.Group as={Row}>
                          <Col>
                            <CardDeck id="Carddeck">
                              {services}
                              <Card style={{ maxWidth: '15rem' }}>
                                <Card.Img />
                                <Card.Body align="center" style={{ marginTop: '50%', marginBottom: '50%' }}>
                                  <FontAwesomeIcon size="7x" icon="plus-circle" onClick={this.AddServiceHandle} />

                                  <Card.Title ></Card.Title>
                                  <Card.Text>

                                  </Card.Text>

                                </Card.Body>
                              </Card>
                            </CardDeck>
                          </Col>
                        </Form.Group>
                      </Container>
                      :
                      null
                    }
                  </Col>
                )
                :
                (
                  null
                )
              }
            </Row>
          </Col>
        </Row>


        <br /><br />
        <Row>
          <Col style={headerStyle}>
            <Button onClick={this.handleDashBoard} >Dashboard</Button>
          </Col>
          <Col style={headerStyle}>
            <Button onClick={this.EditHandle} >Edit Profile</Button>
          </Col>
          <Col style={headerStyle}>
            <Button variant="danger">Logout</Button>
          </Col>

        </Row>
        <br /><br />


      </div>
    );

  }

}

const force = {
  //marginLeft: "10%",
}

const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  align: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default Profile;