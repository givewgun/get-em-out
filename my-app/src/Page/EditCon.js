import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
import LocationPicker from 'react-location-picker';
const axios = require("axios");
const defaultPosition = {
    lat: parseFloat(localStorage.getItem('latitude')),
    lng: parseFloat(localStorage.getItem('longitude')),
};

export class EditCon extends Component {
    constructor() {
        super();
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onPictureSubmit = this.onPictureSubmit.bind(this);
        this.profilePictureChange = this.profilePictureChange.bind(this);
        this.saveHandle = this.saveHandle.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.FDACerChange = this.FDACerChange.bind(this);
        this.compnameChange = this.compnameChange.bind(this);
        this.taxIDChange = this.taxIDChange.bind(this);
        this.addrChange = this.addrChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.phoneChange = this.phoneChange.bind(this);
        this.bankaccountChange = this.bankaccountChange.bind(this);
        this.banknameChange = this.banknameChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.payByCreditChange = this.payByCreditChange.bind(this);
        this.payByBankChange = this.payByBankChange.bind(this);

        this.validateUsername = this.validateUsername.bind(this);       //return 0 if OK
        this.validateFda_cer = this.validateFda_cer.bind(this);  //else return 1
        this.validateCompname = this.validateCompname.bind(this);
        this.validateTax_id = this.validateTax_id.bind(this);
        this.validateAddr = this.validateAddr.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.validatePhone = this.validatePhone.bind(this);
        this.validateBankaccount = this.validateBankaccount.bind(this);
        this.validateBankName = this.validateBankName.bind(this);
        this.validateAllField = this.validateAllField.bind(this);

        this.validateProfilePicture = this.validateProfilePicture.bind(this);

        this.state =
            {
                username: localStorage.getItem('username'),
                fda_cer: localStorage.getItem('fda_cer'),
                comp_name: localStorage.getItem('comp_name'),
                tax_id: localStorage.getItem('tax_id'),
                addr: localStorage.getItem('addr'),
                email: localStorage.getItem('email'),
                phone: localStorage.getItem('phone'),
                bank_account: localStorage.getItem('bank_account'),
                bank_name: localStorage.getItem('bank_name'),
                latitude: localStorage.getItem('latitude'),
                longitude: localStorage.getItem('longitude'),
                photo_url: localStorage.getItem('photo_url'), //latest uploaded & validated photo URL or initial photo URL
                temp_photo: null,
                temp_valid_photo: null,  //latest uploaded & validated photo
                paymentByCredit: localStorage.getItem('paymentByCredit'),
                paymentByBank: localStorage.getItem('paymentByBank'),

                usernameValid: 0,   //0 mean Valid. Other numbers are specific error codes
                passwordValid: 0,   //1 is empty. 2 is invalid. 3 is duplicated.
                fdacerValid: 0,
                compnameValid: 0,
                taxidValid: 0,
                addrValid: 0,
                emailValid: 0,
                phoneValid: 0,
                bankaccountValid: 0,
                banknameValid: 0,

                isUpload: 0,
                user_type: 0, //0 = contractor
                profilePictureValid: 0, //0 OK, 1 null, 2 invalid type, 3 file size too large
            };
    }

    cancelHandle() {
        window.location.href = "Profile";
    }

    saveHandle() {
        //fetch ส่ง back 
        this.onFormSubmit();
        if (this.state.isUpload === 1) this.onPictureSubmit();
        alert("Profile Successfully Edited");
        window.location.href = "Profile";
    }

    async onFormSubmit() {
        console.log(this.state);
        if (this.validateAllField() == 0) {
            try {
                const data = this.state;
                const response = await fetch("/user/edit_profile", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                });
                localStorage.setItem('username', this.state.username);
                localStorage.setItem('comp_name', this.state.comp_name);
                //localStorage.setItem('fda_cer', this.state.fda_cer);
                //localStorage.setItem('tax_id', this.state.tax_id);
                localStorage.setItem('addr', this.state.addr);
                localStorage.setItem('email', this.state.email);
                localStorage.setItem('phone', this.state.phone);
                localStorage.setItem('bank_account', this.state.bank_account);
                localStorage.setItem('bank_name', this.state.bank_name);
                localStorage.setItem('latitude', this.state.latitude);
                localStorage.setItem('longitude', this.state.longitude);
                //localStorage.setItem('photo_url', this.state.photo_url);
                localStorage.setItem('paymentByCredit', this.state.paymentByCredit);
                localStorage.setItem('paymentByBank', this.state.paymentByBank);
            } catch (error) {
                console.log("Form Submission Failed", error);
            }
            
        } else {
            alert("Error. Please check your form information");
        }
    }

    async onPictureSubmit() //not edited yet, only profile pics will be uploaded
    {
        try {
            //console.log(this.state);
            const formData = new FormData();
            formData.set('fda_cer', this.state.fda_cer);
            formData.append('profile_image', this.state.temp_valid_photo);
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                },
            };
            axios.post("/user/upload/profile/0", formData, config)
                .then((response) => {
                }).catch((error) => {
                    alert("Failed to submit picture");
                });
        } catch (error) {
            console.log("ProfilePic Submission Failed", error);
        }
    }

   

    async profilePictureChange(event) {
        console.log(event.target.files[0]);
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                temp_photo: event.target.files[0]
            }, () => this.validateProfilePicture());
        } else {
            this.setState({
                temp_photo: null
            }, () => this.validateProfilePicture());
        }
    }

    async usernameChange(event) {
        this.setState({
            username: event.target.value.trim()
        }, () => this.validateUsername());
    }

    async passwordChange(event) {
        this.setState({
            password: event.target.value
        }, () => this.validatePassword());
    }

    async FDACerChange(event) {
        this.setState({
            fda_cer: event.target.value.trim()
        }, () => this.validateFda_cer());
    }

    async compnameChange(event) {
        this.setState({
            comp_name: event.target.value.trim()
        }, () => this.validateCompname());
    }
    async taxIDChange(event) {
        this.setState({
            tax_id: event.target.value.trim()
        }, () => this.validateTax_id());
    }
    async addrChange(event) {
        this.setState({
            addr: event.target.value.trim()
        }, () => this.validateAddr());
    }
    async emailChange(event) {
        this.setState({
            email: event.target.value.trim()
        }, () => this.validateEmail());
    }
    async phoneChange(event) {
        this.setState({
            phone: event.target.value.trim()
        }, () => this.validatePhone());
    }
    async bankaccountChange(event) {
        this.setState({
            bank_account: event.target.value.trim()
        }, () => this.validateBankaccount());
    }

    async banknameChange(event) {
        this.setState({
            bank_name: event.target.value.trim()
        }, () => this.validateBankName());
    }

    async payByCreditChange(event) {
        this.setState({
            paymentByCredit: 1 - this.state.paymentByCredit
        }, function () { console.log(this.state.paymentByCredit); });
    }

    async payByBankChange(event) {
        this.setState({
            paymentByBank: 1 - this.state.paymentByBank
        }, function () { console.log(this.state.paymentByBank); });

    }

    handleLocationChange({ position, address }) {
        this.setState({ latitude: position.lat, longitude: position.lng });
    }

    validateUsername() {
        let input = this.state.username;
        let regex = /^\S{1,15}$/
        if (regex.test(input)) {
            this.setState({
                usernameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    usernameValid: 1
                });
            } else {
                this.setState({
                    usernameValid: 2
                });
            }
            return 1;
        }
    }


    validateFda_cer() {
        let input = this.state.fda_cer;
        let regex = /^.{1,15}$/
        if (regex.test(input)) {
            this.setState({
                fdacerValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    fdacerValid: 1
                });
            } else {
                this.setState({
                    fdacerValid: 2
                });
            }
            return 1;
        }
    }

    validateCompname() {
        let input = this.state.comp_name;
        let regex = /^.{1,60}$/
        if (regex.test(input)) {
            this.setState({
                compnameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    compnameValid: 1
                });
            } else {
                this.setState({
                    compnameValid: 2
                });
            }
            return 1;
        }
    }

    validateTax_id() {
        let input = this.state.tax_id;
        let regex = /^[0-9]{13}$/
        if (regex.test(input)) {
            this.setState({
                taxidValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    taxidValid: 1
                });
            } else {
                this.setState({
                    taxidValid: 2
                });
            }
            return 1;
        }
    }

    validateAddr() {
        let input = this.state.addr;
        let regex = /^.{1,150}$/
        if (regex.test(input)) {
            this.setState({
                addrValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    addrValid: 1
                });
            } else {
                this.setState({
                    addrValid: 2
                });
            }
            return 1;
        }
    }

    validateEmail() {
        let input = this.state.email;
        let regex = /\S+@\S+\.\S+/
        if (regex.test(input)) {
            this.setState({
                emailValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    emailValid: 1
                });
            } else {
                this.setState({
                    emailValid: 2
                });
            }
            return 1;
        }
    }

    validatePhone() {
        let input = this.state.phone;
        let regex = /^[0-9]{9,10}$/     //regex for allowing only number charactor of length 9 to 10
        if (regex.test(input)) {
            this.setState({
                phoneValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    phoneValid: 1
                });
            } else {
                this.setState({
                    phoneValid: 2
                });
            }
            return 1;
        }
    }

    validateBankaccount() {
        let input = this.state.bank_account;
        let regex = /^[0-9]{10,15}$/
        if (regex.test(input)) {
            this.setState({
                bankaccountValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    bankaccountValid: 1
                });
            } else {
                this.setState({
                    bankaccountValid: 2
                });
            }
            return 1;
        }
    }

    validateBankName() {
        let input = this.state.addr;
        let regex = /^.{1,150}$/
        if (regex.test(input)) {
            this.setState({
                banknameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    banknameValid: 1
                });
            } else {
                this.setState({
                   banknameValid: 2
                });
            }
            return 1;
        }
    }

    validateAllField() {
        return (this.validateUsername() + this.validateFda_cer() + this.validateCompname() + this.validateTax_id() + this.validateAddr() + this.validateEmail() + this.validatePhone() + this.validateBankaccount() + this.validateBankName()) == 0 ? 0 : 1;
    }

    validateProfilePicture() {
        let input = this.state.temp_photo;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension
        console.log(input);

        if (input === null) {
            this.setState({
                profilePictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        profilePictureValid: 0,
                        isUpload: 1,
                        temp_valid_photo: input,
                        photo_url: URL.createObjectURL(input)
                    });
                    return 0
                } else {
                    this.setState({
                        profilePictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    profilePictureValid: 2
                });
                return 1;
            }
        }
    }

    render() {

        return (
            <div>
                <div>
                    <h1 style={format}>Edit Your Profile</h1>
                    <Form style={format}>
                        {/*<Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridUsername">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control type="text" placeholder="Enter username" onChange={this.usernameChange} className={`form-control ${this.state.usernameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.usernameValid == 1 ? "Please enter your username." : this.state.usernameValid == 2 ? "Username must be at most 15 characters with no space in between." : "This username has already been taken."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" onChange={this.passwordChange} className={`form-control ${this.state.passwordValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.passwordValid == 1 ? "Please enter your password." : "Password must be between 8 to 20 characters."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>*/}

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} controlId="formGridCompName">
                                <Form.Label>Company name</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('comp_name')} onChange={this.compnameChange} className={`form-control ${this.state.compnameValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.compnameValid == 1 ? "Please enter your company name" : "The entered name is too long"}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridFDACer">
                                <Form.Label>FDA Certification number</Form.Label>
                                <Form.Control disabled defaultValue={localStorage.getItem('fda_cer')} />
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridTaxID">
                                <Form.Label>Tax ID</Form.Label>
                                <Form.Control disabled defaultValue={localStorage.getItem('tax_id')} />
                                </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} controlId="formGridAddress">
                                <Form.Label>Address</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('addr')} onChange={this.addrChange} className={`form-control ${this.state.addrValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.addrValid == 1 ? "Please fill your address." : "Entered address is too long."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" defaultValue={localStorage.getItem('email')} onChange={this.emailChange} className={`form-control ${this.state.emailValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.emailValid == 1 ? "Please enter your email." : "Invalid email entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPhone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control type="tel" defaultValue={localStorage.getItem('phone')} onChange={this.phoneChange} className={`form-control ${this.state.phoneValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.phoneValid == 1 ? "Please enter your phone number" : "Invalid phone number entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridBankAccount">
                                <Form.Label>Bank account number</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('bank_account')} onChange={this.bankaccountChange} className={`form-control ${this.state.bankaccountValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.bankaccountValid == 1 ? "Please enter your bank account number" : "Invalid bank account number entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridBankName">
                                <Form.Label>Bank name</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('bank_name')} onChange={this.banknameChange} className={`form-control ${this.state.banknameValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.banknameValid == 1 ? "Please enter your bank name" : "Invalid bank name entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 2 }} controlId="formGridPayByCredit">
                                <Form.Label>Payment: </Form.Label>
                            </Form.Group>
                            <Form.Group as={Col} md={{ span: 3 }} controlId="formGridPayByCredit">
                                <input type="checkbox" checked={this.state.paymentByCredit==1? true:false} onChange={this.payByCreditChange} />
                                <Form.Label>Credit card</Form.Label>

                            </Form.Group>
                            <Form.Group as={Col} md={{ span: 3 }} controlId="formGridPayByBank">
                                <input type="checkbox" checked={this.state.paymentByBank==1? true:false} onChange={this.payByBankChange} />
                                <Form.Label>Bank Transferring</Form.Label>

                            </Form.Group>
                        </Form.Row>
                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} controlId="formGridPosition">
                                <Form.Label >Pin your position</Form.Label>
                                <LocationPicker
                                    containerElement={<div style={{ width: '100%', height: '100%' }} />}
                                    mapElement={<div style={{ height: '400px' }} />}
                                    defaultPosition={defaultPosition}
                                    onChange={this.handleLocationChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <br />
                        <Form.Row style={format2}>
                            <Form.Group controlId="formGridProfilePic">
                                <Form.Label>Profile Picture</Form.Label>
                                <Form.Control type="file" onChange={this.profilePictureChange} className={`form-control ${this.state.profilePictureValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.profilePictureValid == 1 ? "Please choose your profile picture." : this.state.profilePictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                {this.state.profilePictureValid == 0 ? <Image id="profilePicture" src={this.state.photo_url}></Image> : null}
                            </Form.Group>
                        </Form.Row>
                        {/*<Form.Row style={format2}>
                                <Form.Group controlId="formGridFDACerPic">
                                    <Form.Label>FDA Certification Picture</Form.Label>
                                    <Form.Control type="file" onChange={this.FDACerPictureChange} className={`form-control ${this.state.fdacerPictureValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.fdacerPictureValid == 1 ? "Please choose your FDA certificate picture." : this.state.fdacerPictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                    {this.state.fdacerPictureValid == 0 ? <Image id="natIDPicture" src={this.state.FDACerPictureURL}></Image> : null}
                                </Form.Group>
                            </Form.Row>  */}


                        <Form.Row>
                            <Col md={{ offset: 7, span: 1 }}>
                                <Button variant="secondary" onClick={this.cancelHandle}>cancel</Button>
                            </Col>
                            <Col md={{ offset: 0, span: 2 }} >
                                <Button style={{ marginLeft: "20px" }} variant="primary" onClick={this.saveHandle}>Save Change</Button>
                            </Col>
                        </Form.Row>
                    </Form>
                    <br />
                </div>
                <br />
            </div>
        );
    }
}





const forceCenter = {
    width: "100vh",
    paddingLeft: "2%",
    paddingRight: "2%"
}

const headerStyle = {

    //paddingLeft: "40px",
    //paddingTop: "20px",
    //paddingLeft : "10%",
    textAlign: "center",
    //fontSize: "35px",
    //fontWeight: "bold"
};
const headerStyle2 = {

    //paddingLeft: "40px",
    //paddingTop: "20px",
    textAlign: "right",
    //fontSize: "35px",
    //fontWeight: "bold"
};
const headerStyle3 = {

    paddingLeft: "10px",
    paddingTop: "10px",
    textAlign: "left",
    //fontSize: "35px",
    //fontWeight: "bold"
};
const paragraphStyle = {
    //paddingLeft: "40px",
    //paddingRight: "40px",
    paddingTop: "5%",
    //textAlign: "left",
    //fontSize: "25px"
};

const current = {
    color: "blue"
};

const format = {
    paddingLeft: "2%",
    paddingRight: "2%"
};
const format2 = {
    paddingLeft: "10%",

};
export default EditCon;