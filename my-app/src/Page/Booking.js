import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
export class Booking extends Component {
  constructor() {
    super();

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleRequestDateChange = this.onRequestDateChange.bind(this);
    this.confirmButtonClick = this.confirmButtonClick.bind(this);
    this.requestService = this.requestService.bind(this);

    this.state = {
      show: false,
      service: [],
      date: '',
      reqStatus: '',
      isHomeowner: 0
    };
  }

  BackHandle(){
    window.location.href='/service/detail/'+sessionStorage.getItem('serviceID');
  }

  async handleClose() {
    this.setState({ show: false });
    if(this.state.reqStatus === "success") window.location.href = "/CaseDetail";
  }

  async handleShow() {
    this.setState({ show: true });
  }


  onRequestDateChange = e => {
    this.setState({ date: e.target.value });
    //console.log(this.state);
  };

  async requestService() {
    const data = { sid: sessionStorage.getItem('serviceID'), request_date: this.state.date, user_type: this.state.isHomeowner, NatID: localStorage.getItem('NatID') }
    console.log(data);
    try {
      const response = await fetch("/booking/request", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      if (results[0].status === "success") {
        console.log("Booking Success!");
        this.setState({ reqStatus: "success" },() => this.handleShow());
        console.log(results[1]);
        sessionStorage.setItem('caseID',results[1].cid);
        
      }
      else console.log("request Failed!!");
    } catch (error) {
      console.log("Request Failed", error);
    }

  }

  async confirmButtonClick() {
    const data = { sid: sessionStorage.getItem('serviceID'), request_date: this.state.date }
    try {
      const response = await fetch("/booking/checkAvailable", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      if (results.status === "available") this.requestService();
      else {
        this.handleShow();
        console.log(results.status);
      }
    } catch (error) {
      console.log("Request Failed", error);
    }
  }

  componentDidMount() {
    if (localStorage.getItem('userType') === "homeowner") this.setState({ isHomeowner: 1 });
    else this.setState({ isHomeowner: 0 });
    console.log(sessionStorage.getItem('serviceID'));
    if (sessionStorage.getItem('serviceID') != null) {
      fetch("/service/get_detail/" +
        sessionStorage.getItem('serviceID'))
        .then((response) => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Internal error');
          }
        })
        .then(data => {
          if (data.length !== 0) {
            this.setState({ service: data[0] });
          }
        })
        .catch((error) => {
          this.setState({ error: true });
          console.log(error);
        });
    }
  }
  render() {

    let popup
    if (this.state.reqStatus === "success") popup =
      <div>
        <Modal.Header closeButton>
          <Modal.Title>Success</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>Reservation Complete</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleClose}>
            Close
                 </Button>
        </Modal.Footer>
      </div>
    else popup =
      <div>
        <Modal.Header closeButton>
          <Modal.Title>Full</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>Please select another date or reserve with another contractor.</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.handleClose}>
            Close
                  </Button>
        </Modal.Footer>
      </div>
   
   return (
      <div class="Page">
      <Modal show={this.state.show} onHide={this.handleClose}>
        {popup}
      </Modal>
        <h1 style={headerStyle3}>Booking</h1>
        <Row style={headerStyle}>
          <Col>
            {(this.state.service.photo_url === null) ? <div>No Photo Available</div> : <p><Image class="Infoimage" width={250} height={250} src={this.state.service.photo_url} thumbnail fluid /></p>}
          </Col>
        </Row>

        <Row>
          <Col>
            <Form>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>Service type</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.type_name} />
                </Col>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Description</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.descr} />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Contractor Name</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.comp_name} />
                </Col>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Contractor ID</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.contractor_id} />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Warranty</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.warranty} />
                </Col>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Price</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.price} />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>email</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.email} />
                </Col>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Phone</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.phone} />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>rating</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control plaintext readOnly defaultValue={this.state.service.rating} />
                </Col>
              </Form.Group>

              <Form.Group as={Row}>

                <Form.Label column sm={{ span: 2, offset: 1 }}>Service Date</Form.Label>
                <Col sm="3" style={headerStyle}>
                  <Form.Control type="date" onChange={this.onRequestDateChange} />
                </Col>
              </Form.Group>

            </Form>
          </Col>
        </Row>
        <br /><br /><br />

        <Row>
          <Col sm={{ span: 2, offset: 1 }} style={headerStyle3}>
            <Button onClick={this.BackHandle}>Back</Button>
          </Col>
          <Col sm={{ span: 2, offset: 6 }} style={headerStyle2}>
            <Button onClick={this.confirmButtonClick}>Confirm</Button>
          </Col>
        </Row>

      </div>
   );
  }

}



const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default Booking;