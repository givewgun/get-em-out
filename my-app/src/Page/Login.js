import React, { Component } from "react";
import { Carousel, Jumbotron, Button, Form, FormControl, Row, Col } from 'react-bootstrap';

//import firstImg from "../img/girl_training.jpg";
//import secondImg from "../img/training.jpg";

export class Login extends Component {
  constructor() {
    super();
    this.loginHomeowner = this.loginHomeowner.bind(this);
    this.loginContractor = this.loginContractor.bind(this);
    this.logoutMember = this.logoutMember.bind(this);
    this.setStorage = this.setStorage.bind(this);
    this.validateUsername = this.validateUsername.bind(this);       //return 0 if OK
    this.validatePassword = this.validatePassword.bind(this);       //else return 1
    this.validateAllField = this.validateAllField.bind(this);

    // mockup courses from searchResults
    this.state = {
      isNotLogin: 1,
      user_type: "",
      username: "",
      password: "",
      errorMessage: "",

      usernameValid: 0,   //0 mean Valid. Other numbers are specific error codes
      passwordValid: 0,   //1 is empty. 2 is invalid. 3 is duplicated.
    };
  }

  onUsernameBarChange = e => {
    this.setState({ username: e.target.value.trim() }, () => this.validateUsername());
    //console.log(this.state);
  };

  onPasswordBarChange = e => {
    this.setState({ password: e.target.value }, () => this.validatePassword());
    //console.log(this.state);
  };

  async setStorage(data, type) {
    if (type === 0) {
      //localStorage.setItem('data', data);
      localStorage.setItem('userType', 'contractor');
      localStorage.setItem('fda_cer', data.fda_cer);
      localStorage.setItem('tax_id', data.tax_id);
      localStorage.setItem('comp_name', data.comp_name);
      localStorage.setItem('username', data.username);
      localStorage.setItem('addr', data.addr);
      localStorage.setItem('latitude', data.latitude);
      localStorage.setItem('longitude', data.longitude);
      localStorage.setItem('photo_url', data.photo_url);
      localStorage.setItem('paymentByCredit', data.paymentByCredit);
      localStorage.setItem('paymentByBank', data.paymentByBank);
      localStorage.setItem('phone', data.phone);
      localStorage.setItem('email', data.email);
      localStorage.setItem('rating', data.rating);
      localStorage.setItem('verify_status', data.verify_status);
      localStorage.setItem('email_verify', data.email_verify);
      localStorage.setItem('bank_account',data.bank_account);
      localStorage.setItem('bank_name',data.bank_name);
      //console.log(localStorage.getItem('bank_account'));
    }
    else if (type === 1) {
      localStorage.setItem('userType', 'homeowner');
      localStorage.setItem('NatID', data.NatID);
      localStorage.setItem('firstname', data.firstname);
      localStorage.setItem('lastname', data.lastname);
      localStorage.setItem('username', data.username);
      localStorage.setItem('addr', data.addr);
      localStorage.setItem('latitude', data.latitude);
      localStorage.setItem('longitude', data.longitude);
      localStorage.setItem('photo_url', data.photo_url);
      localStorage.setItem('phone', data.phone);
      localStorage.setItem('email', data.email);
      localStorage.setItem('email_verify', data.email_verify);
    }
  }

  async loginHomeowner() {
    if (this.validateAllField() == 0) {
      try {
        //console.log(this.state);
        this.setState({ user_type: "1" })
        const data = this.state;
        data.user_type = "1";
        //console.log(JSON.stringify(data));
        const response = await fetch("/user/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
        const results = await response.json();
        console.log(results, results.status);
        if (results.status === "wrong username or password") {
          this.setState({ errorMessage: "Invalid Password" });
        }
        else if (results.status === "no account") {
          this.setState({ errorMessage: "Account not exist!" });
        }
        else if (results.status === "no user") {
          this.setState({ errorMessage: "No user" });
        }
        else {
          this.setState({ sessionData: results[1], isNotLogin: 0 });
          this.setStorage(results[1], 1);
          this.props.setNavbarLoggedinUserType('homeowner');
        }
        console.log(localStorage.getItem('userType'));
      } catch (error) {
        console.log("Login Failed", error);
      }
    } else {
      alert("Error. Please check your username and password");
    }
  }

  async loginContractor() {
    if (this.validateAllField() == 0) {
      try {
        //console.log(this.state);
        this.setState({ user_type: "0" })
        const data = this.state;
        data.user_type = "0";
        //console.log(JSON.stringify(data));
        const response = await fetch("/user/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
        const results = await response.json();
        //console.log(results, results.status);
        if (results.status === "wrong username or password") {
          this.setState({ errorMessage: "Invalid Password" });
        }
        else if (results.status === "no account") {
          this.setState({ errorMessage: "Account not exist!" });
        }
        else if (results.status === "no user") {
          this.setState({ errorMessage: "No user" });
        }
        else {
          this.setState({ sessionData: results[1], isNotLogin: 0 });
          this.setStorage(results[1], 0);
          this.props.setNavbarLoggedinUserType('contractor');
        }
      } catch (error) {
        console.log("Login Failed", error);
      }
    } else {
      alert("Error. Please check your username and password");
    }
  }

  async logoutMember() {
    localStorage.clear();
    sessionStorage.clear();
    this.setState({ isNotLogin: 1 });
    this.props.setNavbarLoggedinUserType('');
  }

  validateUsername() {
    let input = this.state.username;
    let regex = /^\S{1,15}$/
    if (regex.test(input)) {
      this.setState({
        usernameValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          usernameValid: 1
        });
      } else {
        this.setState({
          usernameValid: 2
        });
      }
      return 1;
    }
  }

  validatePassword() {
    let input = this.state.password;
    let regex = /^.{8,20}$/
    if (regex.test(input)) {
      this.setState({
        passwordValid: 0
      });
      return 0;
    } else {
      if (input.length == 0) {
        this.setState({
          passwordValid: 1
        });
      } else {
        this.setState({
          passwordValid: 2
        });
      }
      return 1;
    }
  }

  validateAllField() {
    return (this.validateUsername() + this.validatePassword()) == 0 ? 0 : 1;
  }

  render() {

    let checkLogin

    if (this.state.isNotLogin === 1 & (localStorage.getItem('userType') === null | localStorage.getItem('userType') === "")) checkLogin =
      <div id="loginbox" style={headerStyle}>
        <h1 style={headerStyle}>Welcome, Stranger</h1>
        <br /><br /><br /><br />
        <div>{this.state.errorMessage}</div>
        <Form>
          <Row className="justify-content-md-center" >
            <Col md={{ span: 4 }}>
              <FormControl id="usernameLogin" type="text" placeholder="Username" onChange={this.onUsernameBarChange} className={`form-control ${this.state.usernameValid == 0 ? '' : 'is-invalid'}`} />
              <Form.Control.Feedback type="invalid">{this.state.usernameValid == 1 ? "Please enter your username." : "Username must be at most 15 characters with no space in between."}</Form.Control.Feedback>
            </Col>
          </Row>
          <br />
          <Row className="justify-content-md-center" >
            <Col md={{ span: 4 }}>
              <FormControl id="passwordLogin" type="password" placeholder="Password" onChange={this.onPasswordBarChange} className={`form-control ${this.state.passwordValid == 0 ? '' : 'is-invalid'}`} />
              <Form.Control.Feedback type="invalid">{this.state.passwordValid == 1 ? "Please enter your password." : "Password must be between 8 to 20 characters."}</Form.Control.Feedback>
            </Col>
          </Row>
          <br />
          <Row className="justify-content-md-center" >
            <Col md="auto">
              <Button onClick={this.loginHomeowner}>Login as Homeowner</Button>
            </Col>
            <Col md="auto">
              <Button onClick={this.loginContractor}>Login as Contractor</Button>
            </Col>
          </Row>
        </Form>



      </div>

    else checkLogin =
      <div style={headerStyle}>
        <h1>Welcome, {localStorage.getItem('username')}</h1>
        <Button onClick={this.logoutMember}>Logout</Button>
      </div>

    return (
      <div id="loginPage">
        {checkLogin}
      </div>

    );
  }
}

const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  paddingLeft: "40px",
  paddingRight: "40px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "25px"
};


export default Login;
