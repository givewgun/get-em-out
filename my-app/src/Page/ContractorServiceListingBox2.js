import React, { Component } from "react";
import { Button, ButtonGroup, Card } from "react-bootstrap";
//each variable in here is to be checked with the vars from back-end
import EditServiceForm from "../component/EditServiceForm";
export class ContractorServiceListingBox extends Component {
  constructor() {
    super();
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.state = {
      show: false,
    }

    this.EditServiceHandle = this.EditServiceHandle.bind(this);
  }
  moreInfo() {
    window.location.href = "/service/detail/" + this.props.id;
  }

  contractorInfo() {
    window.location.href = "/contractor/detail/" + this.props.fda_cer;
  }

  EditServiceHandle() {
    this.setState({ show: true });
  }

  async handleClose() {
    this.setState({ show: false });
  }

  async handleShow(request) {
    this.setState({
      show: true,
    });
  }

  render() {
    return (
      <div>
        <EditServiceForm show={this.state.show} handleClose={this.handleClose} reloadData={() => this.props.reloadData()}
          id={this.props.id}
          type={this.props.typeid}
          price={this.props.price}
          descr={this.props.description}
          startTime={this.props.startTime}
          endTime={this.props.endTime}
          warranty={this.props.warranty}
          dailyLimit={this.props.dailyLimit}
          photo_url={this.props.photo_url}
        />
        <div id="item">
          <Card style={{ width: '15rem' }}>
            <Card.Img variant="top" src={this.props.photo_url ? this.props.photo_url + '?' + Date.now() : "https://cdn1.iconfinder.com/data/icons/pest-control-glyph-silhouettes/300/65717902Untitled-3-512.png"} />
            <Card.Body>
              <Card.Title >{this.props.type}</Card.Title>
              <Card.Text>
                <p> Rating : {this.props.rating ? this.props.rating : "No rating yet"} </p>
                <p> Price : THB {this.props.price} </p>
                {this.props.phone == null ? null : <p> Phone : {this.props.phone}</p>}
                {this.props.email == null ? null : <p> Email : {this.props.email}</p>}
              </Card.Text>

              {localStorage.getItem('userType') == 'contractor' ?
                (
                  <ButtonGroup>
                    <Button variant="info" size="sm" title="button_to_service_info" onClick={(e) => this.moreInfo(e)}>Info</Button>
                    <Button size="sm" title="button_to_service_info" onClick={() => this.handleShow()}>Edit</Button>
                  </ButtonGroup>

                )
                :
                (
                  null
                )

              }
            </Card.Body>
          </Card>
        </div>
      </div>
    );
  }
}



export default ContractorServiceListingBox;