import React, { Component } from "react";
import {Image,Carousel, Jumbotron, Button, Form, FormControl, Row, Col,Breadcrumb } from 'react-bootstrap';
export class ThankAndEmail extends Component {
    constructor() {
        super();
    }

    render() {

        return (
            <div class="Page">
            <Breadcrumb>
              <Breadcrumb.Item>select user type</Breadcrumb.Item>
              <Breadcrumb.Item>fill the information</Breadcrumb.Item>
              <Breadcrumb.Item>Upload your profile</Breadcrumb.Item>
              <Breadcrumb.Item active>Confirm</Breadcrumb.Item>
            </Breadcrumb>
            <div class="PageBody3">
              <Jumbotron  style={headerStyle} >
                <h1> Finish!! </h1>
                <p style={paragraphStyle}>
                  Your registration is now done. We will send a verify email to you shortly to confirm your email.
                </p>
                <p>
                  Thank you
                </p>
                <Row style={paragraphStyle} >
                  <Col>
                    <Button href="/Login" variant="primary">Go to Login</Button>
                  </Col>
                  <Col>
                    <Button href="/" variant="primary">Go to Homepage</Button>
                  </Col>
                </Row>
              </Jumbotron>
              </div>
            </div>
        );

    }

}


const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default ThankAndEmail;