import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
import LocationPicker from 'react-location-picker';
const axios = require("axios");
const defaultPosition = {
    lat: parseFloat(localStorage.getItem('latitude')),
    lng: parseFloat(localStorage.getItem('longitude')),
};

export class EditHome extends Component {
    constructor() {
        super();
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onPictureSubmit = this.onPictureSubmit.bind(this);
        this.profilePictureChange = this.profilePictureChange.bind(this);
        this.usernameChange = this.usernameChange.bind(this);
        this.firstnameChange = this.firstnameChange.bind(this);
        this.lastnameChange = this.lastnameChange.bind(this);
        this.addrChange = this.addrChange.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.phoneChange = this.phoneChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.saveHandle = this.saveHandle.bind(this);

        this.validateUsername = this.validateUsername.bind(this);       //return 0 if OK
        this.validateFirstname = this.validateFirstname.bind(this);    //else return 1
        this.validateLastname = this.validateLastname.bind(this);
        this.validateAddr = this.validateAddr.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.validatePhone = this.validatePhone.bind(this);
        this.validateAllField = this.validateAllField.bind(this);

        this.validateProfilePicture = this.validateProfilePicture.bind(this);

        this.state =
            {
                username: localStorage.getItem('username'),
                NatID: localStorage.getItem('NatID'),
                firstname: localStorage.getItem('firstname'),
                lastname: localStorage.getItem('lastname'),
                addr: localStorage.getItem('addr'),
                email: localStorage.getItem('email'),
                phone: localStorage.getItem('phone'),
                latitude: localStorage.getItem('latitude'),
                longitude: localStorage.getItem('longitude'),
                photo_url: localStorage.getItem('photo_url'), //latest uploaded & validated photo URL or initial photo URL
                temp_valid_photo: null,  //latest uploaded & validated photo
                temp_photo: null,
                isUpload: 0,

                usernameValid: 0,   //0 mean Valid. Other numbers are specific error codes
                passwordValid: 0,   //1 is empty. 2 is invalid. 3 is duplicated.
                firstnameValid: 0,
                lastnameValid: 0,
                addrValid: 0,
                emailValid: 0,
                phoneValid: 0,

                isUpload: 0,
                user_type: 1, //1 = homeowner
                profilePictureValid: 0, //0 OK, 1 null, 2 invalid type, 3 file size too large
            };
    }

    cancelHandle() {
        window.location.href = "Profile";
    }

    saveHandle() {
        this.onFormSubmit();
        if (this.state.isUpload === 1) this.onPictureSubmit();
        alert("Profile Successfully Edited");
        window.location.href = "Profile";
    }

    async onFormSubmit() {
        console.log(this.state);
        if (this.validateAllField() == 0) {
            try {
                const data = this.state;
                const response = await fetch("/user/edit_profile", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(data)
                });
                localStorage.setItem('username', this.state.username);
                localStorage.setItem('firstname', this.state.firstname);
                localStorage.setItem('lastname', this.state.lastname);
                localStorage.setItem('addr', this.state.addr);
                localStorage.setItem('email', this.state.email);
                localStorage.setItem('phone', this.state.phone);
                localStorage.setItem('latitude', this.state.latitude);
                localStorage.setItem('longitude', this.state.longitude);
                console.log("Upload " + this.state.photo_url);
                //localStorage.setItem('photo_url', this.state.photo_url);
            } catch (error) {
                console.log("Form Submission Failed", error);
            }
            
        } else {
            alert("Error. Please check your form information");
        }
    }

    async onPictureSubmit() {
        try {
            const formData = new FormData();
            formData.set('NatID', this.state.NatID);
            formData.append('profile_image', this.state.temp_valid_photo);
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                },
            };
            axios.post("/user/upload/profile/1", formData, config)
                .then((response) => {
                    console.log(this.state.photo_url);
                }).catch((error) => {
                    alert("Failed to submit picture");
                });
        } catch (error) {
            console.log("ProfilePic Submission Failed", error);
        }
    }

    async profilePictureChange(event) {
        console.log(event.target.files[0]);
        console.log(URL.createObjectURL(event.target.files[0]));
        if (typeof event.target.files[0] !== 'undefined') {
            this.setState({
                temp_photo: event.target.files[0]
            }, () => this.validateProfilePicture());
        } else {
            this.setState({
                temp_photo: null
            }, () => this.validateProfilePicture());
        }
    }

    async usernameChange(event) {
        this.setState({
            username: event.target.value.trim()
        }, () => this.validateUsername());
    }

    async firstnameChange(event) {
        this.setState({
            firstname: event.target.value.trim()
        }, () => this.validateFirstname());
    }
    async lastnameChange(event) {
        this.setState({
            lastname: event.target.value.trim()
        }, () => this.validateLastname());
    }
    async addrChange(event) {
        this.setState({
            addr: event.target.value.trim()
        }, () => this.validateAddr());
    }
    async emailChange(event) {
        this.setState({
            email: event.target.value.trim()
        }, () => this.validateEmail());
    }
    async phoneChange(event) {
        this.setState({
            phone: event.target.value.trim()
        }, () => this.validatePhone());
    }

    handleLocationChange({ position, address }) {
        this.setState({ latitude: position.lat, longitude: position.lng });
    }

    validateUsername() {
        let input = this.state.username;
        let regex = /^\S{1,15}$/
        if (regex.test(input)) {
            this.setState({
                usernameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    usernameValid: 1
                });
            } else {
                this.setState({
                    usernameValid: 2
                });
            }
            return 1;
        }
    }

    validateFirstname() {
        let input = this.state.firstname;
        let regex = /^[^0-9]{1,50}$/
        if (regex.test(input)) {
            this.setState({
                firstnameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    firstnameValid: 1
                });
            } else {
                this.setState({
                    firstnameValid: 2
                });
            }
            return 1;
        }
    }

    validateLastname() {
        let input = this.state.lastname;
        let regex = /^[^0-9]{1,50}$/
        if (regex.test(input)) {
            this.setState({
                lastnameValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    lastnameValid: 1
                });
            } else {
                this.setState({
                    lastnameValid: 2
                });
            }
            return 1;
        }
    }

    validateAddr() {
        let input = this.state.addr;
        let regex = /^.{1,150}$/
        if (regex.test(input)) {
            this.setState({
                addrValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    addrValid: 1
                });
            } else {
                this.setState({
                    addrValid: 2
                });
            }
            return 1;
        }
    }

    validateEmail() {
        let input = this.state.email;
        let regex = /\S+@\S+\.\S+/
        if (regex.test(input)) {
            this.setState({
                emailValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    emailValid: 1
                });
            } else {
                this.setState({
                    emailValid: 2
                });
            }
            return 1;
        }
    }

    validatePhone() {
        let input = this.state.phone;
        let regex = /^[0-9]{9,10}$/     //regex for allowing only number charactor of length 9 to 10
        if (regex.test(input)) {
            this.setState({
                phoneValid: 0
            });
            return 0;
        } else {
            if (input.length == 0) {
                this.setState({
                    phoneValid: 1
                });
            } else {
                this.setState({
                    phoneValid: 2
                });
            }
            return 1;
        }
    }

    validateAllField() {
        return (this.validateUsername() + this.validateFirstname() + this.validateLastname() + this.validateAddr() + this.validateEmail() + this.validatePhone()) == 0 ? 0 : 1;
    }

    validateProfilePicture() {
        let input = this.state.temp_photo;
        let regex = /^.*\.(jpg|jpeg|png)$/;    //check file extension
        console.log(input);

        if (input === null) {
            this.setState({
                profilePictureValid: 1
            });
            return 1;
        } else {
            if (regex.test(input.name.toLowerCase())) {
                if (input.size / 1024 / 1024 <= 1) { //file size <= 1 mb
                    this.setState({
                        profilePictureValid: 0,
                        isUpload: 1,
                        temp_valid_photo: input,
                        photo_url: URL.createObjectURL(input)
                    });
                    return 0
                } else {
                    this.setState({
                        profilePictureValid: 3
                    });
                    return 1;
                }
            } else {
                this.setState({
                    profilePictureValid: 2
                });
                return 1;
            }
        }
    }

    render() {

        return (
            <div>
                <div>
                    <h1 style={format}>Edit Your Profile</h1>
                    <Form style={format}>
                        {/*<Form.Row style={format2}>
                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridUsername">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control type="text" placeholder="Enter username" onChange={this.usernameChange} className={`form-control ${this.state.usernameValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.usernameValid == 1 ? "Please enter your username." : this.state.usernameValid == 2 ? "Username must be at most 15 characters with no space in between." : "This username has already been taken."}</Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" onChange={this.passwordChange} className={`form-control ${this.state.passwordValid == 0 ? '' : 'is-invalid'}`} />
                                    <Form.Control.Feedback type="invalid">{this.state.passwordValid == 1 ? "Please enter your password." : "Password must be between 8 to 20 characters."}</Form.Control.Feedback>
                                </Form.Group>
                            </Form.Row>*/}

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridFirstName">
                                <Form.Label>Firstname</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('firstname')} onChange={this.firstnameChange} className={`form-control ${this.state.firstnameValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.firstnameValid == 1 ? "What’s your name?" : "Invalid name entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridLastName">
                                <Form.Label>Lastname</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('lastname')} onChange={this.lastnameChange} className={`form-control ${this.state.lastnameValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.lastnameValid == 1 ? "What’s your last name?" : "Invalid last name entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} controlId="formGridNatID">
                                <Form.Label>National ID</Form.Label>
                                <Form.Control disabled defaultValue={localStorage.getItem('NatID')} />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} controlId="formGridAddress">
                                <Form.Label>Address</Form.Label>
                                <Form.Control type="text" defaultValue={localStorage.getItem('addr')} onChange={this.addrChange} className={`form-control ${this.state.addrValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.addrValid == 1 ? "Please fill your address." : "Entered address is too long."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" defaultValue={localStorage.getItem('email')} onChange={this.emailChange} className={`form-control ${this.state.emailValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.emailValid == 1 ? "Please enter your email." : "Invalid email entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} md={{ span: 5 }} controlId="formGridPhone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control type="tel" defaultValue={localStorage.getItem('phone')} onChange={this.phoneChange} className={`form-control ${this.state.phoneValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.phoneValid == 1 ? "Please enter your phone number" : "Invalid phone number entered. Please check."}</Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group as={Col} md={{ span: 10 }} controlId="formGridPosition">
                                <Form.Label>Pin your position</Form.Label>
                                <LocationPicker
                                    containerElement={<div style={{ width: '100%', height: '100%' }} />}
                                    mapElement={<div style={{ height: '400px' }} />}
                                    defaultPosition={defaultPosition}
                                    onChange={this.handleLocationChange}
                                />
                            </Form.Group>
                        </Form.Row>

                        <Form.Row style={format2}>
                            <Form.Group controlId="formGridProfilePic">
                                <Form.Label>Profile Picture</Form.Label>
                                <Form.Control type="file" onChange={this.profilePictureChange} className={`form-control ${this.state.profilePictureValid == 0 ? '' : 'is-invalid'}`} />
                                <Form.Control.Feedback type="invalid">{this.state.profilePictureValid == 1 ? "Please choose your profile picture." : this.state.profilePictureValid == 2 ? "Please upload a valid image file of type .jpg .jpeg or .png" : "The image file size must be at most 1 MB."}</Form.Control.Feedback>
                                {this.state.profilePictureValid == 0 ? <Image id="profilePicture" src={this.state.photo_url}></Image> : null}
                            </Form.Group>
                        </Form.Row>

                        <Form.Row>
                            <Col md={{ offset: 7, span: 1 }}>
                                <Button variant="secondary" onClick={this.cancelHandle}>cancel</Button>
                            </Col>
                            <Col md={{ offset: 0, span: 2 }} >
                                <Button style={{ marginLeft: "20px" }} variant="primary" onClick={this.saveHandle}>Save Change</Button>
                            </Col>
                        </Form.Row>
                        <br />

                    </Form>
                </div>



            </div>
        );
    }
}





const forceCenter = {
    width: "100vh",
    paddingLeft: "2%",
    paddingRight: "2%"
}

const headerStyle = {

    //paddingLeft: "40px",
    //paddingTop: "20px",
    //paddingLeft : "10%",
    textAlign: "center",
    //fontSize: "35px",
    //fontWeight: "bold"
};
const headerStyle2 = {

    //paddingLeft: "40px",
    //paddingTop: "20px",
    textAlign: "right",
    //fontSize: "35px",
    //fontWeight: "bold"
};
const headerStyle3 = {

    paddingLeft: "10px",
    paddingTop: "10px",
    textAlign: "left",
    //fontSize: "35px",
    //fontWeight: "bold"
};
const paragraphStyle = {
    //paddingLeft: "40px",
    //paddingRight: "40px",
    paddingTop: "5%",
    //textAlign: "left",
    //fontSize: "25px"
};

const current = {
    color: "blue"
};

const format = {
    paddingLeft: "2%",
    paddingRight: "2%"
};
const format2 = {
    paddingLeft: "10%",

};
export default EditHome;