import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
export class Receipt extends Component {
  constructor() {
    super();
    this.GetDepositReceipt = this.GetDepositReceipt.bind(this);
    this.GetRemainingReceipt = this.GetRemainingReceipt.bind(this);
    this.getCost = this.getCost.bind(this);

    this.state={
      caseid: sessionStorage.getItem('caseID'),
      receipt: [],
      showDepositReceipt : false,
      showRemainingReceipt: false,
      cost: 0,
      ptype: ''

    };

  }

  async GetDepositReceipt(){
    try {

      const data = {};
      data.caseid = this.state.caseid;
      data.ptype = 'deposit';
      console.log(data);
      const response = await fetch("/payment/receipt", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results[0].status == "success") {
        this.setState({ptype:'deposit'})
        this.getCost();
        this.setState({showDepositReceipt : true,showRemainingReceipt: false,receipt:results[1]})
      } else {
        this.setState({showDepositReceipt : false,showRemainingReceipt: false,receipt:[]})
        alert("Error. Receipt failed");
      }
    } catch (error) {
      this.setState({showDepositReceipt : false,showRemainingReceipt: false,receipt:[]})
      console.log("Receipt Failed", error);
    }

  };

  async getCost() {
    console.log(this.state.ptype);
    try {
      const data = {};
      data.caseid = this.state.caseid;
      let response;
      if (this.state.ptype === 'deposit') {
        response = await fetch("/payment/getDepositCost", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
      } else if (this.state.ptype === 'remaining') {
        response = await fetch("/payment/getRemainingCost", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        });
      }

      const results = await response.json();
      console.log(results);
      this.setState({ cost: results.cost });
    } catch (error) {
      console.log("Get cost Failed", error);
    }
  }

  async GetRemainingReceipt(){
    try {

      const data = {};
      data.caseid = this.state.caseid;
      data.ptype = 'remaining';
      console.log(data);
      const response = await fetch("/payment/receipt", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results[0].status == "success") {
        this.setState({ptype:'remaining'})
        this.getCost();
        this.setState({showDepositReceipt : false,showRemainingReceipt: true,receipt:results[1]})
      } else {
        this.setState({showDepositReceipt : false,showRemainingReceipt: false,receipt:[]})
        alert("Error. Receipt failed");
      }
    } catch (error) {
      this.setState({showDepositReceipt : false,showRemainingReceipt: false,receipt:[]})
      console.log("Receipt Failed", error);
    }

  };
  
  render() {
    console.log(this.state.receipt)
   return (
      <div class="Page">
        <h1 style={headerStyle3}>Receipt <small> Request ID : {this.state.caseid}</small></h1> {/*caseID*/}
        <br />
        <Row>
          <Col style={headerStyle}>
            <Button onClick={this.GetDepositReceipt}> Deposit Receipt</Button>
          </Col>
          <Col style={headerStyle}>
            <Button onClick={this.GetRemainingReceipt}> Remaining Receipt</Button>
          </Col>
        </Row>
        <br />
        <br />
        {(this.state.showDepositReceipt===true || this.state.showRemainingReceipt===true) ? 
          (
            <Form>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>Homeowner :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control disabled value={this.state.receipt.firstname+' '+this.state.receipt.lastname} />{/*first name + last name*/}
                </Col>
              </Form.Group>
              <Form.Group as={Row} style={headerStyle}>
                <Col>
                  <h3>to</h3>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>Contractor :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control disabled value={this.state.receipt.comp_name} />{/*comp_name*/}
                </Col>
              </Form.Group>  
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>Tax ID :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control disabled value={this.state.receipt.tax_id} />{/*tax_id*/}
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>Address :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control as='textarea' rows='3' disabled value={this.state.receipt.addr} />
                </Col>
              </Form.Group>
              <Form.Group as={Row} style={headerStyle}>
                <Col>
                  <h3>payment detail</h3>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>payment date & time :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control disabled value={this.state.receipt.paydate} />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>payment type :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control disabled value={this.state.receipt.ptype} />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm={{ span: 2, offset: 1 }}>Amount :</Form.Label>
                <Col sm="6" style={headerStyle}>
                  <Form.Control disabled value={this.state.cost} />
                </Col>
              </Form.Group>
            </Form>
            )
          :
          ( 
            <Row>
              <Col>
                <h3 style={headerStyle}>
                  Sorry, No receipt here.
                </h3>
              </Col>
            </Row>
            )}
        

      


      </div>
   );
  }

}



const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default Receipt;