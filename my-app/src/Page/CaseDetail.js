import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListGroup, Image, Button, Form, FormControl, Row, Col, Table, thead, tbody, Modal } from 'react-bootstrap';
export class CaseDetail extends Component {
  constructor() {
    super();

    this.handleShow = this.handleShow.bind(this);
    this.handleShowAcceptReject = this.handleShowAcceptReject.bind(this);
    this.handleShowCompleteService = this.handleShowCompleteService.bind(this);
    this.handleShowCancelRequest = this.handleShowCancelRequest.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleReceipt = this.handleReceipt.bind(this);
    this.onAcceptButton = this.onAcceptButton.bind(this);
    this.onRejectButton = this.onRejectButton.bind(this);
    this.onYesCompleteServiceButton = this.onYesCompleteServiceButton.bind(this);
    this.onYesCancelRequestButton = this.onYesCancelRequestButton.bind(this);
    this.fetchCaseDetail = this.fetchCaseDetail.bind(this);
    this.onFeedbackSaveChangesButton = this.onFeedbackSaveChangesButton.bind(this);

    this.state = {
      show: false,
      showAcceptReject: false,
      showCompleteService: false,
      showCancelRequest: false,
      case: [],
      saveChangeButtonDisable: true,
      feedback_comment: "",
      feedback_rating: ""
    };
  }

  async handleClose() {
    this.setState({ show: false, showAcceptReject: false, showCompleteService: false, showCancelRequest: false });
  }
  async handleReceipt() {
    window.location.href = '/Receipt'
  }

  async handleShow() {
    this.setState({
      show: true,
      feedback_rating: this.state.case.feedback_rating === null ? "" : this.state.case.feedback_rating,
      feedback_comment: this.state.case.feedback_comment === null ? "" : this.state.case.feedback_comment,
      saveChangeButtonDisable: true
    });

  }

  async handleShowAcceptReject() {
    this.setState({ showAcceptReject: true });

  }

  async handleShowCompleteService() {
    this.setState({ showCompleteService: true });

  }

  async handleShowCancelRequest() {
    this.setState({ showCancelRequest: true });

  }

  async onAcceptButton() {
    try {

      const data = {};
      data.caseid = this.state.case.caseid;
      data.user_type = localStorage.getItem('userType') === 'contractor' ? 0 : 1;
      console.log(data);
      const response = await fetch("/booking/accept", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        await this.fetchCaseDetail();
        this.handleClose();
      } else {
        alert("Error. Accept failed");
      }
    } catch (error) {
      console.log("Accept Failed", error);
    }

  }

  async onRejectButton() {
    try {

      const data = {};
      data.caseid = this.state.case.caseid;
      data.user_type = localStorage.getItem('userType') === 'contractor' ? 0 : 1;
      console.log(data);
      const response = await fetch("/booking/reject", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        await this.fetchCaseDetail();
        this.handleClose();
      } else {
        alert("Error. Reject failed");
      }
    } catch (error) {
      console.log("Reject Failed", error);
    }

  }

  async onYesCompleteServiceButton() {
    try {

      const data = {};
      data.caseid = this.state.case.caseid;
      data.user_type = localStorage.getItem('userType') === 'contractor' ? 0 : 1;
      console.log(data);
      const response = await fetch("/booking/serviced", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        await this.fetchCaseDetail();
        this.handleClose();
      } else {
        alert("Error. Complete service failed");
      }
    } catch (error) {
      console.log("Complete service Failed", error);
    }

  }

  async onYesCancelRequestButton() {
    try {

      const data = {};
      data.caseid = this.state.case.caseid;
      data.user_type = localStorage.getItem('userType') === 'contractor' ? 0 : 1;
      console.log(data);
      const response = await fetch("/booking/cancel", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        await this.fetchCaseDetail();
        this.handleClose();
      } else {
        alert("Error. Cancel request failed");
      }
    } catch (error) {
      console.log("Cancel request Failed", error);
    }

  }

  async fetchCaseDetail() {
    if (sessionStorage.getItem('caseID') != null)
      try {
        let data = { caseid: "" };
        data.caseid = sessionStorage.getItem('caseID');
        console.log(JSON.stringify(data));
        fetch("/booking/getRequestDetail", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              throw new Error('Internal error');
            }
          })
          .then(data => {
            if (data.length !== 0) {
              this.setState({ case: data[0] });
            }
            console.log(data);
          });
        // console.log(this.state.case);
        //const results = response.json();
        //console.log(results);
        //this.setState({case: results});
        //console.log(this.state.case);
      } catch (error) {
        console.log("Search failed", error);
      }
  }

  //path to case detail = '/CaseDetail/'
  componentDidMount() {
    this.fetchCaseDetail()
  }


  onFeedbackCommentChange = e => {
    //console.log(e.target.value);
    if (e.target.value.trim().length > 255) {
      e.target.value = e.target.value.substring(0, 255);
    }
    this.setState({ feedback_comment: e.target.value.trim() });

    if (String(this.state.feedback_rating).length == 1) {
      this.setState({ saveChangeButtonDisable: false });
    }
  };

  onFeedbackRatingChange = e => {
    //console.log(e.target.value);
    let regex = /^[0-5]{1,2}$/
    if (regex.test(e.target.value)) {
      if (e.target.value.length >= 2) {
        e.target.value = e.target.value.substring(1);
      }
      this.setState({
        feedback_rating: e.target.value,
        saveChangeButtonDisable: false
      });
    } else {
      if (e.target.value.length < 2) {
        e.target.value = "";
      } else {
        e.target.value = e.target.value.substring(0, 1);
      }
    }
    if (e.target.value.length === 0) {
      this.setState({
        saveChangeButtonDisable: true
      });
    }
    //this.setState({ password: e.target.value });
    //console.log(this.state);
  };

  onPayButton = () => {
    sessionStorage.setItem('paymentCaseID', this.state.case.caseid);
    window.location.href = "/Payment";
  }

  async onFeedbackSaveChangesButton() {
    try {
      const data = {};
      data.caseid = this.state.case.caseid;
      data.rating = this.state.feedback_rating;
      data.comment = this.state.feedback_comment;
      console.log(data);
      const response = await fetch("/feedback/giveFeedback", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const results = await response.json();
      console.log(results);
      if (results.status == "success") {
        this.fetchCaseDetail();
        this.handleClose();
      }
    } catch (error) {
      console.log("Give feedback failed", error);
    }
  }

  render() {

    return (
      <div class="Page">
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Feedback of CaseID: {this.state.case.caseid}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group>
                <Form.Label>Comment</Form.Label>
                {localStorage.getItem('userType') === 'contractor' ?
                  <Form.Control as="textarea" rows="3" readOnly defaultValue={this.state.feedback_comment} />
                  :
                  <Form.Control as="textarea" rows="3" onChange={this.onFeedbackCommentChange} defaultValue={this.state.feedback_comment} />}

                <Form.Label>☆ Rating [ 0 - 5 ]</Form.Label>
                {localStorage.getItem('userType') === 'contractor' ?
                  <Form.Control type="text" readOnly defaultValue={this.state.feedback_rating} />
                  :
                  <Form.Control type="text" onChange={this.onFeedbackRatingChange} defaultValue={this.state.feedback_rating} />
                }
              </Form.Group>
            </Form>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
                  </Button>
            {localStorage.getItem('userType') === 'contractor' ? "" :
              this.state.saveChangeButtonDisable ?
                <Button variant="primary" disabled>Save Changes</Button> :
                <Button variant="primary" onClick={this.onFeedbackSaveChangesButton} >Save Changes</Button>
            }
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showAcceptReject} onHide={this.handleClose} size="lg" centered >
          <Modal.Header closeButton>
            <Modal.Title>Do you want to accept or reject this request?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Row>
                <Col style={headerStyle} >
                  <Button size="lg" variant="success" block onClick={this.onAcceptButton}> Accept</Button>
                </Col>
                <Col style={headerStyle} >
                  <Button size="lg" variant="danger" block onClick={this.onRejectButton}>Reject</Button>
                </Col>
              </Row>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
                  </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showCompleteService} onHide={this.handleClose} size="lg" centered >
          <Modal.Header closeButton>
            <Modal.Title>Have you completed the service?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Row>
                <Col style={headerStyle} >
                  <Button size="lg" variant="success" block onClick={this.onYesCompleteServiceButton}>Yes</Button>
                </Col>
                <Col style={headerStyle} >
                  <Button size="lg" variant="danger" block onClick={this.handleClose}>No</Button>
                </Col>
              </Row>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
                  </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showCancelRequest} onHide={this.handleClose} size="lg" centered >
          <Modal.Header closeButton>
            <Modal.Title>Are you sure to cancel this request?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Row>
                <Col style={headerStyle} >
                  <Button size="lg" variant="success" block onClick={this.onYesCancelRequestButton}>Yes</Button>
                </Col>
                <Col style={headerStyle} >
                  <Button size="lg" variant="danger" block onClick={this.handleClose}>No</Button>
                </Col>
              </Row>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
                  </Button>
          </Modal.Footer>
        </Modal>

        <h1 style={headerStyle}>Case ID : {this.state.case.caseid}</h1>
        <Row>
          <Col style={headerStyle}>
            <h3>Case Info</h3>
            <ListGroup variant="flush">
              <ListGroup.Item>Description : {this.state.case.descr}</ListGroup.Item>
              <ListGroup.Item>Service Type : {this.state.case.type_name}</ListGroup.Item>
              <ListGroup.Item>Price : {this.state.case.price}</ListGroup.Item>
              <ListGroup.Item>Status : {this.state.case.status_name}</ListGroup.Item>
              <ListGroup.Item>Request Date : {this.state.case.request_date}</ListGroup.Item>
              <ListGroup.Item>Service Date : {this.state.case.service_date}</ListGroup.Item>
            </ListGroup>
          </Col>
        </Row>

        <br />

        <Row>
          <Col style={headerStyle}>
            <h3 >Contractor Info</h3>
            <p><Image class="Infoimage" width={250} height={250} src={this.state.case.contractor_photo_url} thumbnail fluid /></p>
            <ListGroup variant="flush">
              <ListGroup.Item>{this.state.case.comp_name}</ListGroup.Item>
              <ListGroup.Item>FDA Certification Number : {this.state.case.fda_cer}</ListGroup.Item>
              <ListGroup.Item>Address : {this.state.case.contractor_addr}</ListGroup.Item>
              <ListGroup.Item>Email : {this.state.case.contractor_email}</ListGroup.Item>
              <ListGroup.Item>Phone : {this.state.case.contractor_phone}</ListGroup.Item>
              <ListGroup.Item>Bank Account Number :  {this.state.case.bank_account}</ListGroup.Item>
            </ListGroup>

          </Col>
          <Col style={headerStyle}>
            <h3>Homeowner Info</h3>
            <p><Image class="Infoimage" width={250} height={250} src={this.state.case.homeowner_photo_url} thumbnail fluid /></p>
            <ListGroup variant="flush">
              <ListGroup.Item>{this.state.case.firstname} {this.state.case.lastname}</ListGroup.Item>
              <ListGroup.Item>National ID Number: {this.state.case.NatID}</ListGroup.Item>
              <ListGroup.Item>Address: {this.state.case.homeowner_addr}</ListGroup.Item>
              <ListGroup.Item>Email: {this.state.case.homeowner_email}</ListGroup.Item>
              <ListGroup.Item>Phone: {this.state.case.homeowner_phone}</ListGroup.Item>
            </ListGroup>

          </Col>
        </Row>

        <br /><br />
        <Row>
          <Col style={headerStyle}>
            <Button variant="primary" href={"/RequestDashboard"}>
              back to dashboard
                </Button>
          </Col>

          <Col style={headerStyle}>
            <Button variant="primary" onClick={this.handleShow}>
              Feedback{this.state.case.feedback_rating === null ? "" : " : " + this.state.case.feedback_rating + "☆"}
            </Button>
          </Col>

          <Col style={headerStyle}>
            <Button variant="primary" onClick={this.handleReceipt}>
              Receipt
            </Button>
          </Col>

          <Col style={headerStyle}>
            {localStorage.getItem('userType') === 'contractor' ?
              this.state.case.status_name === 'request' ? <Button variant="primary" onClick={this.handleShowAcceptReject} >Accept/Reject</Button>
                :
                this.state.case.status_name === 'cancelled' ? <Button variant="primary" disabled >Rejected</Button>
                  :
                  this.state.case.status_name === 'deposit pending' ? <Button variant="primary" disabled >Accepted</Button>
                    :
                    this.state.case.status_name === 'servicing' ? <Button variant="primary" onClick={this.handleShowCompleteService}>Complete service</Button>
                      :
                      this.state.case.status_name === 'payment pending' ? <Button variant="primary" disabled >Accepted</Button>
                        :
                        this.state.case.status_name === 'complete' ? <Button variant="primary" disabled >Completed</Button>
                          : <Button variant="primary" disabled >Accepted</Button>
              :
              (this.state.case.status_name === "deposit pending" || this.state.case.status_name === "payment pending") ?
                <Button variant="primary" onClick={() => this.onPayButton()}>PAY</Button>
                : <Button variant="primary" disabled >PAY</Button>}
          </Col>


          {localStorage.getItem('userType') === 'homeowner' ?
            <Col style={headerStyle}>
              {this.state.case.status_name === 'request' ?
                <Button variant="danger" onClick={this.handleShowCancelRequest} >Cancel</Button>
                : <Button variant="danger" disabled >Cancel</Button>
              }
            </Col> : null
          }
        </Row>
        <br /><br />




      </div>
    );

  }

}



const headerStyle = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  //paddingLeft : "10%",
  textAlign: "center",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle2 = {

  //paddingLeft: "40px",
  //paddingTop: "20px",
  textAlign: "right",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const headerStyle3 = {

  paddingLeft: "10px",
  paddingTop: "10px",
  textAlign: "left",
  //fontSize: "35px",
  //fontWeight: "bold"
};
const paragraphStyle = {
  //paddingLeft: "40px",
  //paddingRight: "40px",
  paddingTop: "5%",
  //textAlign: "left",
  //fontSize: "25px"
};

export default CaseDetail;